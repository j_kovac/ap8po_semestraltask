1) Všeobecné informácie
----------------------------------------------------------
Bc. Jozef Kováč
UTB Zlín, 4. ročník, SWI
Institute Management Software
AP8PO - Celoročný projekt

2) Technológie
----------------------------------------------------------
- Použitý vizualizačný jazyk a framework: WPF + .XML, UI - knižnica MahApps.Metro
- Použitý programovací jazyk a framework: .NET 5 + C#
- Úložiskové technológie: MS SQL (SSMS), XML serializácia
- Programovacie prostredie: Visual Studio IDE


3) Typový užívateľ a jeho potreby
----------------------------------------------------------
Typovým užívateľom aplikácie je tajomník / správca školského ústavu.
Aplikácia slúži k vytváraniu, editácii či mazaniu dát v predom stanovenej forme (viď. datamap.jpg). 
Aplikácia ďalej umožňuje tajomníkovi rozdeľovať prácu zamestnancom ústavu pri zohľadnení kapacitných limitov ním nastavených
v čiastkových objektoch a type úväzku jednotlivých zamestnancov.

Typický scenár použitia:
1) Tajomník prostredníctvom GUI vloží zamestnancov, predmety, rozvrhové skupiny a všetky potrebné aktivity
2) Tajomník realizuje uloženie údajov
3) Tajomník prejde na záložku umožňujúcu vygenerovanie pracovných štítkov
4) Tajomník prerozdelí zamestnancom jednotlivé vygenerované štítky za asistencie aplikácie, ktorá kontroluje, či sú dané činnosti v súlade s ich pracovným úväzkom
5) Tajomník uloží stav s ktorým je spokojný do SQL databázy a následne do XML súboru, ktorý môže kedykoľvek použiť k načítaniu ním rozpracovaných dát.

Všeobecný scenár:

4) Možnosti a ciele aplikácie
----------------------------------------------------------
- Vytvárať, editovať, mazať zamestancov
- Vytvárať, editovať, mazať predmety
- Vytvárať, editovať, mazať skupiny
- Vytvárať, editovať, mazať rozvrhové aktivity

- Kontrolovať správnosť zadaných údajov a premietať túto kontrolu do znemožnenia určitých akcií (uloženie)
- Kontrolovať vzťahy medzi niektorými údami a premietať túto kontrolu do reakcií UI (zvolený polovičný úväzok - nemôže zadať úväzok percentuálne)

- Realizovať načítanie / uloženie spracovávaných dát do SQL databázy
- Realizovať načítanie / uloženie spracovávaných dát do XML súborov
- Realizovať načítanie všeobecnej konfigurácie a statických dát aplikácie z XML / JSON súboru

- Generovať rozvrhové aktivity a rozdeľovať ich v závislosti na zadaných údajoch o kapacite a úväzkoch zamestancov
- Rozdeliť vygenerované rozvrhové aktivity medzi zamestnancov ústavu

- Zabezpečiť plynulý prechod medzi jednotlivými obrazovkami aplikácie
- Zabezpečiť integritu dát, ich automatické načítanie a ukladanie tak, aby nedochádzalo k porušeniu zobrazenia aplikácie
- Zabezpečiť previazanie dát, u ktorých z hľadiska ich logickej štruktúry previazanie dáva zmysel

- Poskytnúť prívetivý uživateľský zážitok prostredníctvom moderného štýlu a responzívneho UI

