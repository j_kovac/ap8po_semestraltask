﻿CREATE TABLE [dbo].[ScheduledActivities] (
    [ID]            INT            IDENTITY (1, 1) NOT NULL,
    [StudentsCount] INT            NOT NULL,
    [Name]          NVARCHAR (MAX) NULL,
    [Note]          NVARCHAR (MAX) NULL,
    [Language]      INT            NOT NULL,
    [HoursCount]    INT            NOT NULL,
    [WeeksCount]    INT            NOT NULL,
    [ActivityType]  INT            NOT NULL,
    [DateCreated]   DATETIME2 (7)  NOT NULL,
    [DateUpdated]   DATETIME2 (7)  NOT NULL,
    [IsDeleted]     BIT            DEFAULT (CONVERT([bit],(0))) NOT NULL,
    [ClassID]       INT            NULL,
    [EmployeeID]    INT            NULL,
    CONSTRAINT [PK_ScheduledActivities] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ScheduledActivities_Classes_ClassID] FOREIGN KEY ([ClassID]) REFERENCES [dbo].[Classes] ([ID]),
    CONSTRAINT [FK_ScheduledActivities_Employees_EmployeeID] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([ID])
);





GO
CREATE NONCLUSTERED INDEX [IX_ScheduledActivities_EmployeeID]
    ON [dbo].[ScheduledActivities]([EmployeeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ScheduledActivities_ClassID]
    ON [dbo].[ScheduledActivities]([ClassID] ASC);

