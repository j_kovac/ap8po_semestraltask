﻿CREATE TABLE [dbo].[Employees] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]      NVARCHAR (MAX) NULL,
    [LastName]       NVARCHAR (MAX) NULL,
    [BirthDate]      DATETIME2 (7)  NOT NULL,
    [Email]          NVARCHAR (MAX) NULL,
    [Phone]          NVARCHAR (MAX) NULL,
    [IsDocStudent]   BIT            NOT NULL,
    [Department]     INT            NOT NULL,
    [Salary]         FLOAT (53)     NOT NULL,
    [Note]           NVARCHAR (MAX) NULL,
    [EmployeeType]   INT            NOT NULL,
    [WorkPercentage] FLOAT (53)     NULL,
    [DateCreated]    DATETIME2 (7)  NOT NULL,
    [DateUpdated]    DATETIME2 (7)  NOT NULL,
    [IsDeleted]      BIT            DEFAULT (CONVERT([bit],(0))) NOT NULL,
    CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED ([ID] ASC)
);


