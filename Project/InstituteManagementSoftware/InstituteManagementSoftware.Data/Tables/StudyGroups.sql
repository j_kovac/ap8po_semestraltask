﻿CREATE TABLE [dbo].[StudyGroups] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (MAX) NULL,
    [ShortName]        NVARCHAR (MAX) NULL,
    [StudyYear]        INT            NOT NULL,
    [StudentsMaxCount] INT            NOT NULL,
    [StudyType]        INT            NOT NULL,
    [Semester]         INT            NOT NULL,
    [StudyForm]        INT            NOT NULL,
    [Language]         INT            NOT NULL,
    [DateCreated]      DATETIME2 (7)  NOT NULL,
    [DateUpdated]      DATETIME2 (7)  NOT NULL,
    [IsDeleted]        BIT            DEFAULT (CONVERT([bit],(0))) NOT NULL,
    [GarantID]         INT            NULL,
    CONSTRAINT [PK_StudyGroups] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_StudyGroups_Employees_GarantID] FOREIGN KEY ([GarantID]) REFERENCES [dbo].[Employees] ([ID])
);





GO
CREATE NONCLUSTERED INDEX [IX_StudyGroups_GarantID]
    ON [dbo].[StudyGroups]([GarantID] ASC);

