﻿CREATE TABLE [dbo].[Classes] (
    [ID]                  INT            IDENTITY (1, 1) NOT NULL,
    [FullName]            NVARCHAR (MAX) NULL,
    [ShortName]           NVARCHAR (MAX) NULL,
    [LengthWeeks]         INT            NOT NULL,
    [ClassSize]           INT            NOT NULL,
    [Credits]             INT            NOT NULL,
    [ExercicesHoursCount] INT            NOT NULL,
    [SeminarsHoursCount]  INT            NOT NULL,
    [LectureHoursCount]   INT            NOT NULL,
    [EndingType]          INT            NOT NULL,
    [Department]          INT            NOT NULL,
    [Language]            INT            NOT NULL,
    [DateCreated]         DATETIME2 (7)  NOT NULL,
    [DateUpdated]         DATETIME2 (7)  NOT NULL,
    [IsDeleted]           BIT            DEFAULT (CONVERT([bit],(0))) NOT NULL,
    CONSTRAINT [PK_Classes] PRIMARY KEY CLUSTERED ([ID] ASC)
);


