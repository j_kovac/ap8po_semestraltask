﻿CREATE TABLE [dbo].[StudyGroupSubjects] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [StudyGroupID] INT           NULL,
    [SubjectID]    INT           NULL,
    [IsDeleted]    BIT           NOT NULL,
    [DateCreated]  DATETIME2 (7) NOT NULL,
    [DateUpdated]  DATETIME2 (7) NOT NULL,
    CONSTRAINT [PK_StudyGroupSubjects] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_StudyGroupSubjects_Classes_SubjectID] FOREIGN KEY ([SubjectID]) REFERENCES [dbo].[Classes] ([ID]),
    CONSTRAINT [FK_StudyGroupSubjects_StudyGroups_StudyGroupID] FOREIGN KEY ([StudyGroupID]) REFERENCES [dbo].[StudyGroups] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_StudyGroupSubjects_StudyGroupID]
    ON [dbo].[StudyGroupSubjects]([StudyGroupID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_StudyGroupSubjects_SubjectID]
    ON [dbo].[StudyGroupSubjects]([SubjectID] ASC);

