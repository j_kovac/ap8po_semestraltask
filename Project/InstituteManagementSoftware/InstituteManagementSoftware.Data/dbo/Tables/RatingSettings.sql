﻿CREATE TABLE [dbo].[RatingSettings] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [SettingType] INT           NOT NULL,
    [Value]       FLOAT (53)    NOT NULL,
    [IsDeleted]   BIT           NOT NULL,
    [DateCreated] DATETIME2 (7) NOT NULL,
    [DateUpdated] DATETIME2 (7) NOT NULL,
    CONSTRAINT [PK_RatingSettings] PRIMARY KEY CLUSTERED ([ID] ASC)
);

