﻿namespace InstituteManagementSoftware.Constants.Visualisation.DragAndDrop
{
    public static class DragAndDropGroupManagementConstants
    {
        public static readonly string GroupPool = "GroupPool";
        public static readonly string GroupAssigned = "GroupAssigned";
    }
}
