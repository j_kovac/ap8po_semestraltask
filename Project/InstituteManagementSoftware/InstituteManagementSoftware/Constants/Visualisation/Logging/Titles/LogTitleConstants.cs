﻿namespace InstituteManagementSoftware.Constants.Visualisation.Logging.Titles
{
    public static class LogTitleConstants
    {
        public static readonly string ActionFinishedTitle = "ACTION FINISHED";
        public static readonly string ActionFailedTitle = "ACTION FAILED";

        public static readonly string ImportFinishedTitle = "IMPORT FINISHED";
        public static readonly string ImportFailedTitle = "IMPORT FAILED";

        public static readonly string ExportFinishedTitle = "EXPORT FINISHED";
        public static readonly string ExportFailedTitle = "EXPORT FAILED";

        public static readonly string ConnectionSuccessfulTitle = "CONNECTION SUCCESSFUL";
        public static readonly string ConnectionFailedTitle = "CONNECTION FAILED";

        public static readonly string SelectionSuccessfulTitle = "SELECTION SUCCESSFUL";
        public static readonly string AssignmentSuccessfulTitle = "ASSIGNMENT SUCCESSFUL";
        public static readonly string UnassignmentSuccessfulTitle = "UNASSIGNMENT SUCCESSFUL";
    }
}
