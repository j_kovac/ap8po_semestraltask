﻿namespace InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings.QuickActions
{
    public static class ExportLogStringConstants
    {
        public static readonly string ExportFinished = "EXPORTED file to LOCATION {0} FORMATTED as {1}.";
        public static readonly string ExportFailed = "EXPORT to LOCATION {0} has FAILED. Please check provided parameters and system permissions.";
    }
}
