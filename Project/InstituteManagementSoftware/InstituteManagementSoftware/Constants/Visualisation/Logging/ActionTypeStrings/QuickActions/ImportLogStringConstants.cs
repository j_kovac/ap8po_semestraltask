﻿namespace InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings.QuickActions
{
    public static class ImportLogStringConstants
    {
        public static readonly string ImportFinished = "IMPORTED database from FILE {0}. You can now SAVE your loaded data.";
        public static readonly string ImportFailed = "IMPORT from FILE {0} has FAILED. Please check provided parameters and system permissions.";
    }
}
