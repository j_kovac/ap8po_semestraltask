﻿namespace InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings.QuickActions
{
    public static class SendExportEmailLogStringConstants
    {
        public static readonly string EmailSent = "E-mail to ADDRESS {0} SUCESSFULLY SENT. ATTACHMENT: {1}.";
        public static readonly string EmailFailed = "E-mail to ADDRESS {0} has FAILED to SEND. Please check provided parameters and system permissions.";
    }
}
