﻿namespace InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings.QuickActions
{
    public static class SaveDataLogStringConstants
    {
        public static readonly string SaveFinished = "SAVE of IMPORTED data has been SUCCESSFUL. RELOAD window to display CHANGES.";
        public static readonly string SaveFailed = "SAVE of IMPORTED data has FAILED. EXPORT file might be INCONSISTEND or DAMAGED.";
    }
}
