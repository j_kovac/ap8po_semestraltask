﻿namespace InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings
{
    public static class ClassLogStringConstants
    {
        public static readonly string ClassCreated = "CLASS {0} has been CREATED with assigned ID: {1}.";
        public static readonly string ClassUpdated = "CLASS {0} with ID: {1} has been UPDATED.";
        public static readonly string ClassDeleted = "CLASS with assigned ID: {0} has been DELETED.";
        public static readonly string ClassLoaded = "CLASSES have been loaded. Total Count: {0}.";
    }
}
