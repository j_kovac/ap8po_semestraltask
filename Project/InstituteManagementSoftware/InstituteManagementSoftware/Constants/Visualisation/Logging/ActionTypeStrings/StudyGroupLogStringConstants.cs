﻿namespace InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings
{
    public static class StudyGroupLogStringConstants
    {
        public static readonly string StudyGroupCreated = "STUDY GROUP {0} has been CREATED with assigned ID: {1}.";
        public static readonly string StudyGroupUpdated = "STUDY GROUP {0} with ID: {1} has been UPDATED.";
        public static readonly string StudyGroupDeleted = "STUDY GROUP with assigned ID: {0} has been DELETED.";
        public static readonly string StudyGroupLoaded = "STUDY GROUPS have been loaded. Total Count: {0}.";
    }
}
