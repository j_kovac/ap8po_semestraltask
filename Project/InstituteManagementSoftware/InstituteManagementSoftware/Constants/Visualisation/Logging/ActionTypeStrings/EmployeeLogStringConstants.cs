﻿namespace InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings
{
    public static class EmployeeLogStringConstants
    {
        public static readonly string EmployeeCreated = "EMPLOYEE {0} has been CREATED with assigned ID: {1}.";
        public static readonly string EmployeeUpdated = "EMPLOYEE {0} with ID: {1} has been UPDATED.";
        public static readonly string EmployeeDeleted = "EMPLOYEE with assigned ID: {0} has been DELETED.";
        public static readonly string EmployeeLoaded = "EMPLOYEES have been loaded. Total Count: {0}.";
    }
}
