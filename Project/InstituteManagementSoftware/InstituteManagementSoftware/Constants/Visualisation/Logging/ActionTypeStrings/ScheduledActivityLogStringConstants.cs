﻿namespace InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings
{
    public static class ScheduledActivityLogStringConstants
    {
        public static readonly string ScheduledActivityCreated = "SCHEDULED ACTIVITY {0} has been CREATED with assigned ID: {1}.";
        public static readonly string ScheduledActivityUpdated = "SCHEDULED ACTIVITY {0} with ID: {1} has been UPDATED.";
        public static readonly string ScheduledActivityDeleted = "SCHEDULED ACTIVITY with assigned ID: {0} has been DELETED.";
        public static readonly string ScheduledActivityLoaded = "SCHEDULED ACTIVITIES have been loaded. Total Count: {0}.";
    }
}
