﻿namespace InstituteManagementSoftware.Constants.Visualisation.Logging.Management
{
    public static class ManagementLogStringConstants
    {
        public static readonly string ActivityManagementViewLoaded = "ACTIVITY MANAGEMENT view has been LOADED with {0} ASSIGNED activities and {1} UNASSIGNED activities.";
        public static readonly string GroupManagementViewLoaded = "GROUP MANAGEMENT view has been LOADED with pool of {0} subjects.";

        public static readonly string GroupManagementGroupSelected = "STUDY GROUP '{0}' has been SELECTED with {1} ASSIGNED subjects and {2} UNASSIGNED subjects.";
        public static readonly string GroupManagementGroupSubjectAssigned = "SUBJECT '{1}' has been ASSIGNED to STUDY GROUP '{0}' - Group currently has {2} ASSIGNED subjects and {3} UNASSIGNED subjects.";
        public static readonly string GroupManagementGroupSubjectUnassigned = "SUBJECT '{1}' has been UNASSIGNED from STUDY GROUP '{0}' - Group currently has {2} ASSIGNED subjects and {3} UNASSIGNED subjects.";

        public static readonly string ActivityManagementEmployeeSelected = "EMPLOYEE {0} has been SELECTED with {1} ASSIGNED scheduled activities and {2} activities in pool.";
        public static readonly string ActivityManagementActivityAssigned = "SCHEDULED ACTIVITY '{1}' has been ASSIGNED to EMPLOYEE '{0}' - EMPLOYEE currently has {2} ASSIGNED activities and {3} UNASSIGNED activities in pool.";
        public static readonly string ActivityManagementActivityUnassigned = "SCHEDULED ACTIVITY '{1}' has been UNASSIGNED from EMPLOYEE '{0}' - EMPLOYEE currently has {2} ASSIGNED activities and {3} UNASSIGNED activities in pool.";

        public static readonly string ActivityManagementGlobalUnassigned = "ALL scheduled activities have been UNASSIGNED and moved back to pool - Pool currently contains {0} UNASSIGNED activities.";
        public static readonly string ActivityManagementGlobalDeleted = "ALL {0} scheduled activities have been DELETED permanently.";
        public static readonly string ActivityManagementGlobalGenerated = "Activity Pool has been REGENERATED. Total COUNT of GENERATED scheduled activities: {0}";
    }
}
