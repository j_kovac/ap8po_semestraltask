﻿namespace InstituteManagementSoftware.Constants.Visualisation.Converters.Classes
{
    public static class EndingTypeStringConstants
    {
        public const string CR = "Credit";
        public const string CLCR = "Classified Credit";
        public const string EX = "Exam";
    }
}
