﻿namespace InstituteManagementSoftware.Constants.Visualisation.Converters.ScheduledActivities
{
    public static class ActivityTypeStringConstants
    {
        public const string SE = "Seminar";
        public const string LE = "Lecture";
        public const string EX = "Excercice";

        public const string SP_EXAM = "Exam";
        public const string SP_CR = "Credit";
        public const string SP_CLASCR = "Classified Credit";
    }
}
