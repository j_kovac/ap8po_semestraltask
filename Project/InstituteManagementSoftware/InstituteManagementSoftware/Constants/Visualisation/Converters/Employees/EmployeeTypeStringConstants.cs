﻿namespace InstituteManagementSoftware.Constants.Visualisation.Converters.Employees
{
    public static class EmployeeTypeStringConstants
    {
        public const string FT = "Full-Time";
        public const string PT = "Part-Time";
        public const string CS = "Custom";
    }
}
