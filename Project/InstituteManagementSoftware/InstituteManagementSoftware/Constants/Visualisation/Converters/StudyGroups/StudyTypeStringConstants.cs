﻿namespace InstituteManagementSoftware.Constants.Visualisation.Converters.StudyGroups
{
    public static class StudyTypeStringConstants
    {
        public const string PRE = "Presential";
        public const string DIS = "Distant";
        public const string COM = "Combined";
    }
}
