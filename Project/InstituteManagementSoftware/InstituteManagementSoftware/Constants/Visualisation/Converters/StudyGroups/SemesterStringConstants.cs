﻿namespace InstituteManagementSoftware.Constants.Visualisation.Converters.StudyGroups
{
    public static class SemesterStringConstants
    {
        public const string SS = "Summer Semester";
        public const string WS = "Winter Semester";
    }
}
