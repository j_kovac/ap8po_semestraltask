﻿namespace InstituteManagementSoftware.Constants.Visualisation.Converters.StudyGroups
{
    public static class StudyFormStringConstants
    {
        public const string BAC = "Bachelor";
        public const string MAS = "Masters";
        public const string DOC = "Doctoral";
    }
}
