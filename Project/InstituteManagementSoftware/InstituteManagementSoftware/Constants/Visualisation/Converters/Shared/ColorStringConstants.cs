﻿namespace InstituteManagementSoftware.Constants.Visualisation.Converters.Shared
{
    public static class ColorStringConstants
    {
        public const string Orange = "#FF8C00";
        public const string Green = "#077300";
        public const string Red = "#B92505";
        public const string Blue = "#0767B3";
        public const string White = "#FFFFFF";
    }
}
