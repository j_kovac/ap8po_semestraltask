﻿namespace InstituteManagementSoftware.Constants.Visualisation.Converters.Shared
{
    public static class DepartmentStringConstants
    {
        public const string DIAI = "Department of Informatics and Artifical Intelligence";
        public const string DCCS = "Department of Computer and Communication Systems";
        public const string DACE = "Department of Automation and Control Engineering";
        public const string DEM = "Department of Electronics and Mesurements";
        public const string DSE = "Department of Security Engineering";
        public const string DM = "Department of Mathematics";
        public const string DPC = "Department of Process Control";
        public const string CSIAT = "Centre for Security Information and Advanced Technologies";
        public const string ICT = "Information Communication Technology Park";
    }
}
