﻿namespace InstituteManagementSoftware.Constants.Visualisation.Converters.Shared
{
    public static class LanguageStringConstants
    {
        public const string CZ = "Czech";
        public const string EN = "English";
        public const string SK = "Slovak";
    }
}
