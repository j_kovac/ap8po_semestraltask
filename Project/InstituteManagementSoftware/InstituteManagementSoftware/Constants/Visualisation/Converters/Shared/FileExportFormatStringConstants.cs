﻿namespace InstituteManagementSoftware.Constants.Visualisation.Converters.Shared
{
    public static class FileExportFormatStringConstants
    {
        public const string JSON = ".JSON";
        public const string XML = ".XML";
    }
}
