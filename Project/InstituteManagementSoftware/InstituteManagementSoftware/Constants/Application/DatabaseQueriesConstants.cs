﻿namespace InstituteManagementSoftware.Constants.Application
{
    public static class DatabaseQueriesConstants
    {
        public static readonly string IdentityAddAllow = @"SET IDENTITY_INSERT [dbo].[{0}] ON";
        public static readonly string IdentityAddDeny = @"SET IDENTITY_INSERT [dbo].[{0}] OFF";
    }
}