﻿namespace InstituteManagementSoftware.Constants.Application
{
    public static class FileExportStringConstants
    {
        public static readonly string ExportedFilename = "InstituteManagement_Export";

        public static readonly string ExportedJSONSuffix = "json";
        public static readonly string ExportedXMLSuffix = "xml";

        public static readonly string JSONFileSuffix = ".json";
        public static readonly string XMLFileSuffix = ".xml";
    }
}
