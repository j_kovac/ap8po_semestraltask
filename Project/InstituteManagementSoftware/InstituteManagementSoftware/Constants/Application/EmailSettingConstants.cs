﻿using System;

namespace InstituteManagementSoftware.Constants.Application
{
    public static class EmailSettingConstants
    {
        public static readonly string EmailAddress = "ap8pojozefkovac@gmail.com";
        public static readonly string EmailPassword = "JozefKovacAP8PO";

        public static readonly string Subject = string.Format("Institute Management Software - Export {0}", DateTime.Now.ToShortDateString());
        public static readonly string Body = "Your exported Institute Management Software database is contained within the 'Attachments' section of this E-mail. If you are not an intended recipient of this E-mail, you can safely ignore it and all its contents.";

        public static readonly string StmpGmailAddress = "smtp.gmail.com";
        public static readonly int SmtpPort = 587;
    }
}
