﻿namespace InstituteManagementSoftware.Constants.Validation
{
    public static class StudyGroupValidationConstants
    {
        public const int NameLengthMin = 6;
        public const int NameLengthMax = 255;

        public const int ShortNameLengthMin = 3;
        public const int ShortNameLengthMax = 10;

        public const int StudyYearMinBachelor = 1;
        public const int StudyYearMaxBachelor = 3;
        public const int StudyYearMinMaster = 4;
        public const int StudyYearMaxMaster = 5;
        public const int StudyYearMinDoctorial = 6;
        public const int StudyYearMaxDoctorial = 9;

        public const int StudentsMaxCountMin = 1;
        public const int StudentsMaxCountMax = 100;
    }
}
