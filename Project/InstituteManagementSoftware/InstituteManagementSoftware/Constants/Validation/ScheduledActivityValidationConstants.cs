﻿namespace InstituteManagementSoftware.Constants.Validation
{
    public static class ScheduledActivityValidationConstants
    {
        public const int NameLengthMin = 6;
        public const int NameLengthMax = 256;

        public const int WeeksCountMin = 1;
        public const int HoursCountMin = 1;
        public const int StudentsCountMin = 1;

        public const int WeeksCountMax = 16;
        public const int HoursCountMax = 16;
        public const int StudentsCountMax = 256;
    }
}
