﻿namespace InstituteManagementSoftware.Constants.Validation
{
    public static class EmployeeValidationConstants
    {
        public const int AgeYearsMin = 18;
        public const int AgeYearsMax = 100;

        public const int EmailLenghtMax = 256;

        public const int PhoneLengthMax = 16;

        public const double SalaryMin = 1;
        public const double SalaryMax = 200000;

        public const double FullTimeWorkPercentage = 100;
        public const double PartTimeWorkPercentage = 50;
        public const double CustomWorkPercentageMin = 0;
        public const double CustomWorkPercentageMax = 100;

        public const int FirstNameMinLength = 2;
        public const int FirstNameMaxLength = 128;

        public const int LastNameMinLength = 2;
        public const int LastNameMaxLength = 256;
    }
}
