﻿namespace InstituteManagementSoftware.Constants.Validation
{
    public static class ClassValidationConstants
    {
        public const int LectureHoursMin = 0;
        public const int LectureHoursMax = 128;

        public const int SeminarHoursMin = 0;
        public const int SeminarHoursMax = 128;

        public const int ExerciceHoursMin = 0;
        public const int ExerciceHoursMax = 128;

        public const int CreditsMin = 1;
        public const int CreditsMax = 20;

        public const int ClassSizeMin = 1;
        public const int ClassSizeMax = 256;

        public const int LengthWeeksMin = 1;
        public const int LengthWeeksMax = 16;

        public const int NameLengthMin = 6;
        public const int NameLengthMax = 255;

        public const int ShortNameLengthMin = 3;
        public const int ShortNameLengthMax = 10;
    }
}
