﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InstituteManagementSoftware.Migrations
{
    public partial class StudyGroupSubjectsFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudyGroupSubjects_StudyGroups_SubjectID",
                table: "StudyGroupSubjects");

            migrationBuilder.CreateIndex(
                name: "IX_StudyGroupSubjects_StudyGroupID",
                table: "StudyGroupSubjects",
                column: "StudyGroupID");

            migrationBuilder.AddForeignKey(
                name: "FK_StudyGroupSubjects_StudyGroups_StudyGroupID",
                table: "StudyGroupSubjects",
                column: "StudyGroupID",
                principalTable: "StudyGroups",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudyGroupSubjects_StudyGroups_StudyGroupID",
                table: "StudyGroupSubjects");

            migrationBuilder.DropIndex(
                name: "IX_StudyGroupSubjects_StudyGroupID",
                table: "StudyGroupSubjects");

            migrationBuilder.AddForeignKey(
                name: "FK_StudyGroupSubjects_StudyGroups_SubjectID",
                table: "StudyGroupSubjects",
                column: "SubjectID",
                principalTable: "StudyGroups",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
