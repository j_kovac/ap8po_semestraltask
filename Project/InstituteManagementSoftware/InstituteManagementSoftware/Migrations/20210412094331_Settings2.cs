﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InstituteManagementSoftware.Migrations
{
    public partial class Settings2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SettingName",
                table: "RatingSettings",
                newName: "SettingType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SettingType",
                table: "RatingSettings",
                newName: "SettingName");
        }
    }
}
