﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InstituteManagementSoftware.Migrations
{
    public partial class remigratedDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GarantID",
                table: "StudyGroups",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ClassID",
                table: "ScheduledActivities",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EmployeeID",
                table: "ScheduledActivities",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "StudyGroupSubjects",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StudyGroupID = table.Column<int>(type: "int", nullable: true),
                    SubjectID = table.Column<int>(type: "int", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudyGroupSubjects", x => x.ID);
                    table.ForeignKey(
                        name: "FK_StudyGroupSubjects_Classes_SubjectID",
                        column: x => x.SubjectID,
                        principalTable: "Classes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudyGroupSubjects_StudyGroups_SubjectID",
                        column: x => x.SubjectID,
                        principalTable: "StudyGroups",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudyGroups_GarantID",
                table: "StudyGroups",
                column: "GarantID");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledActivities_ClassID",
                table: "ScheduledActivities",
                column: "ClassID");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledActivities_EmployeeID",
                table: "ScheduledActivities",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_StudyGroupSubjects_SubjectID",
                table: "StudyGroupSubjects",
                column: "SubjectID");

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduledActivities_Classes_ClassID",
                table: "ScheduledActivities",
                column: "ClassID",
                principalTable: "Classes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduledActivities_Employees_EmployeeID",
                table: "ScheduledActivities",
                column: "EmployeeID",
                principalTable: "Employees",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StudyGroups_Employees_GarantID",
                table: "StudyGroups",
                column: "GarantID",
                principalTable: "Employees",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduledActivities_Classes_ClassID",
                table: "ScheduledActivities");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduledActivities_Employees_EmployeeID",
                table: "ScheduledActivities");

            migrationBuilder.DropForeignKey(
                name: "FK_StudyGroups_Employees_GarantID",
                table: "StudyGroups");

            migrationBuilder.DropTable(
                name: "StudyGroupSubjects");

            migrationBuilder.DropIndex(
                name: "IX_StudyGroups_GarantID",
                table: "StudyGroups");

            migrationBuilder.DropIndex(
                name: "IX_ScheduledActivities_ClassID",
                table: "ScheduledActivities");

            migrationBuilder.DropIndex(
                name: "IX_ScheduledActivities_EmployeeID",
                table: "ScheduledActivities");

            migrationBuilder.DropColumn(
                name: "GarantID",
                table: "StudyGroups");

            migrationBuilder.DropColumn(
                name: "ClassID",
                table: "ScheduledActivities");

            migrationBuilder.DropColumn(
                name: "EmployeeID",
                table: "ScheduledActivities");
        }
    }
}
