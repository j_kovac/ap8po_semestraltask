﻿namespace InstituteManagementSoftware.Models.Settings.Enums
{
    public enum RatingSettingType
    {
        LECTURE_HOUR,
        EXERCICE_HOUR,
        SEMINAR_HOUR,
        LECTURE_HOUR_EN,
        EXERCICE_HOUR_EN,
        SEMINAR_HOUR_EN,
        CREDIT,
        CLASSIFIED_CREDIT,
        EXAM,
        CREDIT_EN,
        CLASSIFIED_CREDIT_EN,
        EXAM_EN
    }
}
