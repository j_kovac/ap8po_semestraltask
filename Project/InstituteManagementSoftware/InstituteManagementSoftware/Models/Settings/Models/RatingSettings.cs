﻿using InstituteManagementSoftware.Models.Settings.Enums;

namespace InstituteManagementSoftware.Models.Settings.Models
{
    public class RatingSettings : Entity
    {
        public RatingSettingType SettingType { get; set; }
        public double Value { get; set; }
    }
}
