﻿using InstituteManagementSoftware.Models.Employees.Enums;
using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Models.StudyGroups.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace InstituteManagementSoftware.Models.Employees.Models
{
    public class Employee : Entity
    {
        public Employee()
        {
            ScheduledActivities = new List<ScheduledActivity>();
            StudyGroups = new List<StudyGroup>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsDocStudent { get; set; }
        public Department Department { get; set; }
        public double Salary { get; set; }
        public string Note { get; set; }
        public EmployeeType EmployeeType { get; set; }
        public double? WorkPercentage { get; set; }

        #region Navigation

        [JsonIgnore]
        [XmlIgnoreAttribute]
        public IList<ScheduledActivity> ScheduledActivities { get; set; }

        [JsonIgnore]
        [XmlIgnoreAttribute]
        public IList<StudyGroup> StudyGroups { get; set; }

        #endregion
    }
}
