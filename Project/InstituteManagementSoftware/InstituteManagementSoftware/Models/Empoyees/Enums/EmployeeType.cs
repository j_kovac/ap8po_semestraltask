﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Employees;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Shared;
using System.ComponentModel;

namespace InstituteManagementSoftware.Models.Employees.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum EmployeeType
    {
        [Description(EmployeeTypeStringConstants.CS)]
        CUSTOM,

        [Description(EmployeeTypeStringConstants.PT)]
        PART_TIME,

        [Description(EmployeeTypeStringConstants.FT)]
        FULL_TIME
    }
}
