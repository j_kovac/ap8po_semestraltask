﻿using InstituteManagementSoftware.Models.Classes.Models;
using InstituteManagementSoftware.Models.StudyGroups.Models;
using Newtonsoft.Json;
using System.Xml.Serialization;

namespace InstituteManagementSoftware.Models.NtoNRelations
{
    public class StudyGroupSubject : Entity
    {
        #region Navigation

        public int? StudyGroupID { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public StudyGroup StudyGroup { get; set; }

        public int? SubjectID { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public Subject Subject { get; set; }

        #endregion
    }
}
