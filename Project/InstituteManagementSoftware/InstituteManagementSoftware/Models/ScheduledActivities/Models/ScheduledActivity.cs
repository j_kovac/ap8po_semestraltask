﻿using InstituteManagementSoftware.Models.Classes.Models;
using InstituteManagementSoftware.Models.Employees.Models;
using InstituteManagementSoftware.Models.ScheduledActivities.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using Newtonsoft.Json;
using System.Xml.Serialization;

namespace InstituteManagementSoftware.Models.ScheduledActivities.Models
{
    public class ScheduledActivity : Entity
    {
        public int StudentsCount { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public Language Language { get; set; }
        public int HoursCount { get; set; }
        public int WeeksCount { get; set; }
        public ActivityType ActivityType { get; set; }

        #region Navigation

        public int? EmployeeID { get; set; }

        [JsonIgnore]
        [XmlIgnoreAttribute]
        public Employee Employee { get; set; }

        public int? ClassID { get; set; }

        [JsonIgnore]
        [XmlIgnoreAttribute]
        public Subject Class { get; set; }


        #endregion
    }
}
