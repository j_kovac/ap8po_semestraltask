﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.ScheduledActivities;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Shared;
using System.ComponentModel;

namespace InstituteManagementSoftware.Models.ScheduledActivities.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum  ActivityType
    {
        [Description(ActivityTypeStringConstants.SE)]
        SEMINAR,

        [Description(ActivityTypeStringConstants.LE)]
        LECTURE,

        [Description(ActivityTypeStringConstants.EX)]
        EXERCICE,

        [Description(ActivityTypeStringConstants.SP_CR)]
        SPECIAL_CR,

        [Description(ActivityTypeStringConstants.SP_EXAM)]
        SPECIAL_EXAM,

        [Description(ActivityTypeStringConstants.SP_CLASCR)]
        SPECIAL_CLASCR
    }
}
