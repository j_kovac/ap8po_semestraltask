﻿using System;
using System.Collections.Generic;

namespace InstituteManagementSoftware.Models.Logs.models
{
    public class Log
    {
        public string Title { get; set; }
        public Tuple<Enum, object[]> Action { get; set; }
        public DateTime StartTime { get; set; }
    }
}