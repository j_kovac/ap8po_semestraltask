﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.StudyGroups;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Shared;
using System.ComponentModel;

namespace InstituteManagementSoftware.Models.StudyGroups.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum StudyForm
    {
        [Description(StudyFormStringConstants.BAC)]
        BACHELOR,

        [Description(StudyFormStringConstants.MAS)]
        MASTER,

        [Description(StudyFormStringConstants.DOC)]
        DOCTORAL
    }
}
