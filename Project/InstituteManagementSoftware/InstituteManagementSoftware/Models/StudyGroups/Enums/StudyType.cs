﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.StudyGroups;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Shared;
using System.ComponentModel;

namespace InstituteManagementSoftware.Models.StudyGroups.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum StudyType
    {
        [Description(StudyTypeStringConstants.PRE)]
        PRESENTIAL,

        [Description(StudyTypeStringConstants.DIS)]
        DISTANT,

        [Description(StudyTypeStringConstants.COM)]
        COMBINED
    }
}
