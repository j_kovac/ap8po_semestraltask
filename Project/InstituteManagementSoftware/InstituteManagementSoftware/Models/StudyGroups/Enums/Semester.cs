﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.StudyGroups;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Shared;
using System.ComponentModel;

namespace InstituteManagementSoftware.Models.StudyGroups.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum Semester
    {
        [Description(SemesterStringConstants.SS)]
        SUMMER_SEMESTER,

        [Description(SemesterStringConstants.WS)]
        WINTER_SEMESTER
    }
}
