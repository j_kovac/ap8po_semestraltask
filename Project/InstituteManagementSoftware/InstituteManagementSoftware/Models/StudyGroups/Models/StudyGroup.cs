﻿using InstituteManagementSoftware.Models.Employees.Models;
using InstituteManagementSoftware.Models.NtoNRelations;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Models.StudyGroups.Enums;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Xml.Serialization;

namespace InstituteManagementSoftware.Models.StudyGroups.Models
{
    public class StudyGroup : Entity
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int StudyYear { get; set; }
        public int StudentsMaxCount { get; set; }
        public StudyType StudyType { get; set; }
        public Semester Semester { get; set; }
        public StudyForm StudyForm { get; set; }
        public Language Language { get; set; }

        #region Navigation

        [JsonIgnore]
        [XmlIgnoreAttribute]
        public IList<StudyGroupSubject> StudyGroupSubjects  { get; set; }

        public int? GarantID { get; set; }

        [JsonIgnore]
        [XmlIgnoreAttribute]
        public Employee Garant { get; set; }

        #endregion
    }
}
