﻿using InstituteManagementSoftware.Models.Classes.Enums;
using InstituteManagementSoftware.Models.NtoNRelations;
using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using InstituteManagementSoftware.Models.Shared.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;

using System.Xml.Serialization;

namespace InstituteManagementSoftware.Models.Classes.Models
{
    public class Subject : Entity
    {
        public Subject()
        {
            StudyGroupSubjects = new List<StudyGroupSubject>();
            ScheduledActivities = new List<ScheduledActivity>();
        }

        public string FullName { get; set; }
        public string ShortName { get; set; }
        public int LengthWeeks { get; set; }
        public int ClassSize { get; set; }
        public int Credits { get; set; }
        public int ExercicesHoursCount { get; set; }
        public int SeminarsHoursCount { get; set; }
        public int LectureHoursCount { get; set; }
        public EndingType EndingType { get; set; }
        public Department Department { get; set; }
        public Language Language { get; set; }

        #region Navigation

        [JsonIgnore]
        [XmlIgnoreAttribute]
        public IList<StudyGroupSubject> StudyGroupSubjects { get; set; }

        [JsonIgnore]
        [XmlIgnoreAttribute]
        public IList<ScheduledActivity> ScheduledActivities { get; set; }

        #endregion
    }
}
