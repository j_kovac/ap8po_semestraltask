﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Classes;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Shared;
using System.ComponentModel;

namespace InstituteManagementSoftware.Models.Classes.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum EndingType
    {
        [Description(EndingTypeStringConstants.CR)]
        CREDIT,

        [Description(EndingTypeStringConstants.CLCR)]
        CLASSIFIED_CREDIT,

        [Description(EndingTypeStringConstants.EX)]
        EXAM
    }
}
