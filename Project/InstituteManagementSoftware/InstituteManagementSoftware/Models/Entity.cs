﻿using System;

namespace InstituteManagementSoftware.Models
{
    public class Entity
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}