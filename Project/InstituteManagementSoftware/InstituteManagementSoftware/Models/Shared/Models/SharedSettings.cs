﻿using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Infrastructure.Repositories.Settings;
using InstituteManagementSoftware.Models.Settings.Models;
using InstituteManagementSoftware.Models.Shared.Enums;
using System.Collections.Generic;

namespace InstituteManagementSoftware.Models.Shared.Models
{
    public class SharedSettings
    {
        private readonly ISettingsRepository _settingsRepository;

        public static IList<RatingSettings> RatingSettings;


        public FileExportFormat ExportFormat { get; set; }
        public bool SendExportEmail { get; set; }
        public string ExportEmail { get; set; }
        public string ExportFilePath { get; set; }
        public string ImportFilePath { get; set; }

        public DataContextRaw ImportedDatabase { get; set; }
        public bool AreImportChangesSaved { get; set; }


        public SharedSettings(ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
            RatingSettings = _settingsRepository.GetAll(null);

            AreImportChangesSaved = true;
        }
    }
}
