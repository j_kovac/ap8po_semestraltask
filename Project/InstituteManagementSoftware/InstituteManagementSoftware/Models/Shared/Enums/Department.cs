﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Shared;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Shared;
using System.ComponentModel;

namespace InstituteManagementSoftware.Models.Shared.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum Department
    {
        [Description(DepartmentStringConstants.DIAI)]
        DEPARTMENT_OF_INFORMATICS_AND_ARTIFICAL_INTELLIGENCE,

        [Description(DepartmentStringConstants.DCCS)]
        DEPARTMENT_OF_COMPUTER_AND_COMMUNICATION_SYSTEMS,

        [Description(DepartmentStringConstants.DACE)]
        DEPARTMENT_OF_AUTOMATION_AND_CONTROL_ENGINEERING,

        [Description(DepartmentStringConstants.DEM)]
        DEPARTMENT_OF_ELECTRONICS_AND_MESUREMENTS,

        [Description(DepartmentStringConstants.DSE)]
        DEPARTMENT_OF_SECURITY_ENGINEERING,

        [Description(DepartmentStringConstants.DM)]
        DEPARTMENT_OF_MATHEMATICS,

        [Description(DepartmentStringConstants.DPC)]
        DEPARTMENT_OF_PROCESS_CONTROL,

        [Description(DepartmentStringConstants.CSIAT)]
        CENTRE_FOR_SECURITY_INFORMATION_AND_ADVANCED_TECHNOLOGIES,

        [Description(DepartmentStringConstants.ICT)]
        ICT_TECHNOLOGY_PARK
    }
}
