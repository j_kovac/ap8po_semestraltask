﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Shared;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Shared;
using System.ComponentModel;

namespace InstituteManagementSoftware.Models.Shared.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum Language
    {
        [Description(LanguageStringConstants.CZ)]
        CZECH,

        [Description(LanguageStringConstants.EN)]
        ENGLISH,

        [Description(LanguageStringConstants.SK)]
        SLOVAK
    }
}
