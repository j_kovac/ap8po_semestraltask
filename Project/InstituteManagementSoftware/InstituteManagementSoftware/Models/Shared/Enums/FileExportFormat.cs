﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Shared;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Shared;
using System.ComponentModel;

namespace InstituteManagementSoftware.Models.Shared.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum FileExportFormat
    {
        [Description(FileExportFormatStringConstants.JSON)]
        JSON,
        [Description(FileExportFormatStringConstants.XML)]
        XML
    }
}
