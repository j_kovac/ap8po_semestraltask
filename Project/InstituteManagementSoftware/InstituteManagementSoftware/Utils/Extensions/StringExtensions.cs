﻿using System.Globalization;
using System.Text;

namespace InstituteManagementSoftware.Utils.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveDiacritics(this string s)
        {
            var normalizedString = string.IsNullOrEmpty(s) ? string.Empty : s.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            for (int i = 0; i < normalizedString.Length; i++)
            {
                var c = normalizedString[i];
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }
            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}
