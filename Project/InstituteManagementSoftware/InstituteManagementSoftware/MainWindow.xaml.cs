﻿using InstituteManagementSoftware.ViewModels.Windows;
using MahApps.Metro.Controls;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Windows;

namespace InstituteManagementSoftware
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void BtnOpenExportDirectory_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog()
            {
                InitialDirectory = @"C:\\",
                IsFolderPicker = true
            };
            try
            {
                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    var filename = dialog.FileName;
                    var context = DataContext as IndexWindowViewModel;
                    if (context is not null && !string.IsNullOrWhiteSpace(filename))
                    {
                        context.SelectExportPathCommand.Execute(filename);
                    }
                }
                else
                {
                    MessageBox.Show($"{dialog.FileName} does not exist",
                        "Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show($"{dialog.FileName} does not exist",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void BtnOpenImportFile_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog()
            {
                Multiselect = false,
                Filter = "JSON files (*.json)|*.json|XML files (*.xml)|*.xml"
            };

            try
            {
                if (dialog.ShowDialog() == true)
                {
                    var filename = dialog.FileName;
                    var context = DataContext as IndexWindowViewModel;
                    if (context is not null && !string.IsNullOrWhiteSpace(filename))
                    {
                        context.SelectImportPathCommand.Execute(filename);
                    }
                }
                else
                {
                    MessageBox.Show($"File {dialog.FileName} does not exist",
                        "Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Exception: {ex} has occured",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }
    }
}
