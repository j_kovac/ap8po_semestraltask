﻿using InstituteManagementSoftware.Infrastructure.Data.Configuration.Classes;
using InstituteManagementSoftware.Infrastructure.Data.Configuration.Employees;
using InstituteManagementSoftware.Infrastructure.Data.Configuration.ScheduledActivities;
using InstituteManagementSoftware.Infrastructure.Data.Configuration.Settings;
using InstituteManagementSoftware.Infrastructure.Data.Configuration.StudyGroups;
using InstituteManagementSoftware.Infrastructure.Data.Configuration.StudyGroupSubjects;
using InstituteManagementSoftware.Models;
using InstituteManagementSoftware.Models.Classes.Models;
using InstituteManagementSoftware.Models.Employees.Models;
using InstituteManagementSoftware.Models.NtoNRelations;
using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using InstituteManagementSoftware.Models.Settings.Models;
using InstituteManagementSoftware.Models.StudyGroups.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace InstituteManagementSoftware.Infrastructure.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        { }

        #region Database Sets

        public DbSet<Subject> Classes { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<ScheduledActivity> ScheduledActivities { get; set; }
        public DbSet<StudyGroup> StudyGroups { get; set; }
        public DbSet<StudyGroupSubject> StudyGroupSubjects { get; set; }

        public DbSet<RatingSettings> RatingSettings { get; set; }

        #endregion

        public void StopTrackingChanges()
        {
            ChangeTracker.AutoDetectChangesEnabled = false;
        }

        public void StartTrackingChanges()
        {
            ChangeTracker.AutoDetectChangesEnabled = true;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ClassConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeConfiguration());
            modelBuilder.ApplyConfiguration(new ScheduledActivityConfiguration());
            modelBuilder.ApplyConfiguration(new StudyGroupConfiguration());
            modelBuilder.ApplyConfiguration(new StudyGroupSubjectConfiguration());

            modelBuilder.ApplyConfiguration(new SettingsConfiguration());
        }
        
        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();

            var changedEntities = ChangeTracker.Entries().Where(e => e.Entity is Entity
                                                && (e.State is EntityState.Modified or EntityState.Added or EntityState.Deleted));
            foreach (var item in changedEntities)
            {
                var entity = item.Entity as Entity;

                if (item.State == EntityState.Added)
                {
                    if (entity is not null) entity.DateCreated = DateTime.Now;
                }
                if (entity is not null) entity.DateUpdated = DateTime.Now;
            }

            ChangeTracker.AutoDetectChangesEnabled = false;
            var result = base.SaveChanges();
            ChangeTracker.AutoDetectChangesEnabled = true;

            return result;
        }
        
    }
}
