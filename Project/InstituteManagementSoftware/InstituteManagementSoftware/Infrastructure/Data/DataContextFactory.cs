﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace InstituteManagementSoftware.Infrastructure.Data
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseSqlServer(@"Server=(LocalDb)\MSSQLLocalDB;"
                + "Integrated Security=SSPI;"
                + "Database=AP8PO_Projekt;"
                + "persist security info=True;"
                + "multipleactiveResultsets=True;");
            return new DataContext(builder.Options);
        }
    }
}
