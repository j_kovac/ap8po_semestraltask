﻿using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InstituteManagementSoftware.Infrastructure.Data.Configuration.ScheduledActivities
{
    public class ScheduledActivityConfiguration : IEntityTypeConfiguration<ScheduledActivity>
    {
        public void Configure(EntityTypeBuilder<ScheduledActivity> builder)
        {
            builder.HasKey(e => e.ID);

            builder.HasOne(e => e.Employee)
                .WithMany(e => e.ScheduledActivities)
                .HasForeignKey(e => e.EmployeeID);


            builder.HasOne(e => e.Class)
                .WithMany(e => e.ScheduledActivities)
                .HasForeignKey(e => e.ClassID);
        }
    }
}
