﻿using InstituteManagementSoftware.Models.Classes.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InstituteManagementSoftware.Infrastructure.Data.Configuration.Classes
{
    public class ClassConfiguration : IEntityTypeConfiguration<Subject>
    {
        public void Configure(EntityTypeBuilder<Subject> builder)
        {
            builder.HasKey(e => e.ID);

            builder.HasMany(e => e.ScheduledActivities)
                .WithOne(e => e.Class)
                .HasForeignKey(e => e.ClassID);

            builder.HasMany(e => e.StudyGroupSubjects)
                .WithOne(e => e.Subject)
                .HasForeignKey(e => e.SubjectID);
        }
    }
}
