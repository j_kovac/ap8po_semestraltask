﻿using InstituteManagementSoftware.Models.Employees.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InstituteManagementSoftware.Infrastructure.Data.Configuration.Employees
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasKey(e => e.ID);

            builder.HasMany(e => e.ScheduledActivities)
                .WithOne(e => e.Employee)
                .HasForeignKey(e => e.EmployeeID);

            builder.HasMany(e => e.StudyGroups)
                .WithOne(e => e.Garant)
                .HasForeignKey(e => e.GarantID);
        }
    }
}
