﻿using InstituteManagementSoftware.Models.Settings.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InstituteManagementSoftware.Infrastructure.Data.Configuration.Settings
{
    public class SettingsConfiguration : IEntityTypeConfiguration<RatingSettings>
    {
        public void Configure(EntityTypeBuilder<RatingSettings> builder)
        {
            builder.HasKey(e => e.ID);
        }
    }
}
