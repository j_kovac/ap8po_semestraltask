﻿using InstituteManagementSoftware.Models.StudyGroups.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InstituteManagementSoftware.Infrastructure.Data.Configuration.StudyGroups
{
    public class StudyGroupConfiguration : IEntityTypeConfiguration<StudyGroup>
    {
        public void Configure(EntityTypeBuilder<StudyGroup> builder)
        {
            builder.HasKey(e => e.ID);

            builder.HasOne(e => e.Garant)
                .WithMany(e => e.StudyGroups)
                .HasForeignKey(e => e.GarantID);

            builder.HasMany(e => e.StudyGroupSubjects)
                .WithOne(e => e.StudyGroup)
                .HasForeignKey(e => e.StudyGroupID);
        }
    }
}
