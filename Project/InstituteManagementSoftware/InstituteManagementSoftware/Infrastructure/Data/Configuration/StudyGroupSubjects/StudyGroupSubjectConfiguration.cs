﻿using InstituteManagementSoftware.Models.NtoNRelations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InstituteManagementSoftware.Infrastructure.Data.Configuration.StudyGroupSubjects
{
    public class StudyGroupSubjectConfiguration : IEntityTypeConfiguration<StudyGroupSubject>
    {
        public void Configure(EntityTypeBuilder<StudyGroupSubject> builder)
        {
            builder.HasKey(e => e.ID);

            builder.HasOne(e => e.Subject)
                .WithMany(e => e.StudyGroupSubjects);

            builder.HasOne(e => e.StudyGroup)
                .WithMany(e => e.StudyGroupSubjects);
        }
    }
}
