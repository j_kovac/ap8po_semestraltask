﻿using InstituteManagementSoftware.Models.Classes.Models;
using InstituteManagementSoftware.Models.Employees.Models;
using InstituteManagementSoftware.Models.NtoNRelations;
using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using InstituteManagementSoftware.Models.StudyGroups.Models;
using System.Collections.Generic;

namespace InstituteManagementSoftware.Infrastructure.Data
{
    public class DataContextRaw
    {
        public List<Subject> Classes { get; set; }
        public List<Employee> Employees { get; set; }
        public List<ScheduledActivity> ScheduledActivities { get; set; }
        public List<StudyGroup> StudyGroups { get; set; }
        public List<StudyGroupSubject> StudyGroupSubjects { get; set; }
    }
}
