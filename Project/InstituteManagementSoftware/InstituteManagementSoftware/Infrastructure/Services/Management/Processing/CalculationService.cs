﻿using InstituteManagementSoftware.Models.ScheduledActivities.Enums;
using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using InstituteManagementSoftware.Models.Settings.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Models.Shared.Models;
using System.Collections.Generic;
using System.Linq;

namespace InstituteManagementSoftware.Infrastructure.Services.Management.Processing
{
    public class CalculationService : ICalculationService
    {
        public double CalculateWorkingPointsActivities(IList<ScheduledActivity> activities)
        {
            var sum = 0.0;
            foreach(var activity in activities)
            {
                sum += CalculateWorkingPointsActivity(activity);
            }
            return sum;
        }

        public double CalculateWorkingPointsActivity(ScheduledActivity activity)
        {
            var hourMultiplier = 1;
            var resultRating = 0.0;
            switch (activity.ActivityType)
            {
                case ActivityType.EXERCICE:
                    resultRating = activity.Language == Language.ENGLISH ?
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.EXERCICE_HOUR_EN).Value :
                        SharedSettings.RatingSettings.Single(x=> x.SettingType == RatingSettingType.EXERCICE_HOUR).Value;
                    hourMultiplier = activity.Class?.ExercicesHoursCount ?? 1;
                    break;
                case ActivityType.SEMINAR:
                    resultRating = activity.Language == Language.ENGLISH ? 
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.SEMINAR_HOUR_EN).Value :
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.SEMINAR_HOUR).Value;
                    hourMultiplier = activity.Class?.SeminarsHoursCount ?? 1;
                    break;
                case ActivityType.LECTURE:
                    resultRating = activity.Language == Language.ENGLISH ? 
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.LECTURE_HOUR_EN).Value :
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.LECTURE_HOUR).Value;
                    hourMultiplier = activity.Class?.LectureHoursCount ?? 1;
                    break;
                case ActivityType.SPECIAL_EXAM:
                    resultRating = activity.Language == Language.ENGLISH ? 
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.EXAM_EN).Value :
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.EXAM).Value;
                    break;
                case ActivityType.SPECIAL_CLASCR:
                    resultRating = activity.Language == Language.ENGLISH ? 
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.CLASSIFIED_CREDIT_EN).Value :
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.CLASSIFIED_CREDIT).Value;
                    break;
                case ActivityType.SPECIAL_CR:
                    resultRating = activity.Language == Language.ENGLISH ?
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.CREDIT_EN).Value :
                        SharedSettings.RatingSettings.Single(x => x.SettingType == RatingSettingType.CREDIT).Value;
                    break;
            }
            return resultRating * hourMultiplier;
        }
    }
}
