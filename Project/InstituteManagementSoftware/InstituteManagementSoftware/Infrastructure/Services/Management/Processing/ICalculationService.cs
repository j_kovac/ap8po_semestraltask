﻿using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using System.Collections.Generic;

namespace InstituteManagementSoftware.Infrastructure.Services.Management.Processing
{
    public interface ICalculationService
    {
        double CalculateWorkingPointsActivity(ScheduledActivity activity);
        double CalculateWorkingPointsActivities(IList<ScheduledActivity> activities);
    }
}
