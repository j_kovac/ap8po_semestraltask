﻿using InstituteManagementSoftware.Constants.Visualisation.Logging.Titles;
using InstituteManagementSoftware.Infrastructure.Repositories.Employees;
using InstituteManagementSoftware.Infrastructure.Repositories.ScheduledActivities;
using InstituteManagementSoftware.Infrastructure.Repositories.StudyGroups;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Infrastructure.Services.Management.Processing;
using InstituteManagementSoftware.Models.Classes.Enums;
using InstituteManagementSoftware.Models.Classes.Models;
using InstituteManagementSoftware.Models.Logs.Enums;
using InstituteManagementSoftware.Models.ScheduledActivities.Enums;
using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using InstituteManagementSoftware.Models.StudyGroups.Models;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace InstituteManagementSoftware.Infrastructure.Services.Management.Activities
{
    public class ActivityManagementService : IActivityManagementService
    {
        private readonly IEmployeesRepository _employeesRepository;
        private readonly IStudyGroupsRepository _studyGroupsRepository;
        private readonly IScheduledActivitiesRepository _scheduledActivitiesRepository;

        private readonly ILoggingService _loggingService;
        private readonly ICalculationService _calculationService;

        private EmployeeViewModel SelectedEmployee { get; set; }

        private readonly ObservableCollection<ScheduledActivityViewModel> ScheduledActivitiesUnassigned;
        private readonly ObservableCollection<ScheduledActivityViewModel> ScheduledActivitiesAssigned;
        private readonly ObservableCollection<ScheduledActivityViewModel> ScheduledActivitiesAssignedToOther;

        public ActivityManagementService(IEmployeesRepository employeesRepository, ILoggingService loggingService, IScheduledActivitiesRepository scheduledActivitiesRepository, IStudyGroupsRepository studyGroupsRepository, ICalculationService calculationService)
        {
            _employeesRepository = employeesRepository;
            _studyGroupsRepository = studyGroupsRepository;
            _scheduledActivitiesRepository = scheduledActivitiesRepository;

            _loggingService = loggingService;
            _calculationService = calculationService;

            ScheduledActivitiesUnassigned = new ObservableCollection<ScheduledActivityViewModel>();
            ScheduledActivitiesAssigned = new ObservableCollection<ScheduledActivityViewModel>();
            ScheduledActivitiesAssignedToOther = new ObservableCollection<ScheduledActivityViewModel>();

            InitializeCollections();
        }

        private void InitializeCollections()
        {
            var scheduledActivities = _scheduledActivitiesRepository.GetAll(null);
            foreach (var scheduledActivity in scheduledActivities)
            {
                var activityModel = new ScheduledActivityViewModel(scheduledActivity) { IsChanged = false };
                ScheduledActivitiesUnassigned.Add(activityModel);
            }
            _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.LOAD_ACTIVITY_MANAGEMENT_VIEW, ScheduledActivitiesAssignedToOther.Count, ScheduledActivitiesUnassigned.Count);
        }

        public ObservableCollection<EmployeeViewModel> ExtractAvailableEmployees()
        {
            var employees = _employeesRepository.GetAll(null);
            var list = new ObservableCollection<EmployeeViewModel>();
            foreach (var employee in employees)
            {
                var employeeModel = new EmployeeViewModel(employee) { IsChanged = false };
                list.Add(employeeModel);
            }
            return list;
        }

        public Tuple<ObservableCollection<ScheduledActivityViewModel>, ObservableCollection<ScheduledActivityViewModel>, ObservableCollection<ScheduledActivityViewModel>> ExtractSourceCollections() => new(ScheduledActivitiesAssigned, ScheduledActivitiesUnassigned, ScheduledActivitiesAssignedToOther);

        private void RecalculateWorkPoints(EmployeeViewModel employee, bool nullify = false)
        {
            if (nullify)
            { SelectedEmployee.WorkingPoints = 0; }
            else
            {
                employee.WorkingPoints = _calculationService.CalculateWorkingPointsActivities(ScheduledActivitiesAssigned.Select(x => x.GetModel()).ToList());
            }
        }

        public void GenerateActivities(object obj)
        {
            var studyGroups = _studyGroupsRepository.GetAll(x => !x.IsDeleted);
            var subjects = studyGroups.SelectMany(x => x.StudyGroupSubjects).Select(x => x.Subject).Distinct().ToList();

            var activities = _scheduledActivitiesRepository.InsertAll(GenerateAllScheduledActivities(studyGroups, subjects));

            ScheduledActivitiesUnassigned.Clear();
            ScheduledActivitiesAssigned.Clear();
            ScheduledActivitiesAssignedToOther.Clear();

            foreach(var activity in activities)
            {
                ScheduledActivitiesUnassigned.Add(new ScheduledActivityViewModel(activity));
            }

            _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.ACTIVITY_MANAGEMENT_GENERATED_ACTIVITIES, ScheduledActivitiesUnassigned.Count);
        }

        private IList<ScheduledActivity> GenerateAllScheduledActivities(IList<StudyGroup> studyGroups, IList<Subject> subjects)
        {
            var activities = new List<ScheduledActivity>();

            // Generate activities based on study groups and class types
            foreach (var group in studyGroups)
            {
                foreach (var subject in group.StudyGroupSubjects)
                {

                    var activitiesPerGroupSubject = (int)Math.Ceiling(group.StudentsMaxCount / (double)subject.Subject.ClassSize);
                    var studentsPerSubject = (int)Math.Ceiling(group.StudentsMaxCount / (double)activitiesPerGroupSubject);

                    if (subject.Subject.ExercicesHoursCount != 0)
                    {
                        activities.AddRange(GenerateSubjectActivities(group, subject.Subject, ActivityType.EXERCICE, activitiesPerGroupSubject, studentsPerSubject));
                    }
                    if (subject.Subject.SeminarsHoursCount != 0)
                    {
                        activities.AddRange(GenerateSubjectActivities(group, subject.Subject, ActivityType.SEMINAR, activitiesPerGroupSubject, studentsPerSubject));
                    }
                    if (subject.Subject.LectureHoursCount != 0)
                    {
                        activities.AddRange(GenerateSubjectActivities(group, subject.Subject, ActivityType.LECTURE, 1, group.StudentsMaxCount));
                    }
                }
            }

            // Generate special ending activities for each class
            foreach (var subject in subjects)
            {
                var assignedGroups = studyGroups?.SelectMany(x => x.StudyGroupSubjects).Distinct()?.Where(x => x.SubjectID == subject.ID)?.ToList();
                if(assignedGroups?.Count > 0)
                {
                    activities.Add(GenerateEndingActivity(subject, subject.EndingType));
                }
            }
            return activities;
        }

        private IList<ScheduledActivity> GenerateSubjectActivities(StudyGroup group, Subject subject, ActivityType type, int activitiesCount, int studentsCount)
        {
            var generatedActivities = new List<ScheduledActivity>();
            var hoursCount = 0;

            switch (type)
            {
                case ActivityType.EXERCICE:
                    hoursCount = subject.ExercicesHoursCount;
                    break;
                case ActivityType.SEMINAR:
                    hoursCount = subject.SeminarsHoursCount;
                    break;
                case ActivityType.LECTURE:
                    hoursCount = subject.LectureHoursCount;
                    break;
            }

            for(int i = 0; i < activitiesCount; i++)
            {
                generatedActivities.Add(new ScheduledActivity()
                {
                    Name = $"{subject.ShortName} / {group.ShortName} - {i}",
                    ActivityType = type,
                    Class = subject,
                    ClassID = subject.ID,
                    HoursCount = hoursCount,
                    Language = subject.Language,
                    StudentsCount = studentsCount,
                    WeeksCount = subject.LengthWeeks,
                    Note = $"{type} - {subject.ShortName} - {i}"
                });
            }
            return generatedActivities;
        }

        private ScheduledActivity GenerateEndingActivity(Subject subject, EndingType endingType)
        {
            ActivityType? activityType;
            if (endingType == EndingType.CREDIT) { activityType = ActivityType.SPECIAL_CR; }
            else if(endingType == EndingType.CLASSIFIED_CREDIT) { activityType = ActivityType.SPECIAL_CLASCR; }
            else { activityType = ActivityType.SPECIAL_EXAM; }

            var name = $"{subject.ShortName}";
            return new ScheduledActivity()
            {
                Name = name,
                ActivityType = activityType.Value,
                Class = subject,
                ClassID = subject.ID,
                HoursCount = 0,
                Language = subject.Language,
                WeeksCount = 0,
                StudentsCount = subject.ClassSize,
                Note = $"Special {activityType} activity for subject {subject.ShortName} - {subject.FullName}",
            };
        }


        public void UnassignAll(object obj)
        {
            var sharedPool = ScheduledActivitiesAssigned.Concat(ScheduledActivitiesUnassigned).Concat(ScheduledActivitiesAssignedToOther).ToList();
            foreach(var activity in sharedPool)
            {
                activity.Employee = null;
            }
            _scheduledActivitiesRepository.Update(sharedPool.Select(x => x.GetModel()).ToList());

            ScheduledActivitiesUnassigned.Clear();
            ScheduledActivitiesAssigned.Clear();
            ScheduledActivitiesAssignedToOther.Clear();

            foreach(var activity in sharedPool)
            {
                ScheduledActivitiesUnassigned.Add(activity);
            }
            RecalculateWorkPoints(null, true);
            _loggingService.WriteLog(LogTitleConstants.UnassignmentSuccessfulTitle, LogActionType.ACTIVITY_MANAGEMENT_GLOBAL_UNASSIGNED, ScheduledActivitiesUnassigned.Count);
        }


        public void ChangeSelection(object obj)
        {
            var selectedEmployee = obj as EmployeeViewModel;
            if (selectedEmployee is not null)
            {
                var sharedPool = ScheduledActivitiesAssigned.Concat(ScheduledActivitiesUnassigned).Concat(ScheduledActivitiesAssignedToOther).ToList();

                ScheduledActivitiesUnassigned.Clear();
                ScheduledActivitiesAssigned.Clear();
                ScheduledActivitiesAssignedToOther.Clear();
                foreach (var item in sharedPool)
                {
                    var itemEmployee = item.Employee.GetModel();
                    if(itemEmployee is null)
                    {
                        ScheduledActivitiesUnassigned.Add(item);
                    }
                    else
                    {
                        if (itemEmployee.ID == selectedEmployee.GetModel().ID)
                        {
                            ScheduledActivitiesAssigned.Add(item);
                        }
                        else
                        {
                            ScheduledActivitiesAssignedToOther.Add(item);
                        }
                    }
                }
                SelectedEmployee = selectedEmployee;
                RecalculateWorkPoints(selectedEmployee, false);
                _loggingService.WriteLog(LogTitleConstants.SelectionSuccessfulTitle, LogActionType.ACTIVITY_MANAGEMENT_EMPLOYEE_SELECTED, selectedEmployee.FullName, ScheduledActivitiesAssigned.Count, ScheduledActivitiesUnassigned.Count);
            }
        }

        public void Assign(object activity, EmployeeViewModel selectedEmployee)
        {
            var assignedActivity = activity as ScheduledActivityViewModel;
            if (assignedActivity is not null)
            {
                var employeeModel = selectedEmployee.GetModel();
                assignedActivity.GetModel().Employee = employeeModel;

                _scheduledActivitiesRepository.Update(assignedActivity.GetModel());
                RecalculateWorkPoints(selectedEmployee, false);
                _loggingService.WriteLog(LogTitleConstants.AssignmentSuccessfulTitle, LogActionType.ACTIVITY_MANAGEMENT_ACTIVITY_ASSIGNED, selectedEmployee.FullName, assignedActivity.Name, ScheduledActivitiesAssigned.Count, ScheduledActivitiesUnassigned.Count);
            }
        }

        public void Unassign(object activity, EmployeeViewModel selectedEmployee)
        {
            var removedActivity = activity as ScheduledActivityViewModel;
            if (removedActivity is not null)
            {
                if (removedActivity.Employee.GetModel().ID == selectedEmployee.GetModel().ID)
                {
                    removedActivity.Employee = null;
                    _scheduledActivitiesRepository.Update(removedActivity.GetModel());
                    RecalculateWorkPoints(selectedEmployee, false);
                    _loggingService.WriteLog(LogTitleConstants.UnassignmentSuccessfulTitle, LogActionType.ACTIVITY_MANAGEMENT_ACTIVITY_UNASSIGNED, removedActivity.Name, selectedEmployee.FullName, ScheduledActivitiesAssigned.Count, ScheduledActivitiesUnassigned.Count);
                }
            }
        }

        public void DeleteAll(object obj)
        {
            var sharedPool = ScheduledActivitiesAssigned.Concat(ScheduledActivitiesUnassigned).Concat(ScheduledActivitiesAssignedToOther);
            var count = sharedPool.Count();

            _scheduledActivitiesRepository.DeleteAll(sharedPool.Select(x => x.GetModel()).ToList());

            ScheduledActivitiesUnassigned.Clear();
            ScheduledActivitiesAssigned.Clear();
            ScheduledActivitiesAssignedToOther.Clear();

            RecalculateWorkPoints(null, true);
            _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.ACTIVITY_MANAGEMENT_ALL_DELETED, count);
        }
    }
}