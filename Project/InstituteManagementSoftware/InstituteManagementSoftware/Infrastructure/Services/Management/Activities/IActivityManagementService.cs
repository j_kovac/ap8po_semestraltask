﻿using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using System;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.Management.Activities
{
    public interface IActivityManagementService
    {
        ObservableCollection<EmployeeViewModel> ExtractAvailableEmployees();
        Tuple<ObservableCollection<ScheduledActivityViewModel>, ObservableCollection<ScheduledActivityViewModel>, ObservableCollection<ScheduledActivityViewModel>> ExtractSourceCollections();

        void UnassignAll(object obj);
        void GenerateActivities(object obj);
        void DeleteAll(object obj);

        void ChangeSelection(object obj);
        void Assign(object activity, EmployeeViewModel selectedEmployee);
        void Unassign(object activity, EmployeeViewModel selectedEmployee);
    }
}
