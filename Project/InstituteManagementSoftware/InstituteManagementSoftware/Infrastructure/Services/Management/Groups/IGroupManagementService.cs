﻿using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using System;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.Management.Groups
{
    public interface IGroupManagementService
    {
        ObservableCollection<StudyGroupViewModel> ExtractAvailableStudyGroups();
        Tuple<ObservableCollection<ClassViewModel>, ObservableCollection<ClassViewModel>> ExtractSourceCollections();

        void ChangeSelection(object obj);

        void Assign(object subject, StudyGroupViewModel selectedStudyGroup);
        void Unassign(object subject, StudyGroupViewModel selectedStudyGroup);
    }
}
