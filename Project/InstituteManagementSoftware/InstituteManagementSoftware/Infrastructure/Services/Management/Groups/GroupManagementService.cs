﻿using InstituteManagementSoftware.Constants.Visualisation.Logging.Titles;
using InstituteManagementSoftware.Infrastructure.Repositories.Classes;
using InstituteManagementSoftware.Infrastructure.Repositories.StudyGroups;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Models.Logs.Enums;
using InstituteManagementSoftware.Models.NtoNRelations;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace InstituteManagementSoftware.Infrastructure.Services.Management.Groups
{
    public class GroupManagementService : IGroupManagementService
    {

        private readonly ILoggingService _loggingService;
        private readonly IStudyGroupsRepository _studyGroupRepository;
        private readonly IClassesRepository _classesRepository;

        public ObservableCollection<ClassViewModel> ClassesPool { get; set; }
        public ObservableCollection<ClassViewModel> ClassesAssigned { get; set; }

        public GroupManagementService(IStudyGroupsRepository studyGroupsRepository, ILoggingService loggingService, IClassesRepository classesRepository)
        {
            _studyGroupRepository = studyGroupsRepository;
            _classesRepository = classesRepository;
            _loggingService = loggingService;

            ClassesPool = new ObservableCollection<ClassViewModel>();
            ClassesAssigned = new ObservableCollection<ClassViewModel>();

            InitializeCollections();
        }

        private void InitializeCollections()
        {
            var classes = _classesRepository.GetAll(null);
            foreach (var subject in classes)
            {
                var classModel = new ClassViewModel(subject) { IsChanged = false };

                ClassesPool.Add(classModel);
            }
            _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.LOAD_GROUP_MANAGEMENT_VIEW, classes.Count);
        }

        public ObservableCollection<StudyGroupViewModel> ExtractAvailableStudyGroups()
        {
            var studyGroups = _studyGroupRepository.GetAll(null);
            var list = new ObservableCollection<StudyGroupViewModel>();
            foreach (var studyGroup in studyGroups)
            {
                var groupModel = new StudyGroupViewModel(studyGroup) { IsChanged = false };
                list.Add(groupModel);
            }
            return list;
        }

        public Tuple<ObservableCollection<ClassViewModel>, ObservableCollection<ClassViewModel>> ExtractSourceCollections() => new(ClassesAssigned, ClassesPool);

        public void ChangeSelection(object obj)
        {
            var selectedStudyGroup = obj as StudyGroupViewModel;
            if (selectedStudyGroup is not null)
            {
                var sharedPool = ClassesPool.Concat(ClassesAssigned).ToList();
                var assignedIDs = selectedStudyGroup.GetModel().StudyGroupSubjects.Select(x => x.SubjectID).ToList().Where(x => x.HasValue).ToList();

                ClassesPool.Clear();
                ClassesAssigned.Clear();
                foreach (var item in sharedPool)
                {
                    if (assignedIDs.Contains(item.GetModel().ID))
                    {
                        ClassesAssigned.Add(item);
                    }
                    else
                    {
                        ClassesPool.Add(item);
                    }
                }
                _loggingService.WriteLog(LogTitleConstants.SelectionSuccessfulTitle, LogActionType.GROUP_MANAGEMENT_GROUP_SELECTED, selectedStudyGroup.ShortName, ClassesAssigned.Count, ClassesPool.Count);
            }
        }

        public void Assign(object subject, StudyGroupViewModel selectedStudyGroup)
        {
            var assignedClass = subject as ClassViewModel;
            if (assignedClass is not null)
            {
                var groupModel = selectedStudyGroup.GetModel();
                groupModel.StudyGroupSubjects.Add(new StudyGroupSubject()
                {
                    StudyGroupID = groupModel.ID,
                    SubjectID = assignedClass.GetModel().ID,
                    StudyGroup = groupModel,
                    Subject = assignedClass.GetModel()
                });
                _studyGroupRepository.Update(groupModel);
                _loggingService.WriteLog(LogTitleConstants.AssignmentSuccessfulTitle, LogActionType.GROUP_MANAGEMENT_SUBJECT_ASSIGNED, selectedStudyGroup.ShortName, assignedClass.ShortName, ClassesAssigned.Count, ClassesPool.Count);
            }
        }


        public void Unassign(object subject, StudyGroupViewModel selectedStudyGroup)
        {
            var removedClass = subject as ClassViewModel;
            if (removedClass is not null)
            {
                var groupModel = selectedStudyGroup.GetModel();
                var removed = groupModel.StudyGroupSubjects.Where(x => x.SubjectID == removedClass.GetModel().ID).Select(x => x.ID).ToList();
                for(int i = 0; i < removed.Count; i++)
                {
                    var removedID = removed.ElementAt(i);
                    var removedSubject = groupModel.StudyGroupSubjects.SingleOrDefault(x => x.ID == removedID);
                    if(removedSubject is not null)
                    {
                        groupModel.StudyGroupSubjects.Remove(removedSubject);
                    }
                }
                _studyGroupRepository.Update(groupModel);
                _loggingService.WriteLog(LogTitleConstants.UnassignmentSuccessfulTitle, LogActionType.GROUP_MANAGEMENT_SUBJECT_UNASSIGNED, selectedStudyGroup.ShortName, removedClass.ShortName, ClassesAssigned.Count, ClassesPool.Count);
            }
        }
    }
}