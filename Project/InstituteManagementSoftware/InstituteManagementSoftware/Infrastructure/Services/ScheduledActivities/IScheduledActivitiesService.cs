﻿using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.ScheduledActivities
{
    public interface IScheduledActivitiesService
    {
        void Add(object param);
        void Update(object param);
        void Delete(object param);

        ObservableCollection<ScheduledActivityViewModel> ExtractSourceCollection();
        ScheduledActivityViewModel ExtractItemTemplate();
        ObservableCollection<EmployeeViewModel> ExtractAvailableEmployees();
        ObservableCollection<ClassViewModel> ExtractAvailableClasses();
        bool ValidateViewModel(ScheduledActivityViewModel viewModel);
    }
}
