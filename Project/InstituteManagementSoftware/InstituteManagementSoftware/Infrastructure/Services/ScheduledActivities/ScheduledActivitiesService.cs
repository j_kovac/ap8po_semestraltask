﻿using AutoMapper;
using InstituteManagementSoftware.Constants.Visualisation.Logging.Titles;
using InstituteManagementSoftware.Infrastructure.Repositories.Classes;
using InstituteManagementSoftware.Infrastructure.Repositories.Employees;
using InstituteManagementSoftware.Infrastructure.Repositories.ScheduledActivities;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.Models.Logs.Enums;
using InstituteManagementSoftware.Models.ScheduledActivities.Enums;
using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.ScheduledActivities
{
    public class ScheduledActivitiesService : IScheduledActivitiesService
    {
        private readonly IModelValidator<ScheduledActivityViewModel> _validator;

        private readonly IScheduledActivitiesRepository _scheduledActivitiesRepository;
        private readonly IEmployeesRepository _employeesRepository;
        private readonly IClassesRepository _classesRepository;

        private readonly ILoggingService _loggingService;
        private readonly IMapper _mapper;

        private readonly ObservableCollection<ScheduledActivityViewModel> ScheduledActivities;
        private readonly ScheduledActivityViewModel SingleItemTemplate;

        public ScheduledActivitiesService(IScheduledActivitiesRepository scheduledActivitiesRepository, ILoggingService loggingService,
            IEmployeesRepository employeesRepository, IClassesRepository classesRepository, IMapper mapper, IModelValidator<ScheduledActivityViewModel> validator)
        {
            _validator = validator;
            _scheduledActivitiesRepository = scheduledActivitiesRepository;
            _loggingService = loggingService;
            _employeesRepository = employeesRepository;
            _classesRepository = classesRepository;
            _employeesRepository = employeesRepository;
            _classesRepository = classesRepository;

            _mapper = mapper;

            ScheduledActivities = new ObservableCollection<ScheduledActivityViewModel>();
            SingleItemTemplate = new ScheduledActivityViewModel(new ScheduledActivity());

            InitItemTemplate();
            LoadCollection();
        }

        private void InitItemTemplate()
        {
            SingleItemTemplate.ActivityType = ActivityType.EXERCICE;
            SingleItemTemplate.HoursCount = 6;
            SingleItemTemplate.Language = Language.CZECH;
            SingleItemTemplate.Name = "Boring Activity";
            SingleItemTemplate.Note = "Just a very boring activity .jpg";
            SingleItemTemplate.StudentsCount = 30;
            SingleItemTemplate.WeeksCount = 14;
        }

        private void LoadCollection()
        {
            var scheduledActivities = _scheduledActivitiesRepository.GetAll(null);
            foreach (var scheduledActivity in scheduledActivities)
            {
                var activityModel = new ScheduledActivityViewModel(scheduledActivity) { IsChanged = false };
                ScheduledActivities.Add(activityModel);
            }
            _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.LOAD_SCHEDULEDACTIVITY, ScheduledActivities.Count);
        }

        public void Add(object param)
        {
            if (param is not null)
            {
                var newItem = param as ScheduledActivityViewModel;
                var model = PrepareInsert(newItem);

                var insertedItem = _scheduledActivitiesRepository.Insert(model);

                if (insertedItem is not null)
                {
                    ScheduledActivities.Add(new ScheduledActivityViewModel(insertedItem) { IsChanged = false });
                    _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.CREATE_SCHEDULEDACTIVITY, $"'{insertedItem.Name}'", insertedItem.ID);
                }
            }
        }

        private ScheduledActivity PrepareInsert(ScheduledActivityViewModel viewModel)
        {
            var model = viewModel.GetModel();
            var modelNew = _mapper.Map<ScheduledActivity>(model);

            modelNew.Employee = model.Employee;
            modelNew.Class = model.Class;

            return modelNew;
        }

        public void Delete(object param)
        {
            if (param is not null)
            {
                var deletedItem = param as ScheduledActivityViewModel;
                var id = deletedItem.GetModel().ID;

                _scheduledActivitiesRepository.Remove(id);
                ScheduledActivities.Remove(deletedItem);

                _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.DELETE_SCHEDULEDACTIVITY , id);
            }
        }
        public void Update(object param)
        {
            if (param is not null)
            {
                var updatedItem = param as ScheduledActivityViewModel;
                var updated = _scheduledActivitiesRepository.Update(updatedItem.GetModel());
                updatedItem.IsChanged = false;

                _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.UPDATE_SCHEDULEDACTIVITY, $"'{updated.Name}'", updated.ID);
            }
        }

        public ObservableCollection<ScheduledActivityViewModel> ExtractSourceCollection() => ScheduledActivities;
        public ScheduledActivityViewModel ExtractItemTemplate() => SingleItemTemplate;

        public ObservableCollection<EmployeeViewModel> ExtractAvailableEmployees()
        {
            var employees = _employeesRepository.GetAll(null);
            var list = new ObservableCollection<EmployeeViewModel>();
            foreach (var employee in employees)
            {
                var employeeModel = new EmployeeViewModel(employee) { IsChanged = false };
                list.Add(employeeModel);
            }
            return list;
        }

        public ObservableCollection<ClassViewModel> ExtractAvailableClasses()
        {
            var classes = _classesRepository.GetAll(null);
            var list = new ObservableCollection<ClassViewModel>();
            foreach (var subject in classes)
            {
                var classModel = new ClassViewModel(subject) { IsChanged = false };
                list.Add(classModel);
            }
            return list;
        }

        public bool ValidateViewModel(ScheduledActivityViewModel viewModel) => _validator.Validate(viewModel);
    }
}
