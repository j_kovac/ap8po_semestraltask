﻿using AutoMapper;
using InstituteManagementSoftware.Constants.Visualisation.Logging.Titles;
using InstituteManagementSoftware.Infrastructure.Repositories.Employees;
using InstituteManagementSoftware.Infrastructure.Repositories.StudyGroups;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.Models.Logs.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Models.StudyGroups.Enums;
using InstituteManagementSoftware.Models.StudyGroups.Models;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.StudyGroups
{
    public class StudyGroupService : IStudyGroupService
    {
        private readonly IStudyGroupsRepository _studyGroupsRepository;
        private readonly IEmployeesRepository _employeesRepository;

        private readonly ILoggingService _loggingService;
        private readonly IMapper _mapper;

        private readonly IModelValidator<StudyGroupViewModel> _validator;

        private readonly ObservableCollection<StudyGroupViewModel> StudyGroups;
        private readonly StudyGroupViewModel SingleItemTemplate;

        public StudyGroupService(IStudyGroupsRepository studyGroupsRepository, ILoggingService loggingService, IEmployeesRepository employeesRepository, IMapper mapper, IModelValidator<StudyGroupViewModel> validator)
        {
            _validator = validator;
            _studyGroupsRepository = studyGroupsRepository;
            _loggingService = loggingService;
            _employeesRepository = employeesRepository;
            _mapper = mapper;

            StudyGroups = new ObservableCollection<StudyGroupViewModel>();
            SingleItemTemplate = new StudyGroupViewModel(new StudyGroup());

            InitItemTemplate();
            LoadCollection();
        }

        private void InitItemTemplate()
        {
            SingleItemTemplate.Language = Language.CZECH;
            SingleItemTemplate.Name = "Lazy group n°1";
            SingleItemTemplate.Semester = Semester.SUMMER_SEMESTER;
            SingleItemTemplate.ShortName = "LGN1";
            SingleItemTemplate.StudentsMaxCount = 15;
            SingleItemTemplate.StudyForm = StudyForm.MASTER;
            SingleItemTemplate.StudyType = StudyType.PRESENTIAL;
            SingleItemTemplate.StudyYear = 4;
        }

        private void LoadCollection()
        {
            var studyGroups = _studyGroupsRepository.GetAll(null);
            foreach (var studyGroup in studyGroups)
            {
                var groupModel = new StudyGroupViewModel(studyGroup) { IsChanged = false };
                StudyGroups.Add(groupModel);
            }
            _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.LOAD_STUDYGROUP, StudyGroups.Count);
        }

        public void Add(object param)
        {
            if (param is not null)
            {
                var newItem = param as StudyGroupViewModel;
                var model = PrepareInsert(newItem);

                var insertedItem = _studyGroupsRepository.Insert(model);

                if (insertedItem is not null)
                {
                    StudyGroups.Add(new StudyGroupViewModel(insertedItem) { IsChanged = false });
                    _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.CREATE_STUDYGROUP, $"'{insertedItem.Name}'", insertedItem.ID);
                }
            }
        }

        private StudyGroup PrepareInsert(StudyGroupViewModel viewModel)
        {
            var model = viewModel.GetModel();
            var modelNew = _mapper.Map<StudyGroup>(model);

            modelNew.Garant = model.Garant;
            modelNew.StudyGroupSubjects.Clear();

            return modelNew;
        }

        public void Delete(object param)
        {
            if (param is not null)
            {
                var deletedItem = param as StudyGroupViewModel;
                var id = deletedItem.GetModel().ID;

                _studyGroupsRepository.Remove(id);
                StudyGroups.Remove(deletedItem);

                _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.DELETE_STUDYGROUP, id);
            }
        }

        public void Update(object param)
        {
            if (param is not null)
            {
                var updatedItem = param as StudyGroupViewModel;
                var updated = _studyGroupsRepository.Update(updatedItem.GetModel());
                updatedItem.IsChanged = false;

                _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.UPDATE_STUDYGROUP, $"'{updated.Name}'", updated.ID);
            }
        }

        public ObservableCollection<StudyGroupViewModel> ExtractSourceCollection() => StudyGroups;
        public StudyGroupViewModel ExtractItemTemplate() => SingleItemTemplate;

        public ObservableCollection<EmployeeViewModel> ExtractAvailableEmployees()
        {
            var employees = _employeesRepository.GetAll(null);
            var list = new ObservableCollection<EmployeeViewModel>();
            foreach (var employee in employees)
            {
                var employeeModel = new EmployeeViewModel(employee) { IsChanged = false };
                list.Add(employeeModel);
            }
            return list;
        }

        public bool ValidateViewModel(StudyGroupViewModel viewModel) => _validator.Validate(viewModel);
    }
}
