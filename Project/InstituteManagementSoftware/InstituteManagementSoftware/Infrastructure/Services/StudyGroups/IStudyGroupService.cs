﻿using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.StudyGroups
{
    public interface IStudyGroupService
    {
        void Add(object param);
        void Update(object param);
        void Delete(object param);

        ObservableCollection<StudyGroupViewModel> ExtractSourceCollection();
        StudyGroupViewModel ExtractItemTemplate();
        bool ValidateViewModel(StudyGroupViewModel viewModel);

        ObservableCollection<EmployeeViewModel> ExtractAvailableEmployees();
    }
}
