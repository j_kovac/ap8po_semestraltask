﻿using InstituteManagementSoftware.ViewModels.Config.Settings;

namespace InstituteManagementSoftware.Infrastructure.Services.ActionManager
{
    public interface ICommunicationService
    {
        void Export(object param);
        void Import(object param);
        void Save(object param);
        bool ValidateExport(SettingsViewModel viewModel);
        bool ValidateImport(SettingsViewModel viewModel);
        void InitializeSettings(SettingsViewModel settings);
    }
}
