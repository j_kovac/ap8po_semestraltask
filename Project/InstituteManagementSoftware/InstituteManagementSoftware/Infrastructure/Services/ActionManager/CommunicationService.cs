﻿using InstituteManagementSoftware.Constants.Application;
using InstituteManagementSoftware.Constants.Visualisation.Logging.Titles;
using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Infrastructure.Services.ActionManager.DataManipulation;
using InstituteManagementSoftware.Infrastructure.Services.ActionManager.Export;
using InstituteManagementSoftware.Infrastructure.Services.ActionManager.Import;
using InstituteManagementSoftware.Infrastructure.Services.Email;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Infrastructure.Validators.ImportExport;
using InstituteManagementSoftware.Models.Logs.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Config.Settings;

namespace InstituteManagementSoftware.Infrastructure.Services.ActionManager
{
    public class CommunicationService : ICommunicationService
    {
        private readonly IImportService _importService;
        private readonly IExportService _exportService;
        private readonly IEmailService _emailService;
        private readonly ILoggingService _loggingService;
        private readonly IDataManipulationService _dataManipulationService;

        private readonly ImportValidator _importValidator;
        private readonly ExportValidator _exportValidator;
        private SettingsViewModel Settings { get; set; }

        public CommunicationService(IImportService importService, IExportService exportService, IEmailService emailService, IDataManipulationService dataManipulationService, ILoggingService loggingService,
            ImportValidator importValidator, ExportValidator exportValidator)
        {
            _importService = importService;
            _exportService = exportService;
            _emailService = emailService;
            _loggingService = loggingService;
            _dataManipulationService = dataManipulationService;

            _importValidator = importValidator;
            _exportValidator = exportValidator;
        }

        public void InitializeSettings(SettingsViewModel settings) => Settings = settings;

        public void Export(object param)
        {
            var filePath = string.Empty;
            switch (Settings.ExportFormat)
            {
                case FileExportFormat.JSON:
                    filePath = _exportService.ExportToJSON();
                    break;
                case FileExportFormat.XML:
                    filePath = _exportService.ExportToXML();
                    break;
            }
            if (Settings.SendExportEmail && !string.IsNullOrWhiteSpace(filePath))
            {
                _emailService.SendExport(filePath, Settings.ExportEmail);
            }
        }

        public void Import(object param)
        {
            var filePath = Settings.ImportFilePath;
            DataContextRaw data = null;

            if (filePath.EndsWith(FileExportStringConstants.JSONFileSuffix))
            {
                data = _importService.ImportFromJSON();
            }
            else if (filePath.EndsWith(FileExportStringConstants.XMLFileSuffix))
            {
                data = _importService.ImportFromXML();
            }

            if(data is not null)
            {
                Settings.GetModel().ImportedDatabase = data;
                Settings.AreImportChangesSaved = false;
            }
        }

        public void Save(object param)
        {
            var success = _dataManipulationService.ImportDatabase(Settings.GetModel().ImportedDatabase);
            if (success)
            {
                Settings.AreImportChangesSaved = true;
                _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.SAVE_SUCCESS);
            }
            else
            {
                _loggingService.WriteLog(LogTitleConstants.ActionFailedTitle, LogActionType.SAVE_FAILED);
            }
        }

        public bool ValidateExport(SettingsViewModel viewModel) => _exportValidator.Validate(viewModel);
        public bool ValidateImport(SettingsViewModel viewModel) => _importValidator.Validate(viewModel);
    }
}
