﻿using InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings.QuickActions;
using InstituteManagementSoftware.Constants.Visualisation.Logging.Titles;
using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Models.Logs.Enums;
using InstituteManagementSoftware.Models.Shared.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Xml.Serialization;

namespace InstituteManagementSoftware.Infrastructure.Services.ActionManager.Import
{
    public class ImportService : IImportService
    {
        private readonly ILoggingService _loggingService;
        private readonly SharedSettings _settings;

        public ImportService(ILoggingService loggingService, SharedSettings settings)
        {
            _loggingService = loggingService;
            _settings = settings;
        }

        public DataContextRaw ImportFromJSON()
        {
            try
            {
                using var streamReader = new StreamReader(_settings.ImportFilePath);
                var jsonRead = streamReader.ReadToEnd();
                var dataRaw = JsonConvert.DeserializeObject<DataContextRaw>(jsonRead);

                if(dataRaw is not null)
                { _loggingService.WriteLog(LogTitleConstants.ImportFinishedTitle, LogActionType.IMPORT_SUCCESS, $"'{_settings.ImportFilePath}'"); }
                else
                { _loggingService.WriteLog(LogTitleConstants.ImportFailedTitle, LogActionType.IMPORT_FAILED, $"'{_settings.ImportFilePath}'"); }

                return dataRaw;
            }
            catch (Exception)
            {
                _loggingService.WriteLog(LogTitleConstants.ImportFailedTitle, LogActionType.IMPORT_FAILED, $"'{_settings.ImportFilePath}'");
                return null;
            }
        }

        public DataContextRaw ImportFromXML()
        {
            try
            {
                var serializer = new XmlSerializer(typeof(DataContextRaw));
                using var streamReader = new StreamReader(_settings.ImportFilePath);
                var dataRaw = (DataContextRaw)serializer.Deserialize(streamReader);

                if (dataRaw is not null)
                { _loggingService.WriteLog(LogTitleConstants.ImportFinishedTitle, LogActionType.IMPORT_SUCCESS, $"'{_settings.ImportFilePath}'"); }
                else
                { _loggingService.WriteLog(LogTitleConstants.ImportFailedTitle, LogActionType.IMPORT_FAILED, $"'{_settings.ImportFilePath}'"); }

                return dataRaw;
            }
            catch (Exception)
            {
                _loggingService.WriteLog(LogTitleConstants.ImportFailedTitle, LogActionType.IMPORT_FAILED, $"'{_settings.ImportFilePath}'");
                return null;
            }
        }
    }
}
