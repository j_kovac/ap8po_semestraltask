﻿using InstituteManagementSoftware.Infrastructure.Data;

namespace InstituteManagementSoftware.Infrastructure.Services.ActionManager.Import
{
    public interface IImportService
    {
        DataContextRaw ImportFromJSON();
        DataContextRaw ImportFromXML();
    }
}
