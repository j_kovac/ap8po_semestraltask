﻿using InstituteManagementSoftware.Infrastructure.Data;

namespace InstituteManagementSoftware.Infrastructure.Services.ActionManager.DataManipulation
{
    public interface IDataManipulationService
    {
        bool ImportDatabase(DataContextRaw dataLoaded);
    }
}
