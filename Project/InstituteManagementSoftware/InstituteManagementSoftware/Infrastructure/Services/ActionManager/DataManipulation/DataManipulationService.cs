﻿using InstituteManagementSoftware.Constants.Application;
using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace InstituteManagementSoftware.Infrastructure.Services.ActionManager.DataManipulation
{
    public class DataManipulationService : IDataManipulationService
    {

        private readonly DataContext _dbContext;

        public DataManipulationService(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        private void RemoveAllCollections()
        {
            _dbContext.Classes.RemoveRange(_dbContext.Classes);
            _dbContext.Employees.RemoveRange(_dbContext.Employees);
            _dbContext.ScheduledActivities.RemoveRange(_dbContext.ScheduledActivities);
            _dbContext.StudyGroups.RemoveRange(_dbContext.StudyGroups);
            _dbContext.StudyGroupSubjects.RemoveRange(_dbContext.StudyGroupSubjects);

            _dbContext.SaveChanges();
        }

        private void InsertFullTable<T>(IList<T> entities, string tableName) where T : Entity
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                _dbContext.Database.ExecuteSqlRaw(string.Format(DatabaseQueriesConstants.IdentityAddAllow, tableName));
                _dbContext.Set<T>().AddRange(entities);
                _dbContext.SaveChanges();
                transaction.Commit();
            }
            _dbContext.Database.ExecuteSqlRaw(string.Format(DatabaseQueriesConstants.IdentityAddDeny, tableName));
        }

        public bool ImportDatabase(DataContextRaw dataLoaded)
        {
            try
            {
                RemoveAllCollections();

                InsertFullTable(dataLoaded.Classes, nameof(_dbContext.Classes));
                InsertFullTable(dataLoaded.Employees, nameof(_dbContext.Employees));
                InsertFullTable(dataLoaded.StudyGroups, nameof(_dbContext.StudyGroups));
                InsertFullTable(dataLoaded.ScheduledActivities, nameof(_dbContext.ScheduledActivities));
                InsertFullTable(dataLoaded.StudyGroupSubjects, nameof(_dbContext.StudyGroupSubjects));

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
    }
}
