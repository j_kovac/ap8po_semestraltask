﻿using InstituteManagementSoftware.Constants.Application;
using InstituteManagementSoftware.Constants.Visualisation.Logging.Titles;
using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Models.Logs.Enums;
using InstituteManagementSoftware.Models.Shared.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace InstituteManagementSoftware.Infrastructure.Services.ActionManager.Export
{
    public class ExportService : IExportService
    {
        private readonly ILoggingService _loggingService;
        private readonly DataContext _dataContext;

        private readonly SharedSettings _settings;

        public ExportService(ILoggingService loggingService, DataContext dataContext, SharedSettings settings)
        {
            _loggingService = loggingService;
            _dataContext = dataContext;

            _settings = settings;
        }

        public string ExportToJSON()
        {
            var dbModel = GetFullDataModel();
            var settings = new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            var serializer = JsonSerializer.Create(settings);
            var filePath = $"{ _settings.ExportFilePath}\\{FileExportStringConstants.ExportedFilename}.{FileExportStringConstants.ExportedJSONSuffix}";

            try
            {
                using var stream = new StreamWriter(filePath);
                using var writer = new JsonTextWriter(stream);
                serializer.Serialize(writer, dbModel);

                _loggingService.WriteLog(LogTitleConstants.ExportFinishedTitle, LogActionType.EXPORT_SUCCESS, $"'{filePath}'", FileExportStringConstants.ExportedJSONSuffix);

                return filePath;
            }
            catch (Exception)
            {
                _loggingService.WriteLog(LogTitleConstants.ExportFailedTitle, LogActionType.EXPORT_FAILED, $"'{filePath}'");
                return string.Empty;
            }
        }

        public string ExportToXML()
        {
            var dbModel = GetFullDataModel();
            var serializer = new XmlSerializer(typeof(DataContextRaw));
            var filePath = $"{ _settings.ExportFilePath}\\{FileExportStringConstants.ExportedFilename}.{FileExportStringConstants.ExportedXMLSuffix}";

            try
            {
                using var stream = File.Create(filePath);
                using var writer = XmlWriter.Create(stream);
                serializer.Serialize(writer, dbModel);

                _loggingService.WriteLog(LogTitleConstants.ExportFinishedTitle, LogActionType.EXPORT_SUCCESS, $"'{filePath}'", FileExportStringConstants.JSONFileSuffix.ToUpper());

                return filePath;
            }
            catch (Exception)
            {
                _loggingService.WriteLog(LogTitleConstants.ExportFailedTitle, LogActionType.EXPORT_FAILED, $"'{filePath}'", FileExportStringConstants.ExportedXMLSuffix.ToUpper());
                return string.Empty;
            }
        }

        private DataContextRaw GetFullDataModel()
        {
            var model = new DataContextRaw()
            {
                Classes = _dataContext.Classes.ToList(),
                Employees = _dataContext.Employees.ToList(),
                ScheduledActivities = _dataContext.ScheduledActivities.ToList(),
                StudyGroups = _dataContext.StudyGroups.ToList(),
                StudyGroupSubjects = _dataContext.StudyGroupSubjects.ToList()
            };

            return model;
        }
    }
}
