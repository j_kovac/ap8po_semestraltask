﻿namespace InstituteManagementSoftware.Infrastructure.Services.ActionManager.Export
{
    public interface IExportService
    {
        string ExportToJSON();
        string ExportToXML();
    }
}
