﻿using InstituteManagementSoftware.Models.Logs.models;
using System;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.Logs
{
    public class LoggingService : ILoggingService
    {
        private readonly ObservableCollection<Log> _logs;

        public LoggingService()
        {
            _logs = new ObservableCollection<Log>();
        }

        public void WriteLog(string title, Enum action, params object[] arguments)
        {
            var log = new Log()                                                             
            {
                Title = title,                                                              
                Action = new Tuple<Enum, object[]>(action, arguments),                  
                StartTime = DateTime.Now                                                    
            };
            ClearLogs();
            _logs.Add(log);    
        }

        public void ClearLogs() => _logs.Clear();

        public ObservableCollection<Log> GetLogs() => _logs;
    }
}
