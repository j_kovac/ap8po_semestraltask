﻿using InstituteManagementSoftware.Models.Logs.models;
using System;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.Logs
{
    public interface ILoggingService
    {
        void WriteLog(string title, Enum action, params object[] arguments);
        void ClearLogs();
        ObservableCollection<Log> GetLogs();
    }
}
