﻿using InstituteManagementSoftware.ViewModels.Partial.Classes;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.Classes
{
    public interface IClassService
    {
        void Add(object param);
        void Update(object param);
        void Delete(object param);

        ObservableCollection<ClassViewModel> ExtractSourceCollection();
        ClassViewModel ExtractItemTemplate();
        bool ValidateViewModel(ClassViewModel viewModel);
    }
}
