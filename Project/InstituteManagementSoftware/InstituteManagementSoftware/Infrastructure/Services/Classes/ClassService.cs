﻿using AutoMapper;
using InstituteManagementSoftware.Constants.Visualisation.Logging.Titles;
using InstituteManagementSoftware.Infrastructure.Repositories.Classes;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.Models.Classes.Enums;
using InstituteManagementSoftware.Models.Classes.Models;
using InstituteManagementSoftware.Models.Logs.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using System.Collections.ObjectModel;
using System.Linq;

namespace InstituteManagementSoftware.Infrastructure.Services.Classes
{
    public class ClassService : IClassService
    {
        private readonly IClassesRepository _classesRepository;
        private readonly IModelValidator<ClassViewModel> _validator;

        private readonly ILoggingService _loggingService;
        private readonly IMapper _mapper;

        private readonly ObservableCollection<ClassViewModel> Classes;
        private readonly ClassViewModel SingleItemTemplate;

        public ClassService(IClassesRepository classesRepository, ILoggingService loggingService, IMapper mapper, IModelValidator<ClassViewModel> validator)
        {
            _classesRepository = classesRepository;
            _validator = validator;
            _loggingService = loggingService;
            _mapper = mapper;

            Classes = new ObservableCollection<ClassViewModel>();
            SingleItemTemplate = new ClassViewModel(new Subject());

            InitItemTemplate();
            LoadCollection();
        }

        private void InitItemTemplate()
        {
            SingleItemTemplate.ClassSize = 20;
            SingleItemTemplate.SeminarsHoursCount = 2;
            SingleItemTemplate.LectureHoursCount = 3;
            SingleItemTemplate.ExercicesHoursCount = 1;
            SingleItemTemplate.Credits = 6;
            SingleItemTemplate.EndingType = EndingType.EXAM;
            SingleItemTemplate.Department = Department.ICT_TECHNOLOGY_PARK;
            SingleItemTemplate.FullName = "Dank Class";
            SingleItemTemplate.Language = Language.ENGLISH;
            SingleItemTemplate.LengthWeeks = 14;
            SingleItemTemplate.ShortName = "DC";
        }

        private void LoadCollection()
        {
            var classes = _classesRepository.GetAll(null);
            foreach(var subject in classes){
                var classModel = new ClassViewModel(subject){IsChanged = false};
                Classes.Add(classModel);
            }
            _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.LOAD_CLASS, Classes.Count);
        }

        public void Add(object param)
        {
            if (param is not null)
            {
                var newItem = param as ClassViewModel;
                var model = PrepareInsert(newItem);
                var insertedItem = _classesRepository.Insert(model);

                if(insertedItem is not null)
                {
                    Classes.Add(new ClassViewModel(insertedItem) { IsChanged = false });
                    _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.CREATE_CLASS, $"'{insertedItem.ShortName}'", insertedItem.ID);
                }
            }
        }

        private Subject PrepareInsert(ClassViewModel viewModel)
        {
            var model = viewModel.GetModel();
            var modelNew = _mapper.Map<Subject>(model);

            modelNew.ScheduledActivities.Clear();
            modelNew.StudyGroupSubjects.Clear();

            return modelNew;
        }

        public void Delete(object param)
        {
            if (param is not null)
            {
                var deletedItem = param as ClassViewModel;
                var id = deletedItem.GetModel().ID;

                _classesRepository.Remove(id);
                Classes.Remove(deletedItem);

                _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.DELETE_CLASS, id);
            }
        }

        public void Update(object param)
        {
            if(param is not null)
            {
                var updatedItem = param as ClassViewModel;
                var updated = _classesRepository.Update(updatedItem.GetModel());
                updatedItem.IsChanged = false;

                _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.UPDATE_CLASS, $"'{updated.ShortName}'", updated.ID);
            }
        }

        public ObservableCollection<ClassViewModel> ExtractSourceCollection() => Classes;
        public ClassViewModel ExtractItemTemplate() => SingleItemTemplate;
        public bool ValidateViewModel(ClassViewModel viewModel) => _validator.Validate(viewModel);
    }
}
