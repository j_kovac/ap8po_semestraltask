﻿using InstituteManagementSoftware.ViewModels.Partial.Employees;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.Employees
{
    public interface IEmployeeService
    {
        void Add(object param);
        void Update(object param);
        void Delete(object param);

        ObservableCollection<EmployeeViewModel> ExtractSourceCollection();
        EmployeeViewModel ExtractItemTemplate();
        bool ValidateViewModel(EmployeeViewModel viewModel);
    }
}
