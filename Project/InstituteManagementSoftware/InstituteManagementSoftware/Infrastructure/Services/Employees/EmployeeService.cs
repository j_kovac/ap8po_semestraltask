﻿using AutoMapper;
using InstituteManagementSoftware.Constants.Visualisation.Logging.Titles;
using InstituteManagementSoftware.Infrastructure.Repositories.Employees;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.Models.Employees.Enums;
using InstituteManagementSoftware.Models.Employees.Models;
using InstituteManagementSoftware.Models.Logs.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using System;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.Infrastructure.Services.Employees
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IModelValidator<EmployeeViewModel> _validator;
        private readonly IEmployeesRepository _employeesRepository;

        private readonly ILoggingService _loggingService;
        private readonly IMapper _mapper;

        private readonly ObservableCollection<EmployeeViewModel> Employees;
        private readonly EmployeeViewModel SingleItemTemplate;

        public EmployeeService(IEmployeesRepository employeesRepository, ILoggingService loggingService, IMapper mapper, IModelValidator<EmployeeViewModel> validator)
        {
            _validator = validator;
            _employeesRepository = employeesRepository;

            _loggingService = loggingService;
            _mapper = mapper;

            Employees = new ObservableCollection<EmployeeViewModel>();
            SingleItemTemplate = new EmployeeViewModel(new Employee());

            InitItemTemplate();
            LoadCollection();
        }

        private void InitItemTemplate()
        {
            SingleItemTemplate.FirstName = "Ghenghis";
            SingleItemTemplate.LastName = "Khan";
            SingleItemTemplate.BirthDate = DateTime.Now.AddYears(-30);
            SingleItemTemplate.Department = Department.DEPARTMENT_OF_ELECTRONICS_AND_MESUREMENTS;
            SingleItemTemplate.Email = "Kill0r_420@urmom.com";
            SingleItemTemplate.EmployeeType = EmployeeType.CUSTOM;
            SingleItemTemplate.IsDocStudent = true;
            SingleItemTemplate.Note = "Lazy boi";
            SingleItemTemplate.Phone = "+420 124 666 69";
            SingleItemTemplate.Salary = 20000.15;
            SingleItemTemplate.WorkPercentage = 85;
        }

        private void LoadCollection()
        {
            var employees = _employeesRepository.GetAll(null);
            foreach (var employee in employees)
            {
                var employeeModel = new EmployeeViewModel(employee) { IsChanged = false };
                Employees.Add(employeeModel);
            }
            _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.LOAD_EMPLOYEE, Employees.Count);
        }

        public void Add(object param)
        {
            if (param is not null)
            {
                var newItem = param as EmployeeViewModel;
                var model = PrepareInsert(newItem);
                var insertedItem = _employeesRepository.Insert(model);

                if (insertedItem is not null)
                {
                    var newEmployee = new EmployeeViewModel(insertedItem) { IsChanged = false };
                    Employees.Add(newEmployee);
                    _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.CREATE_EMPLOYEE, $"'{newEmployee.FullName}'", insertedItem.ID);
                }
            }
        }

        private Employee PrepareInsert(EmployeeViewModel viewModel)
        {
            var model = viewModel.GetModel();
            var modelNew = _mapper.Map<Employee>(model);

            modelNew.ScheduledActivities.Clear();
            modelNew.StudyGroups.Clear();

            return modelNew;
        }

        public void Delete(object param)
        {
            if(param is not null)
            {
                var deletedItem = param as EmployeeViewModel;
                var id = deletedItem.GetModel().ID;

                _employeesRepository.Remove(id);
                Employees.Remove(deletedItem);

                _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.DELETE_EMPLOYEE, id);
            }
        }

        public void Update(object param)
        {
            if (param is not null)
            {
                var updatedItem = param as EmployeeViewModel;
                var updated = _employeesRepository.Update(updatedItem.GetModel());
                updatedItem.IsChanged = false;

                _loggingService.WriteLog(LogTitleConstants.ActionFinishedTitle, LogActionType.UPDATE_EMPLOYEE, $"'{updatedItem.FullName}'", updated.ID);
            }
        }

        public ObservableCollection<EmployeeViewModel> ExtractSourceCollection() => Employees;
        public EmployeeViewModel ExtractItemTemplate() => SingleItemTemplate;
        public bool ValidateViewModel(EmployeeViewModel viewModel) => _validator.Validate(viewModel);
    }
}
