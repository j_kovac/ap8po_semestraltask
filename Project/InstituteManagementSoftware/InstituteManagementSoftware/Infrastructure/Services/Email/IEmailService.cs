﻿namespace InstituteManagementSoftware.Infrastructure.Services.Email
{
    public interface IEmailService
    {
        void SendExport(string filePath, string recipientAddress);
    }
}
