﻿using InstituteManagementSoftware.Constants.Application;
using InstituteManagementSoftware.Constants.Visualisation.Logging.Titles;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Models.Logs.Enums;
using System;
using System.Net;
using System.Net.Mail;

namespace InstituteManagementSoftware.Infrastructure.Services.Email
{
    public class EmailService : IEmailService
    {
        private readonly ILoggingService _loggingService;

        public EmailService(ILoggingService loggingService)
        {
            _loggingService = loggingService;
        }

        public void SendExport(string filePath, string recipientAddress)
        {
            try
            {
                var message = new MailMessage()
                {
                    From = new MailAddress(EmailSettingConstants.EmailAddress),
                    Subject = EmailSettingConstants.Subject,
                    Body = EmailSettingConstants.Body
                };
                message.To.Add(recipientAddress);
                message.Attachments.Add(new Attachment(filePath));


                var server = new SmtpClient(EmailSettingConstants.StmpGmailAddress)
                {
                    Port = EmailSettingConstants.SmtpPort,
                    EnableSsl = true,
                    UseDefaultCredentials = false
                };
                server.Credentials = new NetworkCredential(EmailSettingConstants.EmailAddress, EmailSettingConstants.EmailPassword);
                server.Send(message);

                _loggingService.WriteLog(LogTitleConstants.ConnectionSuccessfulTitle, LogActionType.EMAIL_SUCCESS, $"'{recipientAddress}'",  $"'{filePath}'");
            }
            catch (Exception)
            {
                _loggingService.WriteLog(LogTitleConstants.ConnectionFailedTitle, LogActionType.EMAIL_FAILED, $"'{recipientAddress}'");
            }
        }
    }
}
