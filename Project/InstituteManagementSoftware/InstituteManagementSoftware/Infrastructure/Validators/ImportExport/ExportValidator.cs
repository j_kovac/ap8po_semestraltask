﻿using InstituteManagementSoftware.Constants.Validation.ImportExport;
using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Config.Settings;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace InstituteManagementSoftware.Infrastructure.Validators.ImportExport
{
    public class ExportValidator : ISharedSettingsValidator
    {
        public bool Validate(SettingsViewModel viewModel)
        {
            var fileNameV = IsFileNameValid(viewModel.ExportFilePath);
            var formatV = IsFormatValid(viewModel.ExportFormat);
            var emailV = IsEmailValid(viewModel.ExportEmail, viewModel.SendExportEmail);

            return fileNameV && formatV && emailV;
        }

        public static bool IsEmailValid(string exportEmail, bool sendExportEmail)
        {
            var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            return !sendExportEmail || !string.IsNullOrEmpty(exportEmail) &&
                exportEmail.Length <= ExportValidationConstants.EmailLenghtMax &&
                regex.IsMatch(exportEmail);
        }

        public static bool IsFormatValid(FileExportFormat exportFormat) => Enum.IsDefined(exportFormat);

        public static bool IsFileNameValid(string exportFilePath) => Directory.Exists(exportFilePath);
    }
}
