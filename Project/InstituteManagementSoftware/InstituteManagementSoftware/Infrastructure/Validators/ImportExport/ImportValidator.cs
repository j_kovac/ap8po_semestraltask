﻿using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.ViewModels.Config.Settings;
using System.IO;

namespace InstituteManagementSoftware.Infrastructure.Validators.ImportExport
{
    public class ImportValidator : ISharedSettingsValidator
    {
        public bool Validate(SettingsViewModel viewModel)
        {
            var fileNameV = IsFileNameValid(viewModel.ImportFilePath);

            return fileNameV;
        }

        public static bool IsFileNameValid(string importFilePath) => File.Exists(importFilePath);
    }
}
