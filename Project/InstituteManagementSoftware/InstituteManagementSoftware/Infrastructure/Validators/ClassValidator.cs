﻿using InstituteManagementSoftware.Constants.Validation;
using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.Models.Classes.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Utils.Extensions;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using System;
using System.Text.RegularExpressions;

namespace InstituteManagementSoftware.Infrastructure.Validators
{
    public class ClassValidator : IModelValidator<ClassViewModel>
    {
        public bool Validate(ClassViewModel viewModel)
        {
            var fullNameV = IsFullNameValid(viewModel.FullName);
            var shortNameV = IsShortNameValid(viewModel.ShortName);
            var lengthWeeksV = IsLengthWeeksValid(viewModel.LengthWeeks);
            var classSizeV = IsClassSizeValid(viewModel.ClassSize);
            var creditsV = IsCreditsValid(viewModel.Credits);
            var exercicesHoursCountV = IsExcercicesHoursCountValid(viewModel.ExercicesHoursCount);
            var seminarsHoursCountV = IsSeminarsHoursCountValid(viewModel.SeminarsHoursCount);
            var lectureHoursCountV = IsLectureHoursCountValid(viewModel.LectureHoursCount);
            var endingTypeV = IsEndingTypeValid(viewModel.EndingType);
            var departmentV = IsDepartmentValid(viewModel.Department);
            var languageV = IsLanguageValid(viewModel.Language);

            return fullNameV && shortNameV && lengthWeeksV && classSizeV &&
                creditsV && exercicesHoursCountV && seminarsHoursCountV &&
                lectureHoursCountV && endingTypeV && departmentV && languageV;
        }

        public static bool IsLanguageValid(Language language) => Enum.IsDefined(language);

        public static bool IsDepartmentValid(Department department) => Enum.IsDefined(department);

        public static bool IsEndingTypeValid(EndingType endingType) => Enum.IsDefined(endingType);

        public static bool IsLectureHoursCountValid(int lectureHoursCount) => 
            lectureHoursCount >= ClassValidationConstants.LectureHoursMin &&
            lectureHoursCount <= ClassValidationConstants.LectureHoursMax;

        public static bool IsSeminarsHoursCountValid(int seminarsHoursCount) => 
            seminarsHoursCount >= ClassValidationConstants.SeminarHoursMin &&
            seminarsHoursCount <= ClassValidationConstants.SeminarHoursMax;

        public static bool IsExcercicesHoursCountValid(int exercicesHoursCount) => 
            exercicesHoursCount >= ClassValidationConstants.ExerciceHoursMin &&
            exercicesHoursCount <= ClassValidationConstants.ExerciceHoursMax;

        public static bool IsCreditsValid(int credits) => 
            credits >= ClassValidationConstants.CreditsMin &&
            credits <= ClassValidationConstants.CreditsMax;

        public static bool IsClassSizeValid(int classSize) =>
            classSize >= ClassValidationConstants.ClassSizeMin &&
            classSize <= ClassValidationConstants.ClassSizeMax;

        public static bool IsLengthWeeksValid(int lengthWeeks) =>
            lengthWeeks >= ClassValidationConstants.LengthWeeksMin &&
            lengthWeeks <= ClassValidationConstants.LengthWeeksMax;
        
        public static bool IsShortNameValid(string shortName)
        {
            var alphanumeric = new Regex(@"^[0-9A-Za-z ]+$");

            return
                !string.IsNullOrEmpty(shortName) &&
                alphanumeric.IsMatch(shortName.RemoveDiacritics()) &&
                shortName.Length >= ClassValidationConstants.ShortNameLengthMin &&
                shortName.Length <= ClassValidationConstants.ShortNameLengthMax;
        }

        public static bool IsFullNameValid(string fullName)
        {
            var alphanumeric = new Regex(@"^[0-9A-Za-z ]+$");

            return
                !string.IsNullOrEmpty(fullName) &&
                alphanumeric.IsMatch(fullName.RemoveDiacritics()) &&
                fullName.Length >= ClassValidationConstants.NameLengthMin &&
                fullName.Length <= ClassValidationConstants.NameLengthMax;
        }
    }
}
