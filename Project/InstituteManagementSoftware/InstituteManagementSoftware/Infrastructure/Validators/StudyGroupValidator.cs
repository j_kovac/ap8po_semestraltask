﻿using InstituteManagementSoftware.Constants.Validation;
using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Models.StudyGroups.Enums;
using InstituteManagementSoftware.Utils.Extensions;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using System;
using System.Text.RegularExpressions;

namespace InstituteManagementSoftware.Infrastructure.Validators
{
    public class StudyGroupValidator : IModelValidator<StudyGroupViewModel>
    {

        public bool Validate(StudyGroupViewModel viewModel)
        {
            var nameV = IsNameValid(viewModel.Name);
            var shortNameV = IsShortNameValid(viewModel.ShortName);
            var studyYearV = IsStudyYearValid(viewModel.StudyYear, viewModel.StudyForm);
            var studentYearMaxCountV = IsStudentsMaxCountValid(viewModel.StudentsMaxCount);
            var studyTypeV = IsStudyTypeValid(viewModel.StudyType);
            var semesterV = IsSemesterValid(viewModel.Semester);
            var languageV = IsLanguageValid(viewModel.Language);
            var studyFormV = IsStudyFormValid(viewModel.StudyForm);
            var teacherV = IsTeacherValid(viewModel.Garant);

            return nameV && shortNameV && studyYearV && studentYearMaxCountV &&
                studyTypeV && semesterV && languageV && studyFormV && teacherV;
        }

        public static bool IsNameValid(string name)
        {
            return
                !string.IsNullOrEmpty(name) &&
                name.Length >= StudyGroupValidationConstants.NameLengthMin &&
                name.Length <= StudyGroupValidationConstants.NameLengthMax;
        }


        public static bool IsShortNameValid(string shortName)
        {
            var alphanumeric = new Regex(@"^[0-9A-Za-z ]+$");

            return
                !string.IsNullOrEmpty(shortName) &&
                alphanumeric.IsMatch(shortName.RemoveDiacritics()) &&
                shortName.Length >= StudyGroupValidationConstants.ShortNameLengthMin &&
                shortName.Length <= StudyGroupValidationConstants.ShortNameLengthMax;
        }

        public static bool IsStudyYearValid(int studyYear, StudyForm studyForm)
        {
            int minYear, maxYear;

            switch (studyForm)
            {
                case StudyForm.BACHELOR:
                    minYear = StudyGroupValidationConstants.StudyYearMinBachelor;
                    maxYear = StudyGroupValidationConstants.StudyYearMaxBachelor;
                    break;
                case StudyForm.MASTER:
                    minYear = StudyGroupValidationConstants.StudyYearMinMaster;
                    maxYear = StudyGroupValidationConstants.StudyYearMaxMaster;
                    break;
                case StudyForm.DOCTORAL:
                    minYear = StudyGroupValidationConstants.StudyYearMinDoctorial;
                    maxYear = StudyGroupValidationConstants.StudyYearMaxDoctorial;
                    break;
                default:
                    minYear = int.MaxValue;
                    maxYear = int.MinValue;
                    break;
            }
            return
                studyYear >= minYear && studyYear <= maxYear;
        }

        public static bool IsStudentsMaxCountValid(int studyYear) => 
            studyYear >= StudyGroupValidationConstants.StudentsMaxCountMin &&
            studyYear <= StudyGroupValidationConstants.StudentsMaxCountMax;

        public static bool IsStudyTypeValid(StudyType studyType) => Enum.IsDefined(studyType);

        public static bool IsSemesterValid(Semester semester) => Enum.IsDefined(semester);

        public static bool IsLanguageValid(Language language) => Enum.IsDefined(language);

        public static bool IsStudyFormValid(StudyForm studyForm) => Enum.IsDefined(studyForm);

        public static bool IsTeacherValid(EmployeeViewModel garant) => garant.GetModel() is not null;
    }
}
