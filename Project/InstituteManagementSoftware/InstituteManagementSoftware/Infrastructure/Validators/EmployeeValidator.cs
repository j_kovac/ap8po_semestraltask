﻿using InstituteManagementSoftware.Constants.Validation;
using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.Models.Employees.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Utils.Extensions;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using System;
using System.Text.RegularExpressions;

namespace InstituteManagementSoftware.Infrastructure.Validators
{
    public class EmployeeValidator : IModelValidator<EmployeeViewModel>
    {
        public bool Validate(EmployeeViewModel viewModel)
        {
            var firstNameV = IsFirstNameValid(viewModel.FirstName);
            var lastNameV = IsLastNameValid(viewModel.LastName);
            var birthDateV = IsBirthDateValid(viewModel.BirthDate);
            var emailV = IsEmailValid(viewModel.Email);
            var phoneV = IsPhoneValid(viewModel.Phone);
            var isDocStudentV = IsDocStudentValid(viewModel.IsDocStudent, viewModel.EmployeeType);
            var departmentV = IsDepartmentValid(viewModel.Department);
            var salaryV = IsSalaryValid(viewModel.Salary);
            var noteV = IsNoteValid(viewModel.Note);
            var employeeTypeV = IsEmployeeTypeValid(viewModel.EmployeeType, viewModel.IsDocStudent);
            var workPercentageV = IsWorkPercentageValid(viewModel.WorkPercentage, viewModel.EmployeeType);

            return firstNameV && lastNameV && birthDateV && emailV &&
                phoneV && isDocStudentV && departmentV && salaryV &&
                noteV && employeeTypeV && workPercentageV;
        }

        public static bool IsWorkPercentageValid(double? workPercentage, EmployeeType employeeType)
        {
            if(employeeType != EmployeeType.CUSTOM && workPercentage is null)
            {
                return true;
            }
            else
            {
                double minPercentage, maxPercentage;
                minPercentage = maxPercentage = 0;

                switch (employeeType)
                {
                    case EmployeeType.FULL_TIME:
                        minPercentage = maxPercentage = EmployeeValidationConstants.FullTimeWorkPercentage;
                        break;
                    case EmployeeType.PART_TIME:
                        minPercentage = maxPercentage = EmployeeValidationConstants.PartTimeWorkPercentage;
                        break;
                    case EmployeeType.CUSTOM:
                        minPercentage = EmployeeValidationConstants.CustomWorkPercentageMin;
                        maxPercentage = EmployeeValidationConstants.CustomWorkPercentageMax;
                        break;
                }
                return
                    workPercentage >= minPercentage &&
                    workPercentage <= maxPercentage;
            }
        }

        public static bool IsEmployeeTypeValid(EmployeeType employeeType, bool isDocStudent) =>
            (!isDocStudent || employeeType == EmployeeType.CUSTOM) && 
            Enum.IsDefined(employeeType);

        public static bool IsNoteValid(string note) => 
            string.IsNullOrEmpty(note) ||
            (note.Length > 0 && !string.IsNullOrWhiteSpace(note));

        public static bool IsSalaryValid(double salary) =>
            salary >= EmployeeValidationConstants.SalaryMin &&
            salary <= EmployeeValidationConstants.SalaryMax;

        public static bool IsDepartmentValid(Department department) =>
            Enum.IsDefined(department);

        public static bool IsDocStudentValid(bool isDocStudent, EmployeeType employeeType) =>
            !isDocStudent || employeeType == EmployeeType.CUSTOM;

        public static bool IsPhoneValid(string phone)
        {
            var regex = new Regex(@"\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$");

            return !string.IsNullOrEmpty(phone) &&
                regex.IsMatch(phone.Replace(" ", string.Empty)) &&
                phone.Length <= EmployeeValidationConstants.PhoneLengthMax;
        }

        public static bool IsEmailValid(string email)
        {
            var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            return !string.IsNullOrEmpty(email) &&
                email.Length <= EmployeeValidationConstants.EmailLenghtMax &&
                regex.IsMatch(email);
        }

        public static bool IsBirthDateValid(DateTime birthDate)
        {
            return birthDate <= DateTime.Now.AddYears(-EmployeeValidationConstants.AgeYearsMin) &&
                birthDate >= DateTime.Now.AddYears(-EmployeeValidationConstants.AgeYearsMax);
        }

        public static bool IsLastNameValid(string lastName)
        {
            var alphanumeric = new Regex(@"^[0-9A-Za-z ]+$");

            return
                !string.IsNullOrEmpty(lastName) &&
                alphanumeric.IsMatch(lastName.RemoveDiacritics()) &&
                lastName.Length >= EmployeeValidationConstants.LastNameMinLength &&
                lastName.Length <= EmployeeValidationConstants.LastNameMaxLength;
        }

        public static bool IsFirstNameValid(string firstName)
        {
            var alphanumeric = new Regex(@"^[0-9A-Za-z ]+$");

            return
                !string.IsNullOrEmpty(firstName) &&
                alphanumeric.IsMatch(firstName.RemoveDiacritics()) &&
                firstName.Length >= EmployeeValidationConstants.FirstNameMinLength &&
                firstName.Length <= EmployeeValidationConstants.FirstNameMaxLength;
        }
    }
}
