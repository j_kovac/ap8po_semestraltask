﻿using InstituteManagementSoftware.Constants.Validation;
using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.Models.ScheduledActivities.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using System;

namespace InstituteManagementSoftware.Infrastructure.Validators
{
    public class ScheduledActivityValidator : IModelValidator<ScheduledActivityViewModel>
    {
        public bool Validate(ScheduledActivityViewModel viewModel)
        {
            var studentsCountV = IsStudentsCountValid(viewModel.StudentsCount);
            var nameV = IsNameValid(viewModel.Name);
            var noteV = IsNoteValid(viewModel.Note);
            var languageV = IsLanguageValid(viewModel.Language);
            var hoursCountV = IsHoursCountValid(viewModel.HoursCount);
            var weeksCountV = IsWeeksCountValid(viewModel.WeeksCount);
            var activityTypeV = IsActivityTypeValid(viewModel.ActivityType);
            var teacherV = IsTeacherValid(viewModel.Employee);
            var classV = IsClassValid(viewModel.Class);

            return studentsCountV && nameV && noteV && languageV && hoursCountV &&
                weeksCountV && activityTypeV && teacherV && classV;
        }

        public static bool IsClassValid(ClassViewModel @class) => @class.GetModel() is not null;

        public static bool IsTeacherValid(EmployeeViewModel employee) => employee.GetModel() is not null;

        public static bool IsActivityTypeValid(ActivityType activityType) => Enum.IsDefined(activityType);

        public static bool IsLanguageValid(Language language) => Enum.IsDefined(language);

        public static bool IsWeeksCountValid(int weeksCount) =>
            weeksCount >= ScheduledActivityValidationConstants.WeeksCountMin &&
            weeksCount <= ScheduledActivityValidationConstants.WeeksCountMax;

        public static bool IsHoursCountValid(int hoursCount) =>
            hoursCount >= ScheduledActivityValidationConstants.HoursCountMin &&
            hoursCount <= ScheduledActivityValidationConstants.HoursCountMax;

        public static bool IsStudentsCountValid(int studentsCount) =>
            studentsCount >= ScheduledActivityValidationConstants.StudentsCountMin &&
            studentsCount <= ScheduledActivityValidationConstants.StudentsCountMax;

        public static bool IsNoteValid(string note) =>
            string.IsNullOrEmpty(note) ||
            (note.Length > 0 && !string.IsNullOrWhiteSpace(note));

        public static bool IsNameValid(string name)
        {
            return
                !string.IsNullOrEmpty(name) &&
                name.Length >= ScheduledActivityValidationConstants.NameLengthMin &&
                name.Length <= ScheduledActivityValidationConstants.NameLengthMax;
        }
    }
}
