﻿using InstituteManagementSoftware.ViewModels.Config.Settings;

namespace InstituteManagementSoftware.Infrastructure.Validators.Interfaces
{
    public interface ISharedSettingsValidator
    {
        bool Validate(SettingsViewModel viewModel);
    }
}
