﻿namespace InstituteManagementSoftware.Infrastructure.Validators.Interfaces
{
    public interface IModelValidator<T>
    {
        bool Validate(T viewModel);
    }
}
