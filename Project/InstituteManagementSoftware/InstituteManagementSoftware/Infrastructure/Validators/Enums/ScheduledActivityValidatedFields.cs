﻿namespace InstituteManagementSoftware.Infrastructure.Validators.Enums
{
    public enum ScheduledActivityValidatedFields
    {
        STUDENTS_COUNT,
        NAME,
        NOTE,
        LANGUAGE,
        HOURS_COUNT,
        WEEKS_COUNT,
        ACTIVITY_TYPE,
        TEACHER,
        CLASS
    }
}
