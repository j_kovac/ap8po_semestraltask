﻿namespace InstituteManagementSoftware.Infrastructure.Validators.Enums
{
    public enum StudyGroupValidatedFields
    {
        NAME,
        SHORT_NAME,
        STUDY_YEAR,
        STUDENT_MAX_COUNT,
        STUDY_TYPE,
        SEMESTER,
        LANGUAGE,
        STUDY_FORM,
        TEACHER
    }
}
