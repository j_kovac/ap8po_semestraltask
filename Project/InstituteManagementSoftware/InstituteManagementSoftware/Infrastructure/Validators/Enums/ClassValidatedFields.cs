﻿namespace InstituteManagementSoftware.Infrastructure.Validators.Enums
{
    public enum ClassValidatedFields
    {
        FULL_NAME,
        SHORT_NAME,
        LENGTH_WEEKS,
        CLASS_SIZE,
        CREDITS,
        EXERCICES_HOURS_COUNT,
        SEMINARS_HOURS_COUNT,
        LECTURE_HOURS_COUNT,
        ENDING_TYPE,
        DEPARTMENT,
        LANGUAGE
    }
}
