﻿namespace InstituteManagementSoftware.Infrastructure.Validators.Enums
{
    public enum EmployeeValidatedFields
    {
        FIRST_NAME,
        LAST_NAME,
        BIRTH_DATE,
        EMAIL,
        PHONE,
        IS_DOC_STUDENT,
        DEPARTMENT,
        SALARY,
        NOTE,
        EMPLOYEE_TYPE,
        WORK_PERCENTAGE
    }
}
