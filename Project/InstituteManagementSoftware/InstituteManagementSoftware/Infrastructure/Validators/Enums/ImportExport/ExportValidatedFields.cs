﻿namespace InstituteManagementSoftware.Infrastructure.Validators.Enums.ImportExport
{
    public enum ExportValidatedFields
    {
        EMAIL,
        FORMAT,
        DIRECTORY_PATH
    }
}
