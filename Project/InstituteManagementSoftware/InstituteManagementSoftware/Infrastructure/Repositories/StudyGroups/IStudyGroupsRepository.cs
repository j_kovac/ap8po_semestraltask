﻿using InstituteManagementSoftware.Models.StudyGroups.Models;

namespace InstituteManagementSoftware.Infrastructure.Repositories.StudyGroups
{
    public interface IStudyGroupsRepository : IRepositoryBase<StudyGroup>
    {
    }
}
