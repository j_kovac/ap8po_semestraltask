﻿using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Models.StudyGroups.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InstituteManagementSoftware.Infrastructure.Repositories.StudyGroups
{
    public class StudyGroupsRepository : IStudyGroupsRepository
    {
        private readonly DataContext _dataContext;

        public StudyGroupsRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IList<StudyGroup> GetAll(Func<StudyGroup, bool> predicate)
        {
            var data = _dataContext.StudyGroups
                .Include(x => x.Garant)
                .Include(x => x.StudyGroupSubjects)
                .ThenInclude(x => x.Subject)
                .Where(x => !x.IsDeleted)
                .ToList();


            if (predicate is not null)
            {
                data = data
                    .Where(predicate)
                    .ToList();
            }

            return data;
        }

        public StudyGroup Insert(StudyGroup entity)
        {
            _dataContext.StudyGroups.Add(entity);
            _dataContext.SaveChanges();

            return entity;
        }

        public void Remove(int id)
        {
            var removed = _dataContext.StudyGroups.SingleOrDefault(x => x.ID == id);
            if (removed is not null)
            {
                removed.IsDeleted = true;
                _dataContext.SaveChanges();
            }
        }

        public StudyGroup Update(StudyGroup entity)
        {
            var updated = _dataContext.StudyGroups.SingleOrDefault(x => x.ID == entity.ID);
            if (updated is not null)
            {
                _dataContext.SaveChanges();
            }
            return updated;
        }
    }
}
