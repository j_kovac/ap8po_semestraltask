﻿using InstituteManagementSoftware.Models.Classes.Models;

namespace InstituteManagementSoftware.Infrastructure.Repositories.Classes
{
    public interface IClassesRepository : IRepositoryBase<Subject>
    {
    }
}
