﻿using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Models.Classes.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InstituteManagementSoftware.Infrastructure.Repositories.Classes
{
    public class ClassesRepository : IClassesRepository
    {
        private readonly DataContext _dataContext;

        public ClassesRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IList<Subject> GetAll(Func<Subject, bool> predicate)
        {
            var data = _dataContext.Classes
                    .Include(x =>x.ScheduledActivities)
                    .Include(x => x.StudyGroupSubjects)
                    .ThenInclude(x => x.StudyGroup)
                    .Where(x => !x.IsDeleted)
                    .ToList();


            if (predicate is not null)
            {
                data = data
                    .Where(predicate)
                    .ToList();
            }

            return data;
        }

        public Subject Insert(Subject entity)
        {
            _dataContext.Classes.Add(entity);
            _dataContext.SaveChanges();

            return entity;
        }

        public void Remove(int id)
        {
            var removed = _dataContext.Classes.SingleOrDefault(x => x.ID == id);
            if ( removed is not null)
            {
                removed.IsDeleted = true;
                _dataContext.SaveChanges();
            }
        }

        public Subject Update(Subject entity)
        {
            var updated = _dataContext.Classes.SingleOrDefault(x => x.ID == entity.ID);
            if(updated is not null)
            {
                _dataContext.SaveChanges();
            }
            return updated;
        }
    }
}
