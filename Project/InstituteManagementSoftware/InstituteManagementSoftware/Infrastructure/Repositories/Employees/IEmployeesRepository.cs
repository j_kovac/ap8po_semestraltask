﻿using InstituteManagementSoftware.Models.Employees.Models;

namespace InstituteManagementSoftware.Infrastructure.Repositories.Employees
{
    public interface IEmployeesRepository : IRepositoryBase<Employee>
    {
    }
}
