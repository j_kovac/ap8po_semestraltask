﻿using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Models.Employees.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InstituteManagementSoftware.Infrastructure.Repositories.Employees
{
    public class EmployeesRepository : IEmployeesRepository
    {

        private readonly DataContext _dataContext;
        public EmployeesRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IList<Employee> GetAll(Func<Employee, bool> predicate)
        {
            var data = _dataContext.Employees
                .Include(x => x.ScheduledActivities)
                .Include(x => x.StudyGroups)
                .Where(x => !x.IsDeleted)
                .ToList();


            if (predicate is not null)
            {
                data = data
                    .Where(predicate)
                    .ToList();
            }

            return data;
        }

        public Employee Insert(Employee entity)
        {
            _dataContext.Employees.Add(entity);
            _dataContext.SaveChanges();

            return entity;
        }

        public void Remove(int id)
        {
            var removed = _dataContext.Employees.SingleOrDefault(x => x.ID == id);
            if (removed is not null)
            {
                removed.IsDeleted = true;
                _dataContext.SaveChanges();
            }
        }

        public Employee Update(Employee entity)
        {
            var updated = _dataContext.Employees.SingleOrDefault(x => x.ID == entity.ID);
            if (updated is not null)
            {
                _dataContext.SaveChanges();
            }
            return updated;
        }
    }
}
