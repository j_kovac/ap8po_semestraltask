﻿using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using System.Collections.Generic;

namespace InstituteManagementSoftware.Infrastructure.Repositories.ScheduledActivities
{
    public interface IScheduledActivitiesRepository : IRepositoryBase<ScheduledActivity>
    {
        IList<ScheduledActivity> Update(IList<ScheduledActivity> entities);

        void DeleteAll(IList<ScheduledActivity> entities);

        IList<ScheduledActivity> InsertAll(IList<ScheduledActivity> entities);
    }
}
