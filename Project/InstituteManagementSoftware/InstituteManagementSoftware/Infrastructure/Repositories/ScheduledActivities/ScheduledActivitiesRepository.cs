﻿using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InstituteManagementSoftware.Infrastructure.Repositories.ScheduledActivities
{
    public class ScheduledActivitiesRepository : IScheduledActivitiesRepository
    {
        private readonly DataContext _dataContext;

        public ScheduledActivitiesRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IList<ScheduledActivity> GetAll(Func<ScheduledActivity, bool> predicate)
        {
            var data = _dataContext.ScheduledActivities
                .Include(x => x.Class)
                .Include(x => x.Employee)
                .Where(x => !x.IsDeleted)
                .ToList();

            if (predicate is not null)
            {
                data = data
                    .Where(predicate)
                    .ToList();
            }

            return data;
        }

        public ScheduledActivity Insert(ScheduledActivity entity)
        {
            _dataContext.ScheduledActivities.Add(entity);
            _dataContext.SaveChanges();

            return entity;
        }

        public void Remove(int id)
        {
            var removed = _dataContext.ScheduledActivities.SingleOrDefault(x => x.ID == id);
            if (removed is not null)
            {
                removed.IsDeleted = true;
                _dataContext.SaveChanges();
            }
        }

        public ScheduledActivity Update(ScheduledActivity entity)
        {
            var updated = _dataContext.ScheduledActivities.SingleOrDefault(x => x.ID == entity.ID);
            if (updated is not null)
            {
                _dataContext.SaveChanges();
            }
            return updated;
        }

        public IList<ScheduledActivity> Update(IList<ScheduledActivity> entities)
        {
            var ids = entities.Select(x => x.ID).ToList();
            var updated = _dataContext.ScheduledActivities.Where(x => ids.Contains(x.ID)).ToList();
            if (updated is not null && updated.Count == ids.Count)
            {
                _dataContext.SaveChanges();
            }
            return entities;
        }

        public void DeleteAll(IList<ScheduledActivity> entities)
        {
            _dataContext.ScheduledActivities.RemoveRange(entities);
            _dataContext.SaveChanges();
        }

        public IList<ScheduledActivity> InsertAll(IList<ScheduledActivity> entities)
        {
            _dataContext.AddRange(entities);
            _dataContext.SaveChanges();
            return entities;
        }
    }
}
