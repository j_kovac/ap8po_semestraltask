﻿using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Models.Settings.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InstituteManagementSoftware.Infrastructure.Repositories.Settings
{
    public class SettingsRepository : ISettingsRepository
    {
        private readonly DataContext _dataContext;

        public SettingsRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IList<RatingSettings> GetAll(Func<RatingSettings, bool> predicate) => _dataContext.RatingSettings.Where(x => !x.IsDeleted).ToList();

        public RatingSettings Insert(RatingSettings entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        public RatingSettings Update(RatingSettings entity)
        {
            throw new NotImplementedException();
        }
    }
}
