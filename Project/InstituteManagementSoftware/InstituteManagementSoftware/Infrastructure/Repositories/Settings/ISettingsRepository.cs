﻿using InstituteManagementSoftware.Models.Settings.Models;

namespace InstituteManagementSoftware.Infrastructure.Repositories.Settings
{
    public interface ISettingsRepository : IRepositoryBase<RatingSettings>
    {
    }
}
