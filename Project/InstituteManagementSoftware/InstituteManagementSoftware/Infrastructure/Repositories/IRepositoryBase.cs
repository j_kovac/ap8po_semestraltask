﻿using System;
using System.Collections.Generic;

namespace InstituteManagementSoftware.Infrastructure.Repositories
{
    public interface IRepositoryBase<T>
    {
        T Insert(T entity);
        T Update(T entity);
        void Remove(int id);
        IList<T> GetAll(Func<T, bool> predicate);
    }
}
