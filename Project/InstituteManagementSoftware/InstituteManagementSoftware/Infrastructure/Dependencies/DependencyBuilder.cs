﻿using AutoMapper;
using InstituteManagementSoftware.Infrastructure.Data;
using InstituteManagementSoftware.Infrastructure.Mappings;
using InstituteManagementSoftware.Infrastructure.Repositories.Classes;
using InstituteManagementSoftware.Infrastructure.Repositories.Employees;
using InstituteManagementSoftware.Infrastructure.Repositories.ScheduledActivities;
using InstituteManagementSoftware.Infrastructure.Repositories.Settings;
using InstituteManagementSoftware.Infrastructure.Repositories.StudyGroups;
using InstituteManagementSoftware.Infrastructure.Services.ActionManager;
using InstituteManagementSoftware.Infrastructure.Services.ActionManager.DataManipulation;
using InstituteManagementSoftware.Infrastructure.Services.ActionManager.Export;
using InstituteManagementSoftware.Infrastructure.Services.ActionManager.Import;
using InstituteManagementSoftware.Infrastructure.Services.Classes;
using InstituteManagementSoftware.Infrastructure.Services.Email;
using InstituteManagementSoftware.Infrastructure.Services.Employees;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Infrastructure.Services.Management.Activities;
using InstituteManagementSoftware.Infrastructure.Services.Management.Groups;
using InstituteManagementSoftware.Infrastructure.Services.Management.Processing;
using InstituteManagementSoftware.Infrastructure.Services.ScheduledActivities;
using InstituteManagementSoftware.Infrastructure.Services.StudyGroups;
using InstituteManagementSoftware.Infrastructure.Validators;
using InstituteManagementSoftware.Infrastructure.Validators.ImportExport;
using InstituteManagementSoftware.Infrastructure.Validators.Interfaces;
using InstituteManagementSoftware.Models.Shared.Models;
using InstituteManagementSoftware.ViewModels.Config.Navigators;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using InstituteManagementSoftware.Visualisation.Bindings;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop.Management.ActivityManagement;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop.Management.StudyGroupManagement;
using Ninject;
using System.Windows.Input;

namespace InstituteManagementSoftware.Infrastructure.Dependencies
{
    public class DependencyBuilder
    {
        private readonly IKernel _kernel;
        public DependencyBuilder(IKernel kernel)
        {
            _kernel = kernel;
        }

        public T Get<T>() => _kernel.Get<T>();

        public void Build()
        {
            BindRepositories();
            BindServices();
            BindUtilityModels();
            BindVisualisationHelpers();
            BindDbConnection();
            BindMapper();
            BindValidators();
        }

        private void BindRepositories()
        {
            _kernel.Bind<IClassesRepository>().To<ClassesRepository>().InSingletonScope();
            _kernel.Bind<IEmployeesRepository>().To<EmployeesRepository>().InSingletonScope();
            _kernel.Bind<IScheduledActivitiesRepository>().To<ScheduledActivitiesRepository>().InSingletonScope();
            _kernel.Bind<IStudyGroupsRepository>().To<StudyGroupsRepository>().InSingletonScope();
            _kernel.Bind<ISettingsRepository>().To<SettingsRepository>().InSingletonScope();
        }

        private void BindServices()
        {
            _kernel.Bind<ILoggingService>().To<LoggingService>().InSingletonScope();
            _kernel.Bind<ICalculationService>().To<CalculationService>().InTransientScope();

            _kernel.Bind<IClassService>().To<ClassService>().InTransientScope();
            _kernel.Bind<IEmployeeService>().To<EmployeeService>().InTransientScope();
            _kernel.Bind<IScheduledActivitiesService>().To<ScheduledActivitiesService>().InTransientScope();
            _kernel.Bind<IStudyGroupService>().To<StudyGroupService>().InTransientScope();

            _kernel.Bind<IGroupManagementService>().To<GroupManagementService>().InTransientScope();
            _kernel.Bind<IActivityManagementService>().To<ActivityManagementService>().InTransientScope();
            _kernel.Bind<IDataManipulationService>().To<DataManipulationService>().InTransientScope();

            _kernel.Bind<IExportService>().To<ExportService>().InTransientScope();
            _kernel.Bind<IImportService>().To<ImportService>().InTransientScope();
            _kernel.Bind<ICommunicationService>().To<CommunicationService>().InTransientScope();
            _kernel.Bind<IEmailService>().To<EmailService>().InTransientScope();
        }

        private void BindUtilityModels()
        {
            _kernel.Bind<ICommand>().To<RelayCommand>();
            _kernel.Bind<INavigator>().To<Navigator>();
            _kernel.Bind<DependencyBuilder>().ToSelf();

            _kernel.Bind<SharedSettings>().ToSelf().InSingletonScope();
        }

        private void BindVisualisationHelpers()
        {
            _kernel.Bind<DragAndDropHandler<StudyGroupViewModel>>().To<StudyGroupDragAndDropHandler>();
            _kernel.Bind<DragAndDropHandler<ScheduledActivityViewModel>>().To<ScheduledActivityDragAndDropHandler>();
        }

        private void BindValidators()
        {
            _kernel.Bind<IModelValidator<StudyGroupViewModel>>().To<StudyGroupValidator>();
            _kernel.Bind<IModelValidator<EmployeeViewModel>>().To<EmployeeValidator>();
            _kernel.Bind<IModelValidator<ClassViewModel>>().To<ClassValidator>();
            _kernel.Bind<IModelValidator<ScheduledActivityViewModel>>().To<ScheduledActivityValidator>();

            _kernel.Bind<ExportValidator>().ToSelf();
            _kernel.Bind<ImportValidator>().ToSelf();
        }

        private void BindDbConnection()
        {
            var factory = new DataContextFactory();
            var context = factory.CreateDbContext(null);
            _kernel.Bind<DataContext>().ToConstant(context);
        }

        private void BindMapper()
        {
            _kernel.Bind<IMapper>()
                .ToMethod(context =>
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.AddProfile<ClassMappingProfile>();
                        cfg.AddProfile<EmployeeMappingProfile>();
                        cfg.AddProfile<ScheduledActivityMappingProfile>();
                        cfg.AddProfile<StudyGroupMappingProfile>();

                        cfg.ConstructServicesUsing(t => _kernel.Get(t));
                    });
                    return config.CreateMapper();
                }).InSingletonScope();
        }
    }
}
