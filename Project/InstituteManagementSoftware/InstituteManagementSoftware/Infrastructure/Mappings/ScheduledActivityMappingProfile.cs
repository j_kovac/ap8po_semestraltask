﻿using AutoMapper;
using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;

namespace InstituteManagementSoftware.Infrastructure.Mappings
{
    public class ScheduledActivityMappingProfile : Profile
    {
        public ScheduledActivityMappingProfile()
        {
            CreateMaps();
        }

        private void CreateMaps()
        {
            CreateMap<ScheduledActivityViewModel, ScheduledActivityViewModel>();
            CreateMap<ScheduledActivity, ScheduledActivityViewModel>().ReverseMap();

            CreateMap<ScheduledActivity, ScheduledActivity>()
                .ForMember(x => x.ID, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}
