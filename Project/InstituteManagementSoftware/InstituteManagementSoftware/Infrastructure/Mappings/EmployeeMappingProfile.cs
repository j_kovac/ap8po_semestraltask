﻿using AutoMapper;
using InstituteManagementSoftware.Models.Employees.Models;
using InstituteManagementSoftware.ViewModels.Partial.Employees;

namespace InstituteManagementSoftware.Infrastructure.Mappings
{
    public class EmployeeMappingProfile : Profile
    {
        public EmployeeMappingProfile()
        {
            CreateMaps();
        }

        private void CreateMaps()
        {
            CreateMap<EmployeeViewModel, EmployeeViewModel>();
            CreateMap<Employee, EmployeeViewModel>().ReverseMap();

            CreateMap<Employee, Employee>()
                .ForMember(x => x.ID, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}
