﻿using AutoMapper;
using InstituteManagementSoftware.Models.StudyGroups.Models;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;

namespace InstituteManagementSoftware.Infrastructure.Mappings
{
    public class StudyGroupMappingProfile : Profile
    {
        public StudyGroupMappingProfile()
        {
            CreateMaps();
        }

        private void CreateMaps()
        {
            CreateMap<StudyGroupViewModel, StudyGroupViewModel>();
            CreateMap<StudyGroup, StudyGroupViewModel>().ReverseMap();

            CreateMap<StudyGroup, StudyGroup>()
                .ForMember(x => x.ID, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}
