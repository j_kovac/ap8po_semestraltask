﻿using AutoMapper;
using InstituteManagementSoftware.Models.Classes.Models;
using InstituteManagementSoftware.ViewModels.Partial.Classes;

namespace InstituteManagementSoftware.Infrastructure.Mappings
{
    public class ClassMappingProfile : Profile
    {

        public ClassMappingProfile()
        {
            CreateMaps();
        }

        private void CreateMaps()
        {
            CreateMap<ClassViewModel, ClassViewModel>();
            CreateMap<Subject, ClassViewModel>().ReverseMap();

            CreateMap<Subject, Subject>()
                .ForMember(x => x.ID, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}
