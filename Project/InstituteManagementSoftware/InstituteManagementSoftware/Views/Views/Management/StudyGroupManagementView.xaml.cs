﻿using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using InstituteManagementSoftware.ViewModels.Windows.Management;
using System.Windows.Controls;

namespace InstituteManagementSoftware.Views.Views.Management
{
    /// <summary>
    /// Interaction logic for StudyGroupManagementView.xaml
    /// </summary>
    public partial class StudyGroupManagementView : UserControl
    {
        public StudyGroupManagementView()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = (sender as ComboBox).SelectedItem as StudyGroupViewModel;
            var context = DataContext as GroupManagementWindowViewModel;
            if (context is not null && selected is not null)
            {
                context.SelectedChangedCommand.Execute(selected);
            }
        }
    }
}
