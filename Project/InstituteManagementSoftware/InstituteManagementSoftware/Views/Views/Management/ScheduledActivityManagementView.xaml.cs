﻿using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Windows.Management;
using System.Windows.Controls;

namespace InstituteManagementSoftware.Views.Views.Management
{
    /// <summary>
    /// Interaction logic for ScheduledActivityManagementView.xaml
    /// </summary>
    public partial class ScheduledActivityManagementView : UserControl
    {
        public ScheduledActivityManagementView()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = (sender as ComboBox).SelectedItem as EmployeeViewModel;
            var context = DataContext as ActivityManagementWindowViewModel;
            if (context is not null && selected is not null)
            {
                context.SelectedChangedCommand.Execute(selected);
            }
        }
    }
}
