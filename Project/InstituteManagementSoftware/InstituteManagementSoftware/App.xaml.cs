﻿using InstituteManagementSoftware.Infrastructure.Dependencies;
using InstituteManagementSoftware.ViewModels.Windows;
using Ninject;
using System.Windows;

namespace InstituteManagementSoftware
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

           var dependencyBuilder = new DependencyBuilder(new StandardKernel(new NinjectSettings() { InjectNonPublic = true }));
            dependencyBuilder.Build();

            var appViewModel = dependencyBuilder.Get<IndexWindowViewModel>();
            var mainWindow = new MainWindow()
            {
                DataContext = appViewModel
            };
            mainWindow.Show();
        }
    }
}
