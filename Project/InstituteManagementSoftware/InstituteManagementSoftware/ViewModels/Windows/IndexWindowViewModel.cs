﻿using InstituteManagementSoftware.Infrastructure.Services.ActionManager;
using InstituteManagementSoftware.Infrastructure.Services.Logs;
using InstituteManagementSoftware.Models.Logs.models;
using InstituteManagementSoftware.Models.Shared.Models;
using InstituteManagementSoftware.ViewModels.Config.Navigators;
using InstituteManagementSoftware.ViewModels.Config.Settings;
using InstituteManagementSoftware.Visualisation.Bindings;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace InstituteManagementSoftware.ViewModels.Windows
{
    public class IndexWindowViewModel : WindowViewModelBase
    {
        private readonly ICommunicationService _communicationService;

        public INavigator Navigator { get; set; }

        public SettingsViewModel Settings { get; set; }
        public ObservableCollection<Log> Logs { get; set; }

        public ICommand SaveCommand { get; set; }
        public ICommand ImportCommand { get; set; }
        public ICommand ExportCommand { get; set; }
        public ICommand SelectImportPathCommand { get; set; }
        public ICommand SelectExportPathCommand { get; set; }

        public IndexWindowViewModel(INavigator navigator, ILoggingService loggingService, ICommunicationService communicationService, SharedSettings settings)
        {
            Navigator = navigator;
            _communicationService = communicationService;

            Logs = loggingService.GetLogs();
            Settings = new SettingsViewModel(settings);

            InitCommands();
            _communicationService.InitializeSettings(Settings);
        }

        private void InitCommands()
        {
            SaveCommand = new RelayCommand(_communicationService.Save, CanSave);
            ImportCommand = new RelayCommand(_communicationService.Import, CanImport);
            ExportCommand = new RelayCommand(_communicationService.Export, CanExport);

            SelectExportPathCommand = new RelayCommand(SelectExportPath, CanSelectExportPath);
            SelectImportPathCommand = new RelayCommand(SelectImportPath, CanSelectImportPath);
        }

        private bool CanExport(object param)
        {
            var viewModel = param as SettingsViewModel;
            if (viewModel is not null)
            {
                return _communicationService.ValidateExport(viewModel);
            }
            return false;
        }

        private bool CanSave(object param)
        {
            var viewModel = param as SettingsViewModel;
            if(viewModel is not null)
            {
                return !viewModel.AreImportChangesSaved;
            }
            return false;
        }

        private bool CanImport(object param)
        {
            var viewModel = param as SettingsViewModel;
            if (viewModel is not null)
            {
                return _communicationService.ValidateImport(viewModel);
            }
            return false;
        }

        private bool CanSelectExportPath(object param) => true;
        private bool CanSelectImportPath(object param) => true;

        private void SelectExportPath(object param)
        {
            var path = param as string;
            if (!string.IsNullOrWhiteSpace(path))
            {
                Settings.ExportFilePath = path;
            }
        }

        private void SelectImportPath(object param)
        {
            var path = param as string;
            if (!string.IsNullOrWhiteSpace(path))
            {
                Settings.ImportFilePath = path;
            }
        }
    }
}
