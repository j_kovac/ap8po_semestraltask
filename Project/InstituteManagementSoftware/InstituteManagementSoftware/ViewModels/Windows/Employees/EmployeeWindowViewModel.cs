﻿using InstituteManagementSoftware.Infrastructure.Services.Employees;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.Visualisation.Bindings;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.ViewModels.Windows.Employees
{
    public class EmployeeWindowViewModel : WindowViewModelBase
    {
        private readonly IEmployeeService _service;
        public ObservableCollection<EmployeeViewModel> Employees { get; set; }

        public EmployeeViewModel EmployeeSingle { get; set; }

        public EmployeeWindowViewModel(IEmployeeService service)
        {
            _service = service;

            EmployeeSingle = _service.ExtractItemTemplate();
            Employees = _service.ExtractSourceCollection();

            InitCommands();
        }

        private void InitCommands()
        {
            AddCommand = new RelayCommand(_service.Add, CanAdd);
            UpdateCommand = new RelayCommand(_service.Update, CanUpdate);
            DeleteCommand = new RelayCommand(_service.Delete, CanDelete);
        }

        private bool CanAdd(object param)
        {
            var viewModel = param as EmployeeViewModel;
            if (viewModel is not null)
            {
                return _service.ValidateViewModel(viewModel);
            }
            return false;
        }

        private bool CanUpdate(object param)
        {
            var viewModel = param as EmployeeViewModel;
            if (viewModel is not null)
            {
                return _service.ValidateViewModel(viewModel);
            }
            return false;
        }

        private bool CanDelete(object param) => true;
    }
}
