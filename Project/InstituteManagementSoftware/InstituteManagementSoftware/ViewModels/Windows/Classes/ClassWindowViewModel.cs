﻿using InstituteManagementSoftware.Infrastructure.Services.Classes;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.Visualisation.Bindings;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.ViewModels.Windows.Classes
{
    public class ClassWindowViewModel : WindowViewModelBase
    {
        private readonly IClassService _service;
        public ClassViewModel ClassSingle { get; set; }
        public ObservableCollection<ClassViewModel> Classes { get; set; }


        public ClassWindowViewModel(IClassService service)
        {
            _service = service;
            Classes = _service.ExtractSourceCollection();
            ClassSingle = _service.ExtractItemTemplate();

            InitCommands();
        }

        private void InitCommands()
        {
            AddCommand = new RelayCommand(_service.Add, CanAdd);
            UpdateCommand = new RelayCommand(_service.Update, CanUpdate);
            DeleteCommand = new RelayCommand(_service.Delete, CanDelete);
        }

        private bool CanAdd(object param)
        {
            var viewModel = param as ClassViewModel;
            if (viewModel is not null)
            {
                return _service.ValidateViewModel(viewModel);
            }
            return false;
        }

        private bool CanUpdate(object param)
        {
            var viewModel = param as ClassViewModel;
            if (viewModel is not null)
            {
                return _service.ValidateViewModel(viewModel);
            }
            return false;
        }

        private bool CanDelete(object param) => true;
    }
}
