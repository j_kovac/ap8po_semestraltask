﻿using InstituteManagementSoftware.Infrastructure.Services.Management.Groups;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using InstituteManagementSoftware.Visualisation.Bindings;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop.Enums;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace InstituteManagementSoftware.ViewModels.Windows.Management
{
    public class GroupManagementWindowViewModel : WindowViewModelBase
    {
        private readonly IGroupManagementService _service;

        public ICommand SelectedChangedCommand { get; set; }
        public ICommand AssignCommand { get; set; }
        public ICommand RemoveCommand { get; set; }

        public DragAndDropHandler<StudyGroupViewModel> DropHandlerAssign { get; set; }
        public DragAndDropHandler<StudyGroupViewModel> DropHandlerRemove { get; set; }

        public StudyGroupViewModel SelectedStudyGroup { get; set; }

        public ObservableCollection<StudyGroupViewModel> AvailableStudyGroups { get; set; }
        public ObservableCollection<ClassViewModel> ClassesPool { get; set; }
        public ObservableCollection<ClassViewModel> ClassesAssigned { get; set; }



        public GroupManagementWindowViewModel(IGroupManagementService service, DragAndDropHandler<StudyGroupViewModel> dragAndDropHandlerAssign, DragAndDropHandler<StudyGroupViewModel> dragAndDropHandlerRemove)
        {
            _service = service;

            DropHandlerAssign = dragAndDropHandlerAssign;
            DropHandlerAssign.InitActionType(DragAndDropActionType.ASSIGN);
            DropHandlerRemove = dragAndDropHandlerRemove;
            DropHandlerRemove.InitActionType(DragAndDropActionType.REMOVE);

            AvailableStudyGroups = _service.ExtractAvailableStudyGroups();

            var collections = _service.ExtractSourceCollections();
            ClassesAssigned = collections.Item1;
            ClassesPool = collections.Item2;

            InitCommands();
        }

        private void InitCommands()
        {
            SelectedChangedCommand = new RelayCommand((args) => _service.ChangeSelection(args), CanChangeSelection);
            AssignCommand = new RelayCommand((subject) => _service.Assign(subject, SelectedStudyGroup), CanAssign);
            RemoveCommand = new RelayCommand((subject) => _service.Unassign(subject, SelectedStudyGroup), CanRemove);
        }

        private bool CanChangeSelection(object param) => true;
        private bool CanAssign(object param) => true;
        private bool CanRemove(object param) => true;
    }
}