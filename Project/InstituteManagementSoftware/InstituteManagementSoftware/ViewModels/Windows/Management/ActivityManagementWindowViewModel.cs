﻿using InstituteManagementSoftware.Infrastructure.Services.Management.Activities;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using InstituteManagementSoftware.Visualisation.Bindings;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop.Enums;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace InstituteManagementSoftware.ViewModels.Windows.Management
{
    public class ActivityManagementWindowViewModel : WindowViewModelBase
    {
        private readonly IActivityManagementService _service;

        public DragAndDropHandler<ScheduledActivityViewModel> DropHandlerAssign { get; set; }
        public DragAndDropHandler<ScheduledActivityViewModel> DropHandlerRemove { get; set; }

        public ICommand UnassignAllCommand { get; set; }
        public ICommand DeleteAllCommand { get; set; }
        public ICommand GenerateActivitiesCommand { get; set; }

        public ICommand SelectedChangedCommand { get; set; }
        public ICommand AssignCommand { get; set; }
        public ICommand RemoveCommand { get; set; }


        private EmployeeViewModel _selectedEmployee;
        public EmployeeViewModel SelectedEmployee
        {
            get => _selectedEmployee;
            set
            {
                _selectedEmployee = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<EmployeeViewModel> AvailableEmployees { get; set; }

        public ObservableCollection<ScheduledActivityViewModel> ScheduledActivitiesPool { get; set; }
        public ObservableCollection<ScheduledActivityViewModel> ScheduledActivitiesAssigned { get; set; }
        public ObservableCollection<ScheduledActivityViewModel> ScheduledActivitiesAssignedToOthers { get; set; }

        public ActivityManagementWindowViewModel(IActivityManagementService service, DragAndDropHandler<ScheduledActivityViewModel> dragAndDropHandlerAssign, DragAndDropHandler<ScheduledActivityViewModel> dragAndDropHandlerRemove)
        {
            _service = service;

            DropHandlerAssign = dragAndDropHandlerAssign;
            DropHandlerAssign.InitActionType(DragAndDropActionType.ASSIGN);
            DropHandlerRemove = dragAndDropHandlerRemove;
            DropHandlerRemove.InitActionType(DragAndDropActionType.REMOVE);

            AvailableEmployees = _service.ExtractAvailableEmployees();

            var collections = _service.ExtractSourceCollections();
            ScheduledActivitiesAssigned = collections.Item1;
            ScheduledActivitiesPool = collections.Item2;
            ScheduledActivitiesAssignedToOthers = collections.Item3;

            InitCommands();
        }

        private void InitCommands()
        {
            UnassignAllCommand = new RelayCommand(_service.UnassignAll, CanUnassignAll);
            DeleteAllCommand = new RelayCommand(_service.DeleteAll, CanDeleteAll);
            GenerateActivitiesCommand = new RelayCommand(_service.GenerateActivities, CanGenerate);

            SelectedChangedCommand = new RelayCommand((args) => _service.ChangeSelection(args), CanChangeSelection);
            AssignCommand = new RelayCommand((subject) => _service.Assign(subject, SelectedEmployee), CanAssign);
            RemoveCommand = new RelayCommand((subject) => _service.Unassign(subject, SelectedEmployee), CanRemove);
        }

        private bool CanUnassignAll(object obj) => (ScheduledActivitiesAssigned.Count + ScheduledActivitiesAssignedToOthers.Count) > 0;
        private bool CanDeleteAll(object obj) => (ScheduledActivitiesPool.Count + ScheduledActivitiesAssigned.Count + ScheduledActivitiesAssignedToOthers.Count) > 0;
        private bool CanGenerate(object obj) => (ScheduledActivitiesPool.Count + ScheduledActivitiesAssigned.Count + ScheduledActivitiesAssignedToOthers.Count) == 0;

        private bool CanChangeSelection(object param) => true;
        private bool CanAssign(object param) => true;
        private bool CanRemove(object param) => true;
    }
}
