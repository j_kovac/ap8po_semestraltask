﻿using InstituteManagementSoftware.Infrastructure.Services.ScheduledActivities;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using InstituteManagementSoftware.Visualisation.Bindings;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.ViewModels.Windows.ScheduledActivities
{
    public class ScheduledActivityWindowViewModel : WindowViewModelBase
    {
        private readonly IScheduledActivitiesService _service;
        public ObservableCollection<ScheduledActivityViewModel> ScheduledActivities { get; set; }
        public ScheduledActivityViewModel ScheduledActivitySingle { get; set; }

        public ObservableCollection<ClassViewModel> AvailableClasses { get; set; }
        public ObservableCollection<EmployeeViewModel> AvailableEmployees  { get; set; }

        public ScheduledActivityWindowViewModel(IScheduledActivitiesService service)
        {
            _service = service;
            ScheduledActivitySingle = _service.ExtractItemTemplate();
            ScheduledActivities = _service.ExtractSourceCollection();

            AvailableEmployees = _service.ExtractAvailableEmployees();
            AvailableClasses = _service.ExtractAvailableClasses();
            InitCommands();
        }
        private void InitCommands()
        {
            AddCommand = new RelayCommand(_service.Add, CanAdd);
            UpdateCommand = new RelayCommand(_service.Update, CanUpdate);
            DeleteCommand = new RelayCommand(_service.Delete, CanDelete);
        }

        private bool CanAdd(object param)
        {
            var viewModel = param as ScheduledActivityViewModel;
            if (viewModel is not null)
            {
                return _service.ValidateViewModel(viewModel);
            }
            return false;
        }

        private bool CanUpdate(object param)
        {
            var viewModel = param as ScheduledActivityViewModel;
            if (viewModel is not null)
            {
                return _service.ValidateViewModel(viewModel);
            }
            return false;
        }

        private bool CanDelete(object param) => true;
    }
}
