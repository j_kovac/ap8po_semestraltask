﻿using InstituteManagementSoftware.Infrastructure.Services.StudyGroups;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using InstituteManagementSoftware.Visualisation.Bindings;
using System.Collections.ObjectModel;

namespace InstituteManagementSoftware.ViewModels.Windows.StudyGroups
{
    public class StudyGroupWindowViewModel : WindowViewModelBase
    {
        private readonly IStudyGroupService _service;

        public StudyGroupViewModel StudyGroupSingle { get; set; }
        public ObservableCollection<StudyGroupViewModel> StudyGroups { get; set; }

        public ObservableCollection<EmployeeViewModel> AvailableEmployees { get; set; }


        public StudyGroupWindowViewModel(IStudyGroupService service)
        {
            _service = service;
            StudyGroupSingle = _service.ExtractItemTemplate();
            StudyGroups = _service.ExtractSourceCollection();

            AvailableEmployees = _service.ExtractAvailableEmployees();
            InitCommands();
        }

        private void InitCommands()
        {
            AddCommand = new RelayCommand(_service.Add, CanAdd);
            UpdateCommand = new RelayCommand(_service.Update, CanUpdate);
            DeleteCommand = new RelayCommand(_service.Delete, CanDelete);
        }

        private bool CanAdd(object param)
        {
            var viewModel = param as StudyGroupViewModel;
            if(viewModel is not null)
            {
                return _service.ValidateViewModel(viewModel);
            }
            return false;
        }

        private bool CanUpdate(object param)
        {
            var viewModel = param as StudyGroupViewModel;
            if (viewModel is not null)
            {
                return _service.ValidateViewModel(viewModel);
            }
            return false;
        }

        private bool CanDelete(object param) => true;
    }
}
