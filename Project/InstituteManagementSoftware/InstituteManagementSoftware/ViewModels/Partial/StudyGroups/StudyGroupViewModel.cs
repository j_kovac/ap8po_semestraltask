﻿using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Models.StudyGroups.Enums;
using InstituteManagementSoftware.Models.StudyGroups.Models;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.Visualisation.Bindings;

namespace InstituteManagementSoftware.ViewModels.Partial.StudyGroups
{
    public class StudyGroupViewModel : ViewModelBase
    {
        private readonly StudyGroup _studyGroup;

        public StudyGroupViewModel(StudyGroup model)
        {
            _studyGroup = model;
        }

        public StudyGroup GetModel() => _studyGroup;

        private bool _isChanged;
        public bool IsChanged
        {
            get => _isChanged;
            set
            {
                _isChanged = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _studyGroup.Name;
            set
            {
                _studyGroup.Name = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public string ShortName
        {
            get => _studyGroup.ShortName;
            set
            {
                _studyGroup.ShortName = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public int StudyYear
        {
            get => _studyGroup.StudyYear;
            set
            {
                _studyGroup.StudyYear = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public int StudentsMaxCount
        {
            get => _studyGroup.StudentsMaxCount;
            set
            {
                _studyGroup.StudentsMaxCount = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }
        
        public StudyType StudyType
        {
            get => _studyGroup.StudyType;
            set
            {
                _studyGroup.StudyType = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public Semester Semester
        {
            get => _studyGroup.Semester;
            set
            {
                _studyGroup.Semester = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public StudyForm StudyForm
        {
            get => _studyGroup.StudyForm;
            set
            {
                _studyGroup.StudyForm = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public Language Language
        {
            get => _studyGroup.Language;
            set
            {
                _studyGroup.Language = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public EmployeeViewModel Garant
        {
            get => new(_studyGroup.Garant);
            set
            {
                if(value is not null)
                {
                    _studyGroup.Garant = value.GetModel();
                    _studyGroup.GarantID = _studyGroup.Garant.ID;
                }
                else
                {
                    _studyGroup.Garant = null;
                }
                IsChanged = true;
                OnPropertyChanged();
            }
        }
    }
}
