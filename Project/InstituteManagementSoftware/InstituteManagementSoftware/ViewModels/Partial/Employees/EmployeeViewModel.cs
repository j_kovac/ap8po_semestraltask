﻿using InstituteManagementSoftware.Models.Employees.Enums;
using InstituteManagementSoftware.Models.Employees.Models;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Visualisation.Bindings;
using System;

namespace InstituteManagementSoftware.ViewModels.Partial.Employees
{
    public class EmployeeViewModel : ViewModelBase
    {
        private readonly Employee _employee;

        public EmployeeViewModel(Employee model)
        {
            _employee = model;
        }

        public Employee GetModel() => _employee;

        private bool _isChanged;
        public bool IsChanged
        {
            get => _isChanged;
            set
            {
                _isChanged = value;
                OnPropertyChanged();
            }
        }

        public string FullName => $"{FirstName} {LastName}";

        public string FirstName
        {
            get => _employee?.FirstName ?? string.Empty;
            set
            {
                _employee.FirstName = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public string LastName
        {
            get => _employee?.LastName ?? string.Empty;
            set
            {
                _employee.LastName = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public DateTime BirthDate
        {
            get => _employee.BirthDate;
            set
            {
                _employee.BirthDate = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public string Email
        {
            get => _employee.Email;
            set
            {
                _employee.Email = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public string Phone
        {
            get => _employee.Phone;
            set
            {
                _employee.Phone = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public bool IsDocStudent
        {
            get => _employee.IsDocStudent;
            set
            {
                _employee.IsDocStudent = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public Department Department
        {
            get => _employee.Department;
            set
            {
                _employee.Department = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public double Salary
        {
            get => _employee.Salary;
            set
            {
                _employee.Salary = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public string Note
        {
            get => _employee.Note;
            set
            {
                _employee.Note = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public EmployeeType EmployeeType
        {
            get => _employee.EmployeeType;
            set
            {
                _employee.EmployeeType = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public double? WorkPercentage
        {
            get => _employee.WorkPercentage;
            set
            {
                _employee.WorkPercentage = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        private double _workingPoints;
        public double WorkingPoints
        {
            get => _workingPoints;
            set
            {
                _workingPoints = value;
                OnPropertyChanged();
            }
        }
    }
}