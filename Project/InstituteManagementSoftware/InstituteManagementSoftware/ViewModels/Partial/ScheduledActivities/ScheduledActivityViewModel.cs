﻿using InstituteManagementSoftware.Models.ScheduledActivities.Enums;
using InstituteManagementSoftware.Models.ScheduledActivities.Models;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.Visualisation.Bindings;

namespace InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities
{
    public class ScheduledActivityViewModel : ViewModelBase
    {
        private readonly ScheduledActivity _scheduledActivity;

        public ScheduledActivityViewModel(ScheduledActivity model)
        {
            _scheduledActivity = model;
        }

        public ScheduledActivity GetModel() => _scheduledActivity;

        private bool _isChanged;
        public bool IsChanged
        {
            get => _isChanged;
            set
            {
                _isChanged = value;
                OnPropertyChanged();
            }
        }

        public int StudentsCount
        {
            get => _scheduledActivity.StudentsCount;
            set
            {
                _scheduledActivity.StudentsCount = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _scheduledActivity.Name;
            set
            {
                _scheduledActivity.Name = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public string Note
        {
            get => _scheduledActivity.Note;
            set
            {
                _scheduledActivity.Note = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public Language Language
        {
            get => _scheduledActivity.Language;
            set
            {
                _scheduledActivity.Language = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public int HoursCount
        {
            get => _scheduledActivity.HoursCount;
            set
            {
                _scheduledActivity.HoursCount = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public int WeeksCount
        {
            get => _scheduledActivity.WeeksCount;
            set
            {
                _scheduledActivity.WeeksCount = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public ActivityType ActivityType
        {
            get => _scheduledActivity.ActivityType;
            set
            {
                _scheduledActivity.ActivityType = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public EmployeeViewModel Employee
        {
            get => new(_scheduledActivity.Employee);
            set
            {
                if(value is not null)
                {
                    _scheduledActivity.Employee = value.GetModel();
                    _scheduledActivity.EmployeeID = _scheduledActivity.Employee.ID;
                }
                else
                {
                    _scheduledActivity.Employee = null;
                }

                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public ClassViewModel Class
        {
            get => new(_scheduledActivity.Class);
            set
            {
                if(value is not null)
                {
                    _scheduledActivity.Class = value.GetModel();
                    _scheduledActivity.ClassID = _scheduledActivity.Class.ID;
                }
                else
                {
                    _scheduledActivity.Class = null;
                }
                IsChanged = true;
                OnPropertyChanged();
            }
        }
    }
}
