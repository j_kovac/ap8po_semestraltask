﻿using InstituteManagementSoftware.Models.Classes.Enums;
using InstituteManagementSoftware.Models.Classes.Models;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Visualisation.Bindings;

namespace InstituteManagementSoftware.ViewModels.Partial.Classes
{
    public class ClassViewModel : ViewModelBase
    {
        private readonly Subject _class;

        public ClassViewModel(Subject model)
        {
            _class = model;
        }

        public Subject GetModel() => _class;

        private bool _isChanged;
        public bool IsChanged
        {
            get => _isChanged;
            set
            {
                _isChanged = value;
                OnPropertyChanged();
            }
        }

        public string FullName
        {
            get => _class.FullName;
            set
            {
                _class.FullName = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public string ShortName
        {
            get => _class.ShortName;
            set
            {
                _class.ShortName = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public int LengthWeeks
        {
            get => _class.LengthWeeks;
            set
            {
                _class.LengthWeeks = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public int ClassSize
        {
            get => _class.ClassSize;
            set
            {
                _class.ClassSize = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }
        public int Credits
        {
            get => _class.Credits;
            set
            {
                _class.Credits = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public int ExercicesHoursCount
        {
            get => _class.ExercicesHoursCount;
            set
            {
                _class.ExercicesHoursCount = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public int SeminarsHoursCount
        {
            get => _class.SeminarsHoursCount;
            set
            {
                _class.SeminarsHoursCount = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public int LectureHoursCount
        {
            get => _class.LectureHoursCount;
            set
            {
                _class.LectureHoursCount = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public EndingType EndingType
        {
            get => _class.EndingType;
            set
            {
                _class.EndingType = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public Department Department
        {
            get => _class.Department;
            set
            {
                _class.Department = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }

        public Language Language
        {
            get => _class.Language;
            set
            {
                _class.Language = value;
                IsChanged = true;
                OnPropertyChanged();
            }
        }
    }
}
