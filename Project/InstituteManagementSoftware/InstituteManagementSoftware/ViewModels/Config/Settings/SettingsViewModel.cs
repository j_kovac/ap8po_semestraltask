﻿using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Models.Shared.Models;
using InstituteManagementSoftware.Visualisation.Bindings;

namespace InstituteManagementSoftware.ViewModels.Config.Settings
{
    public class SettingsViewModel : ViewModelBase
    {
        private readonly SharedSettings _settings;

        public SettingsViewModel(SharedSettings settings)
        {
            _settings = settings;

            ExportFormat = FileExportFormat.XML;
            SendExportEmail = true;
            ExportEmail = string.Empty;
            ExportFilePath = string.Empty;
            ImportFilePath = string.Empty;
        }

        public SharedSettings GetModel() => _settings;

        public bool AreImportChangesSaved
        {
            get => _settings.AreImportChangesSaved;
            set
            {
                _settings.AreImportChangesSaved = value;
                OnPropertyChanged();
            }
        }

        public FileExportFormat ExportFormat
        {
            get => _settings.ExportFormat;
            set
            {
                _settings.ExportFormat = value;
                OnPropertyChanged();
            }
        }

        public bool SendExportEmail
        {
            get => _settings.SendExportEmail;
            set
            {
                _settings.SendExportEmail = value;
                OnPropertyChanged();
            }
        }

        public string ExportEmail
        {
            get => _settings.ExportEmail;
            set
            {
                _settings.ExportEmail = value;
                OnPropertyChanged();
            }
        }

        public string ExportFilePath
        {
            get => _settings.ExportFilePath;
            set
            {
                _settings.ExportFilePath = value;
                OnPropertyChanged();
            }
        }

        public string ImportFilePath
        {
            get => _settings.ImportFilePath;
            set
            {
                _settings.ImportFilePath = value;
                OnPropertyChanged();
            }
        }
    }
}
