﻿using InstituteManagementSoftware.Infrastructure.Services.Classes;
using InstituteManagementSoftware.Infrastructure.Services.Employees;
using InstituteManagementSoftware.Infrastructure.Services.Management.Activities;
using InstituteManagementSoftware.Infrastructure.Services.Management.Groups;
using InstituteManagementSoftware.Infrastructure.Services.ScheduledActivities;
using InstituteManagementSoftware.Infrastructure.Services.StudyGroups;
using InstituteManagementSoftware.ViewModels.Config.Navigators.Enums;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using InstituteManagementSoftware.ViewModels.Windows.Classes;
using InstituteManagementSoftware.ViewModels.Windows.Employees;
using InstituteManagementSoftware.ViewModels.Windows.Management;
using InstituteManagementSoftware.ViewModels.Windows.ScheduledActivities;
using InstituteManagementSoftware.ViewModels.Windows.StudyGroups;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop;
using Ninject;
using System;
using System.Windows.Input;

namespace InstituteManagementSoftware.ViewModels.Config.Navigators.Commands
{
    public class UpdateCurrentViewModelCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private readonly INavigator _navigator;
        private readonly IKernel _kernel;

        public UpdateCurrentViewModelCommand(INavigator navigator, IKernel kernel)
        {
            _navigator = navigator;
            _kernel = kernel;

            Execute(ViewType.ACTIVITYMANAGEMENT);
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter is ViewType viewType)
            {
                switch (viewType)
                {
                    case ViewType.EMPLOYEES:
                        _navigator.CurrentViewModel = new EmployeeWindowViewModel(_kernel.Get<IEmployeeService>());
                        break;
                    case ViewType.CLASSES:
                        _navigator.CurrentViewModel = new ClassWindowViewModel(_kernel.Get<IClassService>());
                        break;
                    case ViewType.SCHEDULEDACTIVITIES:
                        _navigator.CurrentViewModel = new ScheduledActivityWindowViewModel(_kernel.Get<IScheduledActivitiesService>());
                        break;
                    case ViewType.STUDYGROUPS:
                        _navigator.CurrentViewModel = new StudyGroupWindowViewModel(_kernel.Get<IStudyGroupService>());
                        break;
                    case ViewType.GROUPMANAGEMENT:
                        _navigator.CurrentViewModel = new GroupManagementWindowViewModel(_kernel.Get<IGroupManagementService>(), _kernel.Get<DragAndDropHandler<StudyGroupViewModel>>(), _kernel.Get<DragAndDropHandler<StudyGroupViewModel>>());
                        break;
                    case ViewType.ACTIVITYMANAGEMENT:
                        _navigator.CurrentViewModel = new ActivityManagementWindowViewModel(_kernel.Get<IActivityManagementService>(), _kernel.Get<DragAndDropHandler<ScheduledActivityViewModel>>(), _kernel.Get<DragAndDropHandler<ScheduledActivityViewModel>>());
                        break;
                }
            }
        }
    }
}
