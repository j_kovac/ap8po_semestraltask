﻿namespace InstituteManagementSoftware.ViewModels.Config.Navigators.Enums
{
    public enum ViewType
    {
        EMPLOYEES,
        CLASSES,
        STUDYGROUPS,
        SCHEDULEDACTIVITIES,
        GROUPMANAGEMENT,
        ACTIVITYMANAGEMENT
    }
}
