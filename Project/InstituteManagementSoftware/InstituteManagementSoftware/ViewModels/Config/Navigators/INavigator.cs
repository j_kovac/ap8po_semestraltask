﻿using InstituteManagementSoftware.Visualisation.Bindings;
using System.Windows.Input;

namespace InstituteManagementSoftware.ViewModels.Config.Navigators
{
    public interface INavigator
    {
        WindowViewModelBase CurrentViewModel { get; set; }
        ICommand UpdateCurrentViewModelCommand { get; }
    }
}
