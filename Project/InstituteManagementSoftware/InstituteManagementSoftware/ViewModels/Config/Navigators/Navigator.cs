﻿using InstituteManagementSoftware.ViewModels.Config.Navigators.Commands;
using InstituteManagementSoftware.Visualisation.Bindings;
using Ninject;
using System.Windows.Input;

namespace InstituteManagementSoftware.ViewModels.Config.Navigators
{
    public class Navigator : ViewModelBase, INavigator
    {
        private WindowViewModelBase _currentViewModel;
        public WindowViewModelBase CurrentViewModel
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                _currentViewModel = value;
                OnPropertyChanged();
            }
        }

        public ICommand UpdateCurrentViewModelCommand { get; set; }

        public Navigator(IKernel kernel)
        {
            UpdateCurrentViewModelCommand = new UpdateCurrentViewModelCommand(this, kernel);
        }
    }
}
