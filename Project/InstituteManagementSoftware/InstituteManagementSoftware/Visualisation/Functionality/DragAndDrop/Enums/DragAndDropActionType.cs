﻿namespace InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop.Enums
{
    public enum DragAndDropActionType
    {
        ASSIGN,
        REMOVE
    }
}
