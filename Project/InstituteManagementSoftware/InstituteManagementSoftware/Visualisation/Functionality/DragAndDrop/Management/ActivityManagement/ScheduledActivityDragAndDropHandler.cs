﻿using GongSolutions.Wpf.DragDrop;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using InstituteManagementSoftware.ViewModels.Windows.Management;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop.Enums;
using System.Windows.Controls;

namespace InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop.Management.ActivityManagement
{
    public class ScheduledActivityDragAndDropHandler : DragAndDropHandler<ScheduledActivityViewModel>
    {
        public override void Drop(IDropInfo dropInfo)
        {
            var droppedActivity = dropInfo.Data as ScheduledActivityViewModel;
            var dropArea = dropInfo.VisualTarget as ScrollViewer;
            var viewModel = dropArea.DataContext as ActivityManagementWindowViewModel;
            var targetEmployee = viewModel.SelectedEmployee;

            switch (ActionType)
            {
                case DragAndDropActionType.ASSIGN:
                    AssignActivityToEmployee(droppedActivity, viewModel, targetEmployee);
                    break;
                case DragAndDropActionType.REMOVE:
                    RemoveActivityFromEmployee(droppedActivity, viewModel, targetEmployee);
                    break;
            }
        }

        private void AssignActivityToEmployee(ScheduledActivityViewModel droppedActivity, ActivityManagementWindowViewModel viewModel, EmployeeViewModel targetEmployee)
        {
            if (targetEmployee is not null && droppedActivity is not null)
            {
                if (!viewModel.ScheduledActivitiesAssigned.Contains(droppedActivity))
                {
                    viewModel.ScheduledActivitiesAssigned.Add(droppedActivity);
                    viewModel.ScheduledActivitiesPool.Remove(droppedActivity);

                    viewModel.AssignCommand.Execute(droppedActivity);
                }
            }
        }

        private void RemoveActivityFromEmployee(ScheduledActivityViewModel droppedActivity, ActivityManagementWindowViewModel viewModel, EmployeeViewModel targetEmployee)
        {
            if (targetEmployee is not null && droppedActivity is not null)
            {
                if (viewModel.ScheduledActivitiesAssigned.Contains(droppedActivity))
                {
                    viewModel.ScheduledActivitiesAssigned.Remove(droppedActivity);
                    viewModel.ScheduledActivitiesPool.Add(droppedActivity);

                    viewModel.RemoveCommand.Execute(droppedActivity);
                }
            }
        }
    }
}
