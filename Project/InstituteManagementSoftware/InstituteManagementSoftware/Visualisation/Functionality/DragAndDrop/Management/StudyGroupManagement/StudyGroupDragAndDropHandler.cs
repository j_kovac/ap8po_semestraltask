﻿using GongSolutions.Wpf.DragDrop;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using InstituteManagementSoftware.ViewModels.Windows.Management;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop.Enums;
using System.Windows.Controls;

namespace InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop.Management.StudyGroupManagement
{
    public class StudyGroupDragAndDropHandler : DragAndDropHandler<StudyGroupViewModel>
    {
        public override void Drop(IDropInfo dropInfo)
        {
            var droppedClass = dropInfo.Data as ClassViewModel;
            var dropArea = dropInfo.VisualTarget as ScrollViewer;
            var viewModel = dropArea.DataContext as GroupManagementWindowViewModel;
            var targetStudyGroup = viewModel.SelectedStudyGroup;

            switch (ActionType)
            {
                case DragAndDropActionType.ASSIGN:
                    AssignClassToStudyGroup(droppedClass, viewModel, targetStudyGroup);
                    break;
                case DragAndDropActionType.REMOVE:
                    RemoveClassFromStudyGroup(droppedClass, viewModel, targetStudyGroup);
                    break;
            }
        }

        private void AssignClassToStudyGroup(ClassViewModel droppedClass, GroupManagementWindowViewModel viewModel, StudyGroupViewModel targetStudyGroup)
        {
            if (targetStudyGroup is not null && droppedClass is not null)
            {
                if (!viewModel.ClassesAssigned.Contains(droppedClass))
                {
                    viewModel.ClassesAssigned.Add(droppedClass);
                    viewModel.ClassesPool.Remove(droppedClass);

                    viewModel.AssignCommand.Execute(droppedClass);
                }
            }
        }

        private void RemoveClassFromStudyGroup(ClassViewModel droppedClass, GroupManagementWindowViewModel viewModel, StudyGroupViewModel targetStudyGroup)
        {
            if (targetStudyGroup is not null && droppedClass is not null)
            {
                if (!viewModel.ClassesPool.Contains(droppedClass))
                {
                    viewModel.ClassesAssigned.Remove(droppedClass);
                    viewModel.ClassesPool.Add(droppedClass);

                    viewModel.RemoveCommand.Execute(droppedClass);
                }
            }
        }
    }
}
