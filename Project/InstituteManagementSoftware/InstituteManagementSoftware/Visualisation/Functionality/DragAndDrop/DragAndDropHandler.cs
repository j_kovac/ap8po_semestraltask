﻿using GongSolutions.Wpf.DragDrop;
using InstituteManagementSoftware.Visualisation.Bindings;
using InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop.Enums;
using System.Windows;

namespace InstituteManagementSoftware.Visualisation.Functionality.DragAndDrop
{
    public abstract class DragAndDropHandler<T> : IDropTarget where T: ViewModelBase
    {
        protected DragAndDropActionType? ActionType { get; set; }

        public void InitActionType(DragAndDropActionType actionType){
            ActionType = actionType;
        }

        public virtual void DragOver(IDropInfo dropInfo)
        {
            dropInfo.DropTargetAdorner = typeof(DropTargetHighlightAdorner);
            dropInfo.Effects = DragDropEffects.Move;
        }

        public abstract void Drop(IDropInfo dropInfo);
    }
}
