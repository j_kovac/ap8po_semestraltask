﻿using System.Windows.Input;

namespace InstituteManagementSoftware.Visualisation.Bindings
{
    public class WindowViewModelBase : ViewModelBase
    {
        public ICommand AddCommand { get; protected set; }
        public ICommand UpdateCommand { get; protected set; }
        public ICommand DeleteCommand { get; protected set; }
    }
}
