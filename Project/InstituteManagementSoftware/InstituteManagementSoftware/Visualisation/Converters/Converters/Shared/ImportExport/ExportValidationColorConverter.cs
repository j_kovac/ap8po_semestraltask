﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Shared;
using InstituteManagementSoftware.Infrastructure.Validators.Enums.ImportExport;
using InstituteManagementSoftware.Infrastructure.Validators.ImportExport;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Config.Settings;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.Shared.ImportExport
{
    public class ExportValidationColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var settings = values[0] as SettingsViewModel;
            var value = values[1];
            var fieldType = (ExportValidatedFields)values[2];

            var valid = true;

            if (settings is not null)
            {
                switch (fieldType)
                {
                    case ExportValidatedFields.DIRECTORY_PATH:
                        valid = ExportValidator.IsFileNameValid((string)value);
                        break;
                    case ExportValidatedFields.EMAIL:
                        valid = ExportValidator.IsEmailValid((string)value, settings.SendExportEmail);
                        break;
                    case ExportValidatedFields.FORMAT:
                        valid = ExportValidator.IsFormatValid((FileExportFormat)value);
                        break;
                }
            }
            return valid ? new BrushConverter().ConvertFrom(ColorStringConstants.Blue) : new BrushConverter().ConvertFrom(ColorStringConstants.White);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
