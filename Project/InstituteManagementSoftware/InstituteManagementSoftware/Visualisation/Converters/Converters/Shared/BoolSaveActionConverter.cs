﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Shared;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.Shared
{
    public class BoolSaveActionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isModified = (bool) value;
            return isModified ? new BrushConverter().ConvertFrom(ColorStringConstants.Orange) : new BrushConverter().ConvertFrom(ColorStringConstants.Green);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
