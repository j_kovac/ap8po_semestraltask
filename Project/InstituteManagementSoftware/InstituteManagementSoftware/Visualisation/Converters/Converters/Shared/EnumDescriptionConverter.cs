﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.Shared
{
    public class EnumDescriptionTypeConverter : EnumConverter
    {
        public EnumDescriptionTypeConverter(Type type) : base(type) {}

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                if (value is not null)
                {
                    FieldInfo fi = value.GetType().GetField(value.ToString());
                    if (fi is not null)
                    {
                        var dattributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                        var eattributes = (EnumMemberAttribute[])fi.GetCustomAttributes(typeof(EnumMemberAttribute), false);
                        return dattributes.Select(d => d.Description).Concat(eattributes.Select(e => e.Value)).FirstOrDefault() ?? string.Empty;
                    }
                }
                return string.Empty;
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
