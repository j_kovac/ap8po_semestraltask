﻿using InstituteManagementSoftware.Models.Logs.Enums;
using InstituteManagementSoftware.Models.Logs.models;
using InstituteManagementSoftware.Visualisation.Converters.Dictionaries.Shared;
using System;
using System.Globalization;
using System.Windows.Data;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.Shared
{
    public class LogActionTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var logItem = value as Log;
            if(logItem is not null)
            {
                var titleString = logItem.Title;
                var dateString = logItem.StartTime.ToLongTimeString();
                var actionString = string.Format(LogActionTypeStringDictionary.LogMessages[(LogActionType)logItem.Action.Item1], logItem.Action.Item2);

                return string.Join(" - ", titleString, dateString, actionString);
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
