﻿using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Employees.Enums;
using System;
using System.Globalization;
using System.Windows.Data;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.Employees
{
    public class EmployeesDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var displayString = string.Empty;
            var employeeObject = value as EmployeeViewModel;
            if (employeeObject is not null)
            {
                if (parameter is not null)
                {
                    var fieldType = (EmployeeDisplayedInfo)parameter;
                    switch (fieldType)
                    {
                        case EmployeeDisplayedInfo.NAME:
                            displayString = $"{employeeObject.FullName}";
                            break;
                        case EmployeeDisplayedInfo.CONTACT:
                            displayString = $"Phone: {employeeObject.Phone}, Email: {employeeObject.Email}";
                            break;
                        case EmployeeDisplayedInfo.EMPLOYMENT_INFO:
                            displayString = $"Work type: {employeeObject.EmployeeType} - {employeeObject.WorkPercentage.ToString() ?? "N/A"}% - {(employeeObject.IsDocStudent ? "IS" : "IS NOT")} PhD student";
                            break;
                        case EmployeeDisplayedInfo.EMPLOYEE_TYPE:
                            displayString = $"{employeeObject.EmployeeType}";
                            break;
                        case EmployeeDisplayedInfo.PHD:
                            displayString = $"{(employeeObject.IsDocStudent ? "IS" : "NOT")} a PhD student";
                            break;
                        case EmployeeDisplayedInfo.POINTS:
                            displayString = $"{(employeeObject is null ? string.Empty : " Work Points")}";
                            break;
                    }
                }
                else
                {
                    displayString = $"{employeeObject.FullName}";
                }
            }
            return displayString;
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
