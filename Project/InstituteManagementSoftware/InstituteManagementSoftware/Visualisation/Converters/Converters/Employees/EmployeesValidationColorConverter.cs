﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Shared;
using InstituteManagementSoftware.Infrastructure.Validators;
using InstituteManagementSoftware.Infrastructure.Validators.Enums;
using InstituteManagementSoftware.Models.Employees.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.Employees
{
    public class EmployeesValidationColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var employee = values[0] as EmployeeViewModel;
            var value = values[1];
            var fieldType = (EmployeeValidatedFields)values[2];

            var valid = true;

            if (employee is not null)
            {
                switch (fieldType)
                {
                    case EmployeeValidatedFields.FIRST_NAME:
                        valid = EmployeeValidator.IsFirstNameValid((string)value);
                        break;
                    case EmployeeValidatedFields.LAST_NAME:
                        valid = EmployeeValidator.IsLastNameValid((string)value);
                        break;
                    case EmployeeValidatedFields.BIRTH_DATE:
                        valid = EmployeeValidator.IsBirthDateValid((DateTime)value);
                        break;
                    case EmployeeValidatedFields.EMAIL:
                        valid = EmployeeValidator.IsEmailValid((string)value);
                        break;
                    case EmployeeValidatedFields.PHONE:
                        valid = EmployeeValidator.IsPhoneValid((string)value);
                        break;
                    case EmployeeValidatedFields.IS_DOC_STUDENT:
                        {
                            var value2 = values[3];
                            valid = EmployeeValidator.IsDocStudentValid((bool)value, (EmployeeType)value2);
                        }
                        break;
                    case EmployeeValidatedFields.DEPARTMENT:
                        valid = EmployeeValidator.IsDepartmentValid((Department)value);
                        break;
                    case EmployeeValidatedFields.SALARY:
                        valid = EmployeeValidator.IsSalaryValid((double)value);
                        break;
                    case EmployeeValidatedFields.NOTE:
                        valid = EmployeeValidator.IsNoteValid((string)value);
                        break;
                    case EmployeeValidatedFields.EMPLOYEE_TYPE:
                        {
                            var value2 = values[3];
                            valid = EmployeeValidator.IsEmployeeTypeValid((EmployeeType)value, (bool)value2);
                        }
                        break;
                    case EmployeeValidatedFields.WORK_PERCENTAGE:
                        {
                            var value2 = values[3];
                            valid = EmployeeValidator.IsWorkPercentageValid((double?)value, (EmployeeType)value2);
                        }
                        break;
                }
            }
            return valid ? new BrushConverter().ConvertFrom(ColorStringConstants.Blue) : new BrushConverter().ConvertFrom(ColorStringConstants.White);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
