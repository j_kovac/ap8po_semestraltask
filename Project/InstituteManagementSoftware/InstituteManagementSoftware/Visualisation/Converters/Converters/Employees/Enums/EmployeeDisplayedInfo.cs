﻿namespace InstituteManagementSoftware.Visualisation.Converters.Converters.Employees.Enums
{
    public enum EmployeeDisplayedInfo
    {
        NAME,
        CONTACT,
        EMPLOYMENT_INFO,
        EMPLOYEE_TYPE,
        PHD,
        POINTS
    }
}
