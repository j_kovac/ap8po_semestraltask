﻿using InstituteManagementSoftware.Infrastructure.Services.Management.Processing;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using InstituteManagementSoftware.Visualisation.Converters.Converters.ScheduledActivities.Enums;
using System;
using System.Globalization;
using System.Windows.Data;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.ScheduledActivities
{
    public class ScheduledActivityDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var displayString = string.Empty;
            var activityObject = value as ScheduledActivityViewModel;
            if (activityObject is not null)
            {
                if (parameter is not null)
                {
                    var fieldType = (ScheduledActivityDisplayedInfo)parameter;
                    switch (fieldType)
                    {
                        case ScheduledActivityDisplayedInfo.DESCRIPTION:
                            displayString = $"{activityObject.Name} - {activityObject.ActivityType}";
                            break;
                        case ScheduledActivityDisplayedInfo.STUDENTS:
                            displayString = $"Students: {activityObject.StudentsCount}";
                            break;
                        case ScheduledActivityDisplayedInfo.DURATION:
                            displayString = $"{activityObject.HoursCount} H/w, {activityObject.WeeksCount} Weeks";
                            break;
                        case ScheduledActivityDisplayedInfo.LANGUAGE:
                            displayString = $"{activityObject.Language}";
                            break;
                        case ScheduledActivityDisplayedInfo.SUBJECT:
                            displayString = activityObject.Class is null ? "N/A" : $"{activityObject.Class.ShortName}";
                            break;
                        case ScheduledActivityDisplayedInfo.SUBJECT_LONG:
                            displayString = activityObject.Class is null ? "CLASS UNASSIGNED" : $"{activityObject.Class.FullName}";
                            break;
                        case ScheduledActivityDisplayedInfo.POINTS:
                            var service = new CalculationService();
                            displayString = $"{service.CalculateWorkingPointsActivity(activityObject.GetModel())} Points";
                            break;
                    }
                }
                else
                {
                    displayString = $"{activityObject.Name} - {activityObject.HoursCount} H, {activityObject.WeeksCount} W. Attendees: {activityObject.StudentsCount}";
                }
            }
            return displayString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
