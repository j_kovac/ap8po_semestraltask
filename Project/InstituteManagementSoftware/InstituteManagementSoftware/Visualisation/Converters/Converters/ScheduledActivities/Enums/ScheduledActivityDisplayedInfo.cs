﻿namespace InstituteManagementSoftware.Visualisation.Converters.Converters.ScheduledActivities.Enums
{
    public enum ScheduledActivityDisplayedInfo
    {
        DESCRIPTION,
        STUDENTS,
        DURATION,
        LANGUAGE,
        SUBJECT,
        SUBJECT_LONG,
        POINTS
    }
}
