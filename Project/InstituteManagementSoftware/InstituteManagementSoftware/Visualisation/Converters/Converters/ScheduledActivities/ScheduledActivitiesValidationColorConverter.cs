﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Shared;
using InstituteManagementSoftware.Infrastructure.Validators;
using InstituteManagementSoftware.Infrastructure.Validators.Enums;
using InstituteManagementSoftware.Models.ScheduledActivities.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.ScheduledActivities;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.ScheduledActivities
{
    public class ScheduledActivitiesValidationColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var activity = values[0] as ScheduledActivityViewModel;
            var value = values[1];
            var fieldType = (ScheduledActivityValidatedFields)values[2];

            var valid = true;

            if (activity is not null)
            {
                switch (fieldType)
                {
                    case ScheduledActivityValidatedFields.STUDENTS_COUNT:
                        valid = ScheduledActivityValidator.IsStudentsCountValid((int)value);
                        break;
                    case ScheduledActivityValidatedFields.NAME:
                        valid = ScheduledActivityValidator.IsNameValid((string)value);
                        break;
                    case ScheduledActivityValidatedFields.NOTE:
                        valid = ScheduledActivityValidator.IsNoteValid((string)value);
                        break;
                    case ScheduledActivityValidatedFields.LANGUAGE:
                        valid = ScheduledActivityValidator.IsLanguageValid((Language)value);
                        break;
                    case ScheduledActivityValidatedFields.HOURS_COUNT:
                        valid = ScheduledActivityValidator.IsHoursCountValid((int)value);
                        break;
                    case ScheduledActivityValidatedFields.WEEKS_COUNT:
                        valid = ScheduledActivityValidator.IsWeeksCountValid((int)value);
                        break;
                    case ScheduledActivityValidatedFields.ACTIVITY_TYPE:
                        valid = ScheduledActivityValidator.IsActivityTypeValid((ActivityType)value);
                        break;
                    case ScheduledActivityValidatedFields.TEACHER:
                        valid = ScheduledActivityValidator.IsTeacherValid(value as EmployeeViewModel);
                        break;
                    case ScheduledActivityValidatedFields.CLASS:
                        valid = ScheduledActivityValidator.IsClassValid(value as ClassViewModel);
                        break;
                }
            }
            return valid ? new BrushConverter().ConvertFrom(ColorStringConstants.Blue) : new BrushConverter().ConvertFrom(ColorStringConstants.White);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
