﻿using InstituteManagementSoftware.ViewModels.Partial.Classes;
using InstituteManagementSoftware.Visualisation.Converters.Converters.Classes.Enums;
using System;
using System.Globalization;
using System.Windows.Data;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.Classes
{
    public class ClassesDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var displayString = string.Empty;
            var classObject = value as ClassViewModel;
            if (classObject is not null)
            {
                if (parameter is not null)
                {
                    var fieldType = (ClassDisplayedInfo)parameter;
                    switch (fieldType)
                    {
                        case ClassDisplayedInfo.NAME:
                            displayString = $"{classObject.FullName}";
                            break;
                        case ClassDisplayedInfo.SHORTNAME:
                            displayString = $"{classObject.ShortName}";
                            break;
                        case ClassDisplayedInfo.STUDENTS:
                            displayString = $"{classObject.ClassSize} Students";
                            break;
                        case ClassDisplayedInfo.HOURS:
                            displayString = $"S/w: {classObject.SeminarsHoursCount}, E/w: {classObject.ExercicesHoursCount}, L/w: {classObject.LectureHoursCount}";
                            break;
                        case ClassDisplayedInfo.LANG:
                            displayString = $"{classObject.Language}";
                            break;
                        case ClassDisplayedInfo.FINISH:
                            displayString = $"{classObject.LengthWeeks} Weeks, {classObject.Credits} Credits - {classObject.EndingType}";
                            break;
                    }
                }
                else
                {
                    displayString  = $"{classObject.ShortName} - {classObject.FullName}";
                }
            }
            return displayString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
