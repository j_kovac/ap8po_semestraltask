﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Shared;
using InstituteManagementSoftware.Infrastructure.Validators;
using InstituteManagementSoftware.Infrastructure.Validators.Enums;
using InstituteManagementSoftware.Models.Classes.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.ViewModels.Partial.Classes;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.Classes
{
    public class ClassesValidationColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var subject = values[0] as ClassViewModel;
            var value = values[1];
            var fieldType = (ClassValidatedFields) values[2];

            var valid = true;

            if (subject is not null)
            {
                switch (fieldType)
                {
                    case ClassValidatedFields.FULL_NAME:
                        valid = ClassValidator.IsFullNameValid((string)value);
                        break;
                    case ClassValidatedFields.SHORT_NAME:
                        valid = ClassValidator.IsShortNameValid((string)value);
                        break;
                    case ClassValidatedFields.LENGTH_WEEKS:
                        valid = ClassValidator.IsLengthWeeksValid((int)value);
                        break;
                    case ClassValidatedFields.CLASS_SIZE:
                        valid = ClassValidator.IsClassSizeValid((int)value);
                        break;
                    case ClassValidatedFields.CREDITS:
                        valid = ClassValidator.IsCreditsValid((int)value);
                        break;
                    case ClassValidatedFields.EXERCICES_HOURS_COUNT:
                        valid = ClassValidator.IsExcercicesHoursCountValid((int)value);
                        break;
                    case ClassValidatedFields.SEMINARS_HOURS_COUNT:
                        valid = ClassValidator.IsSeminarsHoursCountValid((int)value);
                        break;
                    case ClassValidatedFields.LECTURE_HOURS_COUNT:
                        valid = ClassValidator.IsLectureHoursCountValid((int)value);
                        break;
                    case ClassValidatedFields.ENDING_TYPE:
                        valid = ClassValidator.IsEndingTypeValid((EndingType)value);
                        break;
                    case ClassValidatedFields.DEPARTMENT:
                        valid = ClassValidator.IsDepartmentValid((Department)value);
                        break;
                    case ClassValidatedFields.LANGUAGE:
                        valid = ClassValidator.IsLanguageValid((Language)value);
                        break;
                }
            }
            return valid ? new BrushConverter().ConvertFrom(ColorStringConstants.Blue) : new BrushConverter().ConvertFrom(ColorStringConstants.White);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
