﻿namespace InstituteManagementSoftware.Visualisation.Converters.Converters.Classes.Enums
{
    public enum ClassDisplayedInfo
    {
        NAME,
        SHORTNAME,
        HOURS,
        STUDENTS,
        FINISH,
        LANG,
    }
}
