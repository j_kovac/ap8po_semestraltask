﻿using InstituteManagementSoftware.Constants.Visualisation.Converters.Shared;
using InstituteManagementSoftware.Infrastructure.Validators;
using InstituteManagementSoftware.Infrastructure.Validators.Enums;
using InstituteManagementSoftware.Models.Shared.Enums;
using InstituteManagementSoftware.Models.StudyGroups.Enums;
using InstituteManagementSoftware.ViewModels.Partial.Employees;
using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.StudyGroups
{
    public class StudyGroupsValidationColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var studyGroup = values[0] as StudyGroupViewModel;
            var value = values[1];
            var fieldType = (StudyGroupValidatedFields)values[2];

            var valid = true;

            if (studyGroup is not null)
            {
                switch (fieldType)
                {
                    case StudyGroupValidatedFields.NAME:
                        valid = StudyGroupValidator.IsNameValid((string)value);
                        break;
                    case StudyGroupValidatedFields.SHORT_NAME:
                        valid = StudyGroupValidator.IsShortNameValid((string)value);
                        break;
                    case StudyGroupValidatedFields.STUDY_YEAR:
                        {
                            var value2 = values[3];
                            valid = StudyGroupValidator.IsStudyYearValid((int)value, (StudyForm)value2);
                        }
                        break;
                    case StudyGroupValidatedFields.STUDENT_MAX_COUNT:
                        valid = StudyGroupValidator.IsStudentsMaxCountValid((int)value);
                        break;
                    case StudyGroupValidatedFields.STUDY_TYPE:
                        valid = StudyGroupValidator.IsStudyTypeValid((StudyType)value);
                        break;
                    case StudyGroupValidatedFields.SEMESTER:
                        valid = StudyGroupValidator.IsSemesterValid((Semester)value);
                        break;
                    case StudyGroupValidatedFields.LANGUAGE:
                        valid = StudyGroupValidator.IsLanguageValid((Language)value);
                        break;
                    case StudyGroupValidatedFields.STUDY_FORM:
                        valid = StudyGroupValidator.IsStudyFormValid((StudyForm)value);
                        break;
                    case StudyGroupValidatedFields.TEACHER:
                        valid = StudyGroupValidator.IsTeacherValid(value as EmployeeViewModel);
                        break;
                }
            }
            return valid ? new BrushConverter().ConvertFrom(ColorStringConstants.Blue) : new BrushConverter().ConvertFrom(ColorStringConstants.White);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
