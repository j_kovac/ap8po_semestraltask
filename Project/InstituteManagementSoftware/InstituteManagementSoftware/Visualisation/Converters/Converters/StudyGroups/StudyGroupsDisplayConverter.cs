﻿using InstituteManagementSoftware.ViewModels.Partial.StudyGroups;
using System;
using System.Globalization;
using System.Windows.Data;

namespace InstituteManagementSoftware.Visualisation.Converters.Converters.StudyGroups
{
    public class StudyGroupsDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var groupObject = value as StudyGroupViewModel;
            if (groupObject is not null)
            {
                return $"{groupObject.ShortName} - {groupObject.Name}, Students: {groupObject.StudentsMaxCount}";
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
