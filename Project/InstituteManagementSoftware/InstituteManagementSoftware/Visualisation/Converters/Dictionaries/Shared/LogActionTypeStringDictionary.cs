﻿using InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings;
using InstituteManagementSoftware.Constants.Visualisation.Logging.ActionTypeStrings.QuickActions;
using InstituteManagementSoftware.Constants.Visualisation.Logging.Management;
using InstituteManagementSoftware.Models.Logs.Enums;
using System.Collections.Generic;

namespace InstituteManagementSoftware.Visualisation.Converters.Dictionaries.Shared
{
    public static class LogActionTypeStringDictionary
    {
        public static readonly IDictionary<LogActionType, string> LogMessages =
        new Dictionary<LogActionType, string>()
        {
            { LogActionType.LOAD_CLASS, ClassLogStringConstants.ClassLoaded},
            { LogActionType.LOAD_EMPLOYEE, EmployeeLogStringConstants.EmployeeLoaded},
            { LogActionType.LOAD_STUDYGROUP, StudyGroupLogStringConstants.StudyGroupLoaded },
            { LogActionType.LOAD_SCHEDULEDACTIVITY, ScheduledActivityLogStringConstants.ScheduledActivityLoaded },

            { LogActionType.CREATE_CLASS, ClassLogStringConstants.ClassCreated},
            { LogActionType.CREATE_EMPLOYEE, EmployeeLogStringConstants.EmployeeCreated},
            { LogActionType.CREATE_STUDYGROUP, StudyGroupLogStringConstants.StudyGroupCreated },
            { LogActionType.CREATE_SCHEDULEDACTIVITY, ScheduledActivityLogStringConstants.ScheduledActivityCreated },

            { LogActionType.UPDATE_CLASS, ClassLogStringConstants.ClassUpdated},
            { LogActionType.UPDATE_EMPLOYEE, EmployeeLogStringConstants.EmployeeUpdated},
            { LogActionType.UPDATE_STUDYGROUP, StudyGroupLogStringConstants.StudyGroupUpdated },
            { LogActionType.UPDATE_SCHEDULEDACTIVITY, ScheduledActivityLogStringConstants.ScheduledActivityUpdated },

            { LogActionType.DELETE_CLASS, ClassLogStringConstants.ClassDeleted},
            { LogActionType.DELETE_EMPLOYEE, EmployeeLogStringConstants.EmployeeDeleted},
            { LogActionType.DELETE_STUDYGROUP, StudyGroupLogStringConstants.StudyGroupDeleted },
            { LogActionType.DELETE_SCHEDULEDACTIVITY, ScheduledActivityLogStringConstants.ScheduledActivityDeleted },

            { LogActionType.IMPORT_SUCCESS, ImportLogStringConstants.ImportFinished},
            { LogActionType.IMPORT_FAILED, ImportLogStringConstants.ImportFailed},

            { LogActionType.EXPORT_SUCCESS, ExportLogStringConstants.ExportFinished},
            { LogActionType.EXPORT_FAILED, ExportLogStringConstants.ExportFailed},

            { LogActionType.SAVE_SUCCESS, SaveDataLogStringConstants.SaveFinished},
            { LogActionType.SAVE_FAILED, SaveDataLogStringConstants.SaveFailed},

            { LogActionType.EMAIL_SUCCESS, SendExportEmailLogStringConstants.EmailSent},
            { LogActionType.EMAIL_FAILED, SendExportEmailLogStringConstants.EmailFailed},

            { LogActionType.LOAD_ACTIVITY_MANAGEMENT_VIEW, ManagementLogStringConstants.ActivityManagementViewLoaded },
            { LogActionType.LOAD_GROUP_MANAGEMENT_VIEW, ManagementLogStringConstants.GroupManagementViewLoaded },

            { LogActionType.GROUP_MANAGEMENT_GROUP_SELECTED, ManagementLogStringConstants.GroupManagementGroupSelected },
            { LogActionType.GROUP_MANAGEMENT_SUBJECT_ASSIGNED, ManagementLogStringConstants.GroupManagementGroupSubjectAssigned },
            { LogActionType.GROUP_MANAGEMENT_SUBJECT_UNASSIGNED, ManagementLogStringConstants.GroupManagementGroupSubjectUnassigned },

            { LogActionType.ACTIVITY_MANAGEMENT_EMPLOYEE_SELECTED, ManagementLogStringConstants.ActivityManagementEmployeeSelected },
            { LogActionType.ACTIVITY_MANAGEMENT_ACTIVITY_ASSIGNED, ManagementLogStringConstants.ActivityManagementActivityAssigned },
            { LogActionType.ACTIVITY_MANAGEMENT_ACTIVITY_UNASSIGNED, ManagementLogStringConstants.ActivityManagementActivityUnassigned },

            { LogActionType.ACTIVITY_MANAGEMENT_GLOBAL_UNASSIGNED, ManagementLogStringConstants.ActivityManagementGlobalUnassigned },
            { LogActionType.ACTIVITY_MANAGEMENT_ALL_DELETED, ManagementLogStringConstants.ActivityManagementGlobalDeleted },
            { LogActionType.ACTIVITY_MANAGEMENT_GENERATED_ACTIVITIES, ManagementLogStringConstants.ActivityManagementGlobalGenerated }
        };
    }
}